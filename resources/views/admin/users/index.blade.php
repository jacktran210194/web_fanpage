@extends('admin.layout.index')
@section('main')
    <main id="main" class="main">

        <section class="section">
            <div class="row">
                <div class="col-lg-12">
                    @if (session('success'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            {{session('success')}}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                        <div class="d-flex justify-content-between align-items-center">
                            <h5 class="card-title">{{$titlePage}}</h5>
                        </div>
                    <div class="card">
                        <div class="card-body">
                            @if(count($listData) > 0)
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">STT</th>
                                    <th scope="col">SĐT</th>
                                    <th scope="col">Tên</th>
                                    <th scope="col">Ngày sinh</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($listData as $k => $value)
                                        <tr>
                                            <th scope="row">{{$k += 1}}</th>
                                            <td>{{$value->phone}}</td>
                                            <td>{{$value->name}}</td>
                                            <td>{{$value->date_of_birth}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="d-flex justify-content-center">
                                {{ $listData->render('pagination_custom.index') }}
                            </div>
                            @else
                                <h5 class="card-title">Không có dữ liệu</h5>
                         @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
@section('script')
    <script>
        $('a.btn-delete').confirm({
            title: 'Xác nhận!',
            content: 'Bạn có chắc chắn muốn xóa bản ghi này?',
            buttons: {
                ok: {
                    text: 'Xóa',
                    btnClass: 'btn-danger',
                    action: function(){
                        location.href = this.$target.attr('href');
                    }
                },
                close: {
                    text: 'Hủy',
                    action: function () {}
                }
            }
        });
    </script>
@endsection
