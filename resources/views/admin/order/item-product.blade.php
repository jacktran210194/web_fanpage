@foreach($products as $k => $product)
    <tr class="@if($k%2!=0) tr-active @else tr-default @endif">
        <td scope="col">
            <img class="image-preview" style="width: 40px; height: auto" src="{{$product->image}}">
        </td>

        <td scope="col">{{$product->sku}}</td>
        <td scope="col">{{$product->name}}</td>
        <td scope="col">
            <input type="number" name="value_id[{{$product->id}}][quantity]" placeholder="Số lượng" >
            <input type="hidden" name="value_id[{{$product->id}}][price]" value="{{$product->price}}" >
        </td>
    </tr>
@endforeach
