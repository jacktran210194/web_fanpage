@extends('admin.layout.index')
@section('main')
    <main id="main" class="main">
        <section class="section">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Thêm mới đơn hàng</h5>
                            <!-- General Form Elements -->
                            @if (session('error'))
                                <div class="alert alert-danger bg-danger text-light border-0 alert-dismissible fade show" role="alert">
                                    {{session('error')}}
                                    <button type="button" class="btn-close btn-close-white" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            @endif
                            <form action="{{route('admin.order.store')}}" method="post">
                                @csrf
                                <h8 class="card-title" style="color: #f26522">| Thông tin người mua hàng</h8>
                                <div class="row mb-3">
                                    <label for="inputNumber" class="col-sm-2 col-form-label">Số điện thoại</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="phone" required class="form-control">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-2 col-form-label">Họ và tên</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="name" required class="form-control">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-2 col-form-label">Tỉnh/Thành phố</label>
                                    <div class="col-sm-10">
                                        <select class="form-select" name="province_id" aria-label="Default select example">
                                            <option class="bg-info" value="">Tất cả</option>
                                            @foreach($province as $_prv)
                                                <option class="bg-info" value="{{$_prv->ProvinceID}}">{{$_prv->ProvinceName}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-2 col-form-label">Quận/ Huyện</label>
                                    <div class="col-sm-10">
                                        <select class="form-select" name="district_id" aria-label="Default select example">
                                            <option class="bg-info" value="">Chưa chọn tỉnh/thành phố</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-2 col-form-label">Phường/ Xã</label>
                                    <div class="col-sm-10">
                                        <select class="form-select" name="ward_id" aria-label="Default select example">
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-2 col-form-label">Địa chỉ cụ thể</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="address_detail" required class="form-control">
                                    </div>
                                </div>

                                <h8 class="card-title" style="color: #f26522">|| Sản phẩm</h8>

                                <div class="row mb-3">
                                    <label for="inputNumber" class="col-sm-2 col-form-label">Tìm kiếm sản phẩm</label>
                                    <div class="col-sm-10">
                                        <div class="search">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12">
                                                    <div class="form-label-group position-relative has-icon-left">
                                                        <input type="text" id="search" class="form-control" name="search"
                                                               placeholder="Nhập tên sản phẩm hoặc mã sản phẩm">
                                                        <div class="form-control-position">
                                                            <i class="feather icon-search"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">

                                    <table class="table table-borderless ">
                                        <thead>
                                        <tr>
                                            <th scope="col">Hình ảnh</th>
                                            <th scope="col">SKU</th>
                                            <th scope="col">Tên sản phẩm</th>
                                            <th scope="col">Giá</th>
                                        </tr>
                                        </thead>
                                        <tbody class="table_data">

                                        </tbody>
                                    </table>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-sm-2 col-form-label"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">Tạo đơn hàng</button>
                                    </div>
                                </div>

                            </form><!-- End General Form Elements -->

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
@section('script')
    <script>
        $(document).ready(function () {

            //Danh sách Quận/ Huyện
            $('select[name="province_id"]').change(function () {
                let province_id;
                province_id = $(this).val();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: window.location.origin + '/get_district/' + province_id,
                    type: 'get',
                    dataType: 'json',
                    success: function (data) {
                        console.log('data' + data);
                        if(data.status){
                            $('select[name="district_id"]').html(data.html);
                        }
                    }
                });
            });

            //Danh sách phường xã
            $('select[name="district_id"]').change(function () {
                let district_id;
                district_id = $(this).val();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: window.location.origin + '/get_ward/' + district_id,
                    type: 'get',
                    dataType: 'json',
                    success: function (data) {
                        console.log('data' + data);
                        if(data.status){
                            $('select[name="ward_id"]').html(data.html);
                        }
                    }
                });
            });

        });

    </script>

    <script>
        $(document).ready(function () {
            fetch_notification_data();
            function fetch_notification_data(query = '') {
                $.ajax({
                    url:'{{route('admin.products.search_product')}}',
                    method:'GET',
                    data:{query:query},
                    dataType: 'json',
                    success:function (data) {
                        $('tbody').html(data.table_data);
                    }
                });
            }

            $(document).on('keyup', '#search', function () {
                var query = $(this).val();
                // alert(query);
                fetch_notification_data(query);
            })

        });

    </script>
@endsection

