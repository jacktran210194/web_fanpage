@extends('admin.layout.index')
@section('main')
    <main id="main" class="main">

        <section class="section">
            <div class="row">
                <div class="col-lg-12">
                    @if (session('success'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            {{session('success')}}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title"></h5>
                                <a href="{{url('admin/order/index/all')}}" type="button" class="btn btn-outline-secondary @if($status == 'all') active @endif"> Tất cả đơn hàng <span style="font-weight: 700">{{$order_all}}</span></a>
                                <a href="{{url('admin/order/index/0')}}" class="btn btn-outline-warning @if($status == 0) active @endif">Chờ xác nhận <span style="font-weight: 700">{{$order_pending}}</span></a>
                                <a href="{{url('admin/order/index/1')}}" type="button" class="btn btn-outline-info @if($status == 1) active @endif">Đã xác nhận <span style="font-weight: 700">{{$order_confirm}}</span></a>
                                <a href="{{url('admin/order/index/2')}}" type="button" class="btn btn-outline-primary @if($status == 2) active @endif">Đang vận chuyển <span style="font-weight: 700">{{$order_delivery}}</span></a>
                                <a href="{{url('admin/order/index/3')}}" type="button" class="btn btn-outline-success @if($status == 3) active @endif">Đơn hàng hoàn thành <span style="font-weight: 700">{{$order_complete}}</span></a>
                                <a href="{{url('admin/order/index/4')}}" type="button" class="btn btn-outline-danger @if($status == 4) active @endif">Đơn huỷ <span style="font-weight: 700">{{$order_cancel}}</span></a>
                            </div>
                        </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <h5 class="card-title">{{$titlePage}}</h5>
{{--                                <a class="btn btn-success" href="{{route('admin.order.create')}}">Tạo đơn hàng mới</a>--}}
                            </div>
                            @if(count($listData) > 0)
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Mã đơn</th>
                                    <th scope="col">Bên nhận</th>
{{--                                    <th scope="col">Thuộc kho</th>--}}
                                    <th scope="col">Tổng tiền</th>
{{--                                    <th scope="col">Tùy chọn thanh toán</th>--}}
{{--                                    <th scope="col"></th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($listData as $k => $value)
                                        <tr>
                                            <th id="{{$value->id}}" scope="row">{{$value->id}}</th>
                                            <td>
                                                <a href="{{url('admin/order/detail/'.$value->id)}}" class="btn btn-icon btn-light btn-hover-success btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="Chi tiết đơn hàng">
                                                    {{$value->code_order}}<br>
                                                    <span style="color: @if($value->status == 0) #FF9900 @elseif($value->status == 1) #0099FF @elseif($value->status == 2) #0066FF @elseif($value->status == 3) #00FF00 @elseif($value->status == 4) #FF3333 @endif; font-weight: 600">{{$value->status_name}}</span>
                                                    <br>{{$value->created_at}}
                                                </a>
                                            </td>
                                            <td>
                                                {{$value->name_user}}<br>
                                                {{$value->phone_user}}<br>
                                                {{$value->address_user}}
                                            </td>

                                            <td>
                                                {{number_format($value->total_money)}} đ
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="d-flex justify-content-center">
                                {{ $listData->render('pagination_custom.index') }}
                            </div>
                            @else
                                <h5 class="card-title">Không có dữ liệu</h5>
                         @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
@section('script')
    <script>
        $('a.btn-delete').confirm({
            title: 'Xác nhận!',
            content: 'Bạn có chắc chắn muốn xóa bản ghi này?',
            buttons: {
                ok: {
                    text: 'Xóa',
                    btnClass: 'btn-danger',
                    action: function(){
                        location.href = this.$target.attr('href');
                    }
                },
                close: {
                    text: 'Hủy',
                    action: function () {}
                }
            }
        });
    </script>
@endsection
