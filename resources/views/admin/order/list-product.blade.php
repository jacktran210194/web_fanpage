@if($data_product->total() > 0)
@foreach($data_product as $value)
    <div class="row mb-2">
        <div class="col-3"><img src="{{$value->image}}" class="w-100"></div>
        <div class="col-9">
            <p class="text-black font-weight-bold mb-1">{{$value->name}}</p>
            @foreach($value->product_value as $item)
                <div class="d-flex mb-2 value-product">
                    <input type="checkbox" name="product_value_id" @if(isset($data_product_value) && in_array($item->id,$data_product_value)) checked @endif style="margin-right: 15px" value="{{$item->id}}">
                    <p class="m-0">Màu: {{$item->color_name}} - Size: {{$item->name_size}}</p>
                </div>
            @endforeach
        </div>
    </div>
@endforeach
<div class="d-flex justify-content-center">
    {{ $data_product->render('pagination_custom.index') }}
</div>
@else
    <p>Không có sản phẩm</p>
@endif
