@extends('layout.admin.index')
@section('main')
    <main id="main" class="main">
        <section class="section">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Cập nhật {{$titlePage}}</h5>
                            <!-- General Form Elements -->
                            @if (session('error'))
                                <div class="alert alert-danger bg-danger text-light border-0 alert-dismissible fade show" role="alert">
                                    {{session('error')}}
                                    <button type="button" class="btn-close btn-close-white" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            @endif
                            <form action="{{route('admin.training.update', $training->id)}}" method="post">
                                @csrf
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-2 col-form-label">Tiêu đề</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="title" required class="form-control" value="{{$training->title}}">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputNumber" class="col-sm-2 col-form-label">Banner</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="file" accept="image/*" id="formFile">
                                        <input type="hidden" name="src" value="">
                                    </div>
                                    <div class="d-flex justify-content-center align-items-center mt-5">
                                        <div class="w-50 select_image"></div>
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-2 col-form-label">Key video</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="code_youtube" class="form-control" value="{{$training->code_youtube}}">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-2 col-form-label">Mô tả ngắn</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="sub_description" required class="form-control" value="{{$training->sub_description}}">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-2 col-form-label">Nội dung</label>
                                    <div class="col-sm-10">
                                        <textarea name="content" class="ckeditor"> {{$training->content}}</textarea>
                                    </div>
                                </div>

                                <fieldset class="row mb-3">
                                    <legend class="col-form-label col-sm-2 pt-0">Thể loại</legend>
                                    <div class="col-sm-10">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="type" id="gridRadios1" value="0" @if($training->type == 0) checked @endif>
                                            <label class="form-check-label" for="gridRadios1">
                                                Văn bản
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="type" id="gridRadios2" value="1" @if($training->type == 1) checked @endif>
                                            <label class="form-check-label" for="gridRadios2">
                                                Video
                                            </label>
                                        </div>
                                    </div>
                                </fieldset>

                                <fieldset class="row mb-3">
                                    <legend class="col-form-label col-sm-2 pt-0">Lựa chọn loại đào tạo</legend>
                                    <div class="col-sm-10">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="rank" id="gridRadios1" value="GĐ_KD" @if($training->rank == 'GĐ_KD') checked @endif>
                                            <label class="form-check-label" for="gridRadios1">
                                                Giám đốc kinh doanh
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="rank" id="gridRadios2" value="GĐ_V" @if($training->rank == 'GĐ_V') checked @endif>
                                            <label class="form-check-label" for="gridRadios2">
                                                Giám đốc vùng
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="rank" id="gridRadios2" value="GĐ_T" @if($training->rank == 'GĐ_T') checked @endif>
                                            <label class="form-check-label" for="gridRadios2">
                                                Giám đốc tỉnh Tp
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="rank" id="gridRadios2" value="NPP" @if($training->rank == 'NPP') checked @endif>
                                            <label class="form-check-label" for="gridRadios2">
                                                Phân phối
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="rank" id="gridRadios2" value="ĐL" @if($training->rank == 'ĐL') checked @endif>
                                            <label class="form-check-label" for="gridRadios2">
                                                Đại lý
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="rank" id="gridRadios2" value="CTV" @if($training->rank == 'CTV') checked @endif >
                                            <label class="form-check-label" for="gridRadios2">
                                                Cộng tác viên
                                            </label>
                                        </div>
                                    </div>
                                </fieldset>

                                <fieldset class="row mb-3">
                                    <legend class="col-form-label col-sm-2 pt-0">Trạng thái</legend>
                                    <div class="col-sm-10">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="is_active" id="gridRadios1" value="1" @if($training->is_active == 1) checked @endif>
                                            <label class="form-check-label" for="gridRadios1">
                                                Hoạt động
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="is_active" id="gridRadios2" value="0" @if($training->is_active == 0) checked @endif>
                                            <label class="form-check-label" for="gridRadios2">
                                                Khoá || Lưu tạm thời
                                            </label>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="row mb-3">
                                    <label class="col-sm-2 col-form-label"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">Lưu thông tin</button>
                                    </div>
                                </div>

                            </form><!-- End General Form Elements -->

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(document).on("change", 'input[type="file"]', function () {
                var input = this;
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var formData = new FormData();
                        formData.append('file', input.files[0]);
                        formData.append('path', 'upload/category');
                        $.ajax({
                            url : window.location.origin + '/api/upload-file/image',
                            data: formData,
                            type: 'POST',
                            dataType: 'json',
                            contentType: false,
                            processData: false,
                            success: function (data) {
                                if (data.status){
                                    var img = '<img src="'+ data.src +'" class="w-100 h-100" style="object-fit: cover;">';
                                    $(".select_image").html(img);
                                    $('input[name="src"]').val(data.src);
                                }else {
                                    console.log('Đã có lỗi xảy ra');
                                }
                            }
                        });
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            });
        });
    </script>

    <script src="//cdn.ckeditor.com/4.18.0/full/ckeditor.js"></script>
    <script type="text/javascript">
        CKEDITOR.replace('content', {
            filebrowserUploadUrl: "{{route('admin.ckeditor.image-upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
    </script>
@endsection
