@extends('admin.layout.index')
@section('main')
    <main id="main" class="main">
        <section class="section">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Mã đơn hàng: {{$listData->order_code}}</h5>
                            <!-- General Form Elements -->
                            @if (session('error'))
                                <div class="alert alert-danger bg-danger text-light border-0 alert-dismissible fade show" role="alert">
                                    {{session('error')}}
                                    <button type="button" class="btn-close btn-close-white" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            @endif
                            <h8 class="card-title" style="color: #f26522">1. Thông tin người mua hàng</h8>
                            <br>
                            <br>
                            <div class="row mb-3">
                                <label for="inputNumber" class="col-sm-3 col-form-label">Số điện thoại</label>
                                <div class="col-sm-9">
                                    <input disabled type="text" name="phone" required class="form-control" value="{{$listData->phone_user}}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="inputText" class="col-sm-3 col-form-label">Họ và tên</label>
                                <div class="col-sm-9">
                                    <input disabled type="text" name="name" required class="form-control" value="{{$listData->name_user}}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="inputText" class="col-sm-3 col-form-label">Địa chỉ cụ thể</label>
                                <div class="col-sm-9">
                                    <input disabled type="text" name="address_detail" required class="form-control" value="{{$listData->address_user}}">
                                </div>
                            </div>

                            <h8 class="card-title" style="color: #f26522">3. Thông tin chi tiết đơn hàng</h8>

                            <div class="card-body">

                                <table class="table table-borderless ">
                                    <thead>
                                    <tr>
                                        <th scope="col">Stt</th>
                                        <th scope="col">Hình ảnh</th>
                                        <th scope="col">SKU</th>
                                        <th scope="col">Tên sản phẩm</th>
                                        <th scope="col">Số lượng</th>
                                        <th scope="col">Đơn giá</th>
                                        <th scope="col">Tổng tiền</th>
                                    </tr>
                                    </thead>
                                    <tbody class="table_data">
                                    @foreach($listData['order_item'] as $k => $order_item)
                                        <tr>
                                            <td>{{$k+=1}}</td>
                                            <td><img class="image-preview" style="width: 40px; height: auto" src="{{$order_item->img_product}}"></td>
                                            <td>{{$order_item->sku}}</td>
                                            <td>{{$order_item->name_product}}</td>
                                            <td>{{$order_item->quantity}}</td>
                                            <td>{{number_format($order_item->price)}}đ</td>
                                            <td>{{number_format($order_item->total_money)}}đ</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <h8 class="card-title" style="color: #f26522">4. Tổng giá trị đơn hàng</h8>
                            <br>
                            <br>

                            <div class="row mb-3">
                                <label for="inputText" class="col-sm-3 col-form-label">Trạng thái đơn hàng</label>
                                <div class="col-sm-9">
                                    <input style="color: @if($listData->status == 0) #FF9900 @elseif($listData->status == 1) #0099FF @elseif($listData->status == 2) #0066FF @elseif($listData->status == 3) #00FF00 @elseif($listData->status == 4) #FF3333 @endif; font-weight: 600" disabled type="text" name="status" required class="form-control" value="{{$listData->status_name}}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="inputText" class="col-sm-3 col-form-label">Phí vận chuyển</label>
                                <div class="col-sm-9">
                                    <input disabled type="text" name="fee_shipping" required class="form-control" value="{{number_format($listData->fee_ship)}}đ">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="inputText" class="col-sm-3 col-form-label">Tổng tiền sản phẩm</label>
                                <div class="col-sm-9">
                                    <input disabled type="text" name="order_code_transport" required class="form-control" value="{{number_format($listData->money)}}đ">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="inputText" class="col-sm-3 col-form-label">Tổng tiền đơn hàng</label>
                                <div class="col-sm-9">
                                    <input disabled type="text" name="order_code_transport" required class="form-control" value="{{number_format($listData->total_money)}}đ">
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-sm-12">
                                    @if($listData->status == 0)
                                        @if($listData->status == 0)
                                            <a href="{{url('admin/order/status/'.$listData->id.'/1')}}">
                                                <button type="submit" class="btn btn-primary">Xác nhận đơn hàng</button>
                                            </a>
                                        @endif
                                        <a href="{{url('admin/order/status/'.$listData->id.'/4')}}">
                                            <button type="submit" class="btn btn-danger">Huỷ đơn hàng</button>
                                        </a>
                                        @elseif($listData->status == 1)
                                        <a href="{{url('admin/order/status/'.$listData->id.'/2')}}">
                                            <button type="submit" class="btn btn-primary">Giao hàng</button>
                                        </a>
                                        @elseif($listData->status == 2)
                                        <a href="{{url('admin/order/status/'.$listData->id.'/3')}}">
                                            <button type="submit" class="btn btn-primary">Hoàn thành đơn hàng</button>
                                        </a>
                                        @else
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
@section('script')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="assets/admin/order.js"></script>
@endsection

