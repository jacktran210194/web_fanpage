@extends('admin.layout.index')
@section('main')
    <main id="main" class="main">

        <section class="section">
            <div class="row">
                <div class="col-lg-12">
                    @if (session('success'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            {{session('success')}}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-body">
                            <div class=" justify-content-center align-items-center">
                                <br>
                                <form action="{{route('admin.products.save_image_product', ['product_id' => $product_id])}}"  method="post" enctype="multipart/form-data">
                                    @csrf
                                    <input required name="file[]" type="file" accept="image/*" multiple class="form-control"
                                           style="width: 50%;">
                                    <br>
                                    <button type="submit" name="submit" class="btn btn-success btn-add-account"><i
                                            class="fa fa-floppy-o" aria-hidden="true"></i> Lưu
                                    </button>
                                </form>
                            </div>
                            @if(count($listData) > 0)
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Hình ảnh</th>
                                        <th scope="col">Hành động</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($listData as $k => $value)
                                        <tr>
                                            <th scope="row">{{$k+1}}</th>
                                            <td><img class="image-preview" style="width: 40px; height: auto"
                                                     src="{{$value->src}}"></td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{url('admin/products/delete-img/'.$value->id)}}" class="btn btn-delete btn-icon btn-light btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="Xóa">
                                                        <i class="bi bi-trash "></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <h5 class="card-title">Không có dữ liệu</h5>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
@section('script')
    <script>
        $('a.btn-delete').confirm({
            title: 'Xác nhận!',
            content: 'Bạn có chắc chắn muốn xóa bản ghi này?',
            buttons: {
                ok: {
                    text: 'Xóa',
                    btnClass: 'btn-danger',
                    action: function(){
                        location.href = this.$target.attr('href');
                    }
                },
                close: {
                    text: 'Hủy',
                    action: function () {}
                }
            }
        });
    </script>
@endsection
