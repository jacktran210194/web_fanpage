{{--<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>--}}
{{--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.1/socket.io.js"></script>--}}
<html>
<head>
    <title>Demo Socketio - Homepage</title>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.1/socket.io.js"></script>

    <script>
        var socket = io("http://103.226.249.30:3000");

        //client nhận dữ liệu từ server
        // socket.on("Server-sent-data", function(data)
        // {
        //     $("#chat-content").append(data);
        // });

        //client gửi dữ liệu lên server
        $(document).ready(function()
        {
            $("#send").click(function()
            {
                alert('123');
                socket.emit("Client-sent-data", "Hello world");
            });
        });
    </script>
</head>

<body>
<h1>Demo Socketio</h1>

<div>
    <button id="send">Send</button>
</div>
</body>
</html>

