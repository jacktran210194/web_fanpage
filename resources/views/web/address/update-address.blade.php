<!doctype html>
<html lang="vi">
<head>
    <base href="{{asset('')}}">
    <meta name="google-site-verification" content="googleeacc2166ce777ac3.html"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title> {{$w_title??''}}</title>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    {{--CSS Page--}}
    <link href="assets/images/logo_page.jpg" rel="icon">
    <link rel="stylesheet" type="text/css" href="{{asset('dist/web/style.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('dist/web/address/address.css')}}">
</head>
<style>
    *{
        font-family: 'Nunito', sans-serif;
    }
</style>
<body>
<main class="main" style="height: auto">
    <div class="header-page" id="header">
        <div class="d-flex justify-content-between align-items-center">
            <a href="{{url('dia-chi')}}" class="d-flex">
                <img src="{{asset('assets/images/back.svg')}}" width="40px">
            </a>
            <p class="m-0 text-white" style="font-size: 20px;font-weight: 700;letter-spacing: -0.165px;">Cập nhật địa chỉ</p>
            <div class="d-flex align-items-center">
                <a href="{{route('cart')}}" class="d-flex align-items-center position-relative mr-2">
                    <img src="{{asset('assets/images/cart.svg')}}" width="20px" alt="">
                    <p class="circle"></p>
                </a>
                <a href="https://m.me/874919005985495?ref=chat">
                    <img src="{{asset('assets/images/chat.svg')}}" width="20px" alt="">
                </a>
            </div>
        </div>
    </div>
    <form action="{{route('address.update', $address->id)}}" method="post" class="update-address">
        @csrf
        <input hidden name="token" value="{{$user->token}}">
        <p class="title-lien-he mb-2">Thông tin liên hệ</p>
        <div class="info-lien-he">
            <input type="text" name="name" value="{{$address->name}}" placeholder="Họ và Tên" class="inp-name">
            <input type="text" name="phone" value="{{$address->phone}}" placeholder="Số điện thoại" class="inp-name">
        </div>
        <p class="title-lien-he mb-2">Thông tin liên hệ</p>
        <div class="info-lien-he">
            <input type="text" name="address" value="{{$address->address}}" placeholder="Chọn địa chỉ" class="inp-name">
            <input type="text" name="comment" value="{{$address->comment}}" placeholder="Ghi chú địa chỉ" class="inp-name">
        </div>

        <div class="btn-update">
            <button class="btn-update-address" style="background: #01B2FF;color: #fff">Lưu lại địa chỉ</button>
        </div>
    </form>
</main>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script src="{{url('dist/web/home/check-login.js')}}" type="text/javascript"></script>
<script src="{{url('dist/web/product/show.js')}}" type="text/javascript"></script>
@include('web.partials.plugins-js', ['slick'=>true, 'sweetalert'=>true])
</body>
</html>
