<!doctype html>
<html lang="vi">
<head>
    <base href="{{asset('')}}">
    <meta name="google-site-verification" content="googleeacc2166ce777ac3.html"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Chi tiết bài viết</title>
    {{--Font for web--}}
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    {{--CSS Page--}}
    <link href="assets/images/logo_page.jpg" rel="icon">
    <link rel="stylesheet" type="text/css" href="{{asset('dist/web/home/home.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('dist/web/product/indext.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('dist/web/style.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>
        @include('web.partials.plugins-css', ['slick'=>true, 'sweetalert'=>true])
    <style>
        *{
            font-family: 'Nunito', sans-serif;
        }
        .img-header{
            width: 100%;
            object-fit: contain;
            position: relative;
        }
        .exit{
            width: 44px;
            height: 44px;
            position: absolute;
            top: 20px;
            left: 20px;
            background: rgba(255, 255, 255, 0.4);
            border-radius: 8px;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .detail-blog{
            position: relative;
            top: -30px;
        }
        .detail-blog-content{
            font-weight: 400;
            font-size: 15px;
            color: #333333;
        }
    </style>
</head>
<body>
{{--Header--}}
<div style="max-width: 414px;margin: 0 auto;position: relative">
    <img src="{{asset('assets/images/baby-health.png')}}" alt="" class="img-header">
    <p class="exit">
        <img src="{{asset('assets/images/exit.svg')}}" alt="">
    </p>
</div>
{{--Content Page--}}
<main class="main">
    <div class="content detail-blog" style="padding-bottom: 90px;height: auto;background: #FFFFFF;border-radius: 32px 32px 0px 0px;">
        <div class="p-3">
            <div class="d-flex align-items-center mb-2">
                <img src="{{asset('assets/images/baby.svg')}}" alt="">
                <span style="font-weight: 400;font-size: 13px;color: #828282;margin-left: 10px">Dạy trẻ tập đi</span>
            </div>
            <p style="font-weight: 600;font-size: 13px;color: #333333;margin-bottom: 10px">Các hoạt động trong giai đoạn đầu của trẻ thường rất quan trọng. Hãy chăm chỉ dạy trẻ.</p>
            <span style="font-weight: 400;font-size: 13px;color: #828282;">06:00 pm 15 thg 3</span>
            <div style="border-bottom: 1px solid #cccc; margin-top: 10px"></div>
        </div>
        <div class="detail-blog-content p-3">
            Dù chỉ mới 10 tuổi, nhưng cô bé đã sở hữu nét đẹp nổi bật, rạng ngời khiến nhiều người trầm trồ. Dẫu cuộc sống còn nhiều vất vả, nhưng trên môi của cô bé vẫn giữ được nụ cười tươi tắn, ánh mắt rạng rỡ ngây thơ như làm sáng rực cả một vùng núi rừng đại ngàn xanh tươi.

            Hình ảnh hồn nhiên, trong trẻo của các em nhỏ vùng cao hiện lên trong từng hoạt động sống thường ngày cũng khiến nhiều người xốn xang khi đặt chân đến miền sơn cước. Trước đó, dân mạng cũng từng ngẩn ngơ trước hình ảnh em bé Tỏ (sống ở bản Lao Chải San, thuộc huyện Sa Pa, Lào Cai) khi sở hữu nụ cười tươi tắn hồn nhiên. Bức ảnh lập tức "gây bão", những người xem còn thốt lên "Xinh đẹp quá!".

            Hình ảnh hồn nhiên, trong trẻo của các em nhỏ vùng cao hiện lên trong từng hoạt động sống thường ngày cũng khiến nhiều người xốn xang khi đặt chân đến miền sơn cước. Trước đó, dân mạng cũng từng ngẩn ngơ trước hình ảnh em bé Tỏ (sống ở bản Lao Chải San, thuộc huyện Sa Pa, Lào Cai) khi sở hữu nụ cười tươi tắn hồn nhiên. Bức ảnh lập tức "gây bão", những người xem còn thốt lên "Xinh đẹp quá!".
        </div>
        <div class="slide-detail-blog">
            @for($i=0;$i<4;$i++)
                <a href="#" style="text-decoration: none">
                    <div class="cart-blog mb-3">
                        <img src="{{asset('assets/images/baby-health.png')}}" alt="" style="border-radius: 16px">
                        <div class="p-3">
                            <div class="d-flex align-items-center mb-2">
                                <img src="{{asset('assets/images/baby.svg')}}" alt="">
                                <span style="font-weight: 400;font-size: 13px;color: #828282;margin-left: 10px">Dạy trẻ tập đi</span>
                            </div>
                            <p style="font-weight: 600;font-size: 13px;color: #333333;margin-bottom: 10px">Các hoạt động
                                trong giai đoạn đầu của trẻ thường rất quan trọng. Hãy chăm chỉ dạy trẻ.</p>
                            <span style="font-weight: 400;font-size: 13px;color: #828282;">06:00 pm 15 thg 3</span>
                        </div>
                    </div>
                </a>
            @endfor
        </div>
    </div>
    @include('web.partials.footer')
</main>
{{--Footer--}}

{{--JS Plugins--}}

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
{{--JS Page--}}
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
    @include('web.partials.plugins-js', ['slick'=>true, 'sweetalert'=>true])
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script src="{{url('dist/web/product/index.js')}}"></script>
<script src="{{url('dist/web/home/check-login.js')}}" type="text/javascript"></script>
<script src="{{url('dist/web/product/show.js')}}" type="text/javascript"></script>
<script src="{{asset('dist/web/home/home.js')}}"></script>
</body>
</html>


