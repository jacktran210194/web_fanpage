@extends('web.layout.master')
@section('title','web fanpage')
{{--meta--}}
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content="Trang chủ">
@stop

@section('plugins_css')
    @include('web.partials.plugins-css', ['slick'=>true, 'sweetalert'=>true])
    <style>
        .header-page{
            padding: 21px 26px;
        }
        .btn-item {
            color: #000000;
            font-weight: 600;
            font-size: 14px;
            line-height: 19px;
            background: #F0F0F0;
            width: 100%;
            padding: 15px;
            border: 0;
            text-decoration: none !important;
        }
        .btn-item.active{
            color: #000000;
            background: #ffffff;
        }
        .title-product{
            font-size: 10px;
            font-weight: 700;
            color: #000000;
            line-height: 22px;
        }
        .desc-product{
            font-weight: 700;
            font-size: 7px;
            color: #828282;
        }
        .btn-plus{
            position: absolute;
            bottom: 15px;
            right: 15px;
            z-index: 100;
            outline: none !important;
        }
        .btn-item.bd-left{
            border-top-left-radius: 20px;
        }
        .btn-item.bd-top{
            border-top-right-radius: 20px;
        }
        .btn-item.bd-bottom{
            border-bottom-right-radius: 20px;
        }
        /*.scroll-bar::-webkit-scrollbar{*/
        /*    width: 14px;*/
        /*}*/
        main.main{
            max-width: 411px;
            margin: 0 auto;
        }
        .scroll-bar{
            max-height: 65vh;
            overflow: auto;
        }
    </style>
@stop
@section('style_page')
@stop

{{--content of page--}}
@section('content')
    <div class="content">
        <div class="header-page">
            <div class="d-flex align-center p-2" style="background: #EBEBEB;border-radius: 5px;">
                <button class="p-0 bg-transparent border-0 btn-search" value="{{$agency->slug}}" style="outline: none !important;">
                    <img src="{{url('assets/images/icon-search.svg')}}">
                </button>
                <input hidden name="category_id" value="{{$categories->id}}">
                <input class="border-0 w-100 bg-transparent" name="key_search" style="padding-left: 10px;color: #828282; font-size: 14px;outline: none" placeholder="Tìm kiếm sản phẩm">
            </div>
        </div>
        <div class="content-body" style="padding-bottom: 90px">
            <div class="d-flex justify-content-between">
                <div class="bg-white" style="width: 105px">
                    @foreach($category as $value)
                        <a href="{{url('danh-muc/'.$agency->slug.'/'. $value->id)}}" class="btn-item d-block w-100 text-center @if($value->id == $categories->id) active @endif">
                            {{$value->name}}
                        </a>
                    @endforeach
                </div>
                <div class="bg-white" style="width: calc(100% - 124px)">
                    <div class="w-100 d-flex justify-content-between" style="padding: 15px 0">
                        <p class="m-0" style="font-size: 12px; color: #000000 ">{{count($products)}} sản phẩm</p>
                        <p class="m-0" style="font-size: 12px; color: #F26091 ;font-weight: 600; padding-right: 29px">Gợi ý dành cho bạn</p>
                    </div>
                    <div class="scroll-bar list-item">
                        @foreach($products as $item)
                            <div class="position-relative" style="padding: 15px 0; border-bottom: 1px solid #DBDBDB;">
                                <a href="{{route('product', $item->slug)}}" class="d-flex justify-content-between">
                                    <img src="{{url($item->image)}}" style="width: 88px; height: 88px;object-fit: cover">
                                    <div class="position-relative" style="width: calc(100% - 106px)">
                                        <div>
                                            <p class="d-block title-product m-0">{{$item->name}}</p>
                                            <div class="m-0 d-flex align-items-center">
                                                <div style="width: 1px; height: 7px;background: #828282;margin-right: 2px"></div>
                                                <p class="m-0 desc-product">{{$item->name_category}}</p>
                                            </div>
                                            <p class="m-0 title-product">{{number_format($item->price)}}đ</p>
                                            {{--                                                <p class="m-0 desc-product">Đã bán 999</p>--}}
                                        </div>
                                    </div>
                                </a>
                                <button value="{{$item->slug}}" class="btn-add bg-transparent border-0 p-0 btn-plus"><img src="{{url('assets/images/btn-plus.svg')}}" width="15px"></button>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('plugins_js')
    @include('web.partials.plugins-js', ['slick'=>true, 'sweetalert'=>true])
@stop
@section('script_page')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{asset('dist/web/category/index.js')}}"></script>
@stop
