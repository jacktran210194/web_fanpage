@foreach($products as $item)
    <div class="position-relative" style="padding: 15px 0; border-bottom: 1px solid #DBDBDB;">
        <a href="{{route('product', $item->slug)}}" class="d-flex justify-content-between">
            <img src="{{url($item->image)}}" style="width: 88px; height: 88px;object-fit: cover">
            <div class="position-relative" style="width: calc(100% - 106px)">
                <div>
                    <p class="d-block title-product m-0">{{$item->name}}</p>
                    <div class="m-0 d-flex align-items-center">
                        <div style="width: 1px; height: 7px;background: #828282;margin-right: 2px"></div>
                        <p class="m-0 desc-product">{{$item->name_category}}</p>
                    </div>
                    <p class="m-0 title-product">{{number_format($item->price)}}đ</p>
                    {{--                                                <p class="m-0 desc-product">Đã bán 999</p>--}}
                </div>
            </div>
        </a>
        <button class="btn-add bg-transparent border-0 p-0 btn-plus" value="{{$item->slug}}"><img src="{{url('assets/images/btn-plus.svg')}}" width="15px"></button>
    </div>
@endforeach
