<!doctype html>
<html lang="vi">
<head>
    <base href="{{asset('')}}">
    <meta name="google-site-verification" content="googleeacc2166ce777ac3.html"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>{{$product->name??''}}</title>
    {{--Font for web--}}
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    {{--CSS Page--}}
    <link href="assets/images/logo_page.jpg" rel="icon">
    <link rel="stylesheet" type="text/css" href="{{asset('dist/web/home/home.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('dist/web/product/indext.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('dist/web/style.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>
    <style>
        *{
            font-family: 'Nunito', sans-serif;
        }
        .popup-form, .popup-slide {
            top: 100%;
            right: 50%;
            opacity: 0;
            z-index: -100;
            transition: .5s;
            background: #33333380;
            max-width: 411px;
            transform: translate(50%, 0);
        }

        .popup-slide.active {
            top: 0;
            opacity: 1;
            z-index: 10000;
        }
    </style>
</head>
<body>
{{--Header--}}
<header>
    <?php
    $token = session()->get('token_user');
    $user = \App\Models\User::where('token', $token)->first();
    $number_cart = \App\Models\CartModel::where('user_id', $user->id)->count();
    ?>
    <div class="header justify-content-between align-items-center" id="header">
        <a class="d-flex" href="{{url()->previous()}}">
            <img src="{{asset('assets/images/back.svg')}}" width="30px">
        </a>
        <div class="d-flex align-items-center">
            <a href="{{url('gio-hang/'.$agency->slug)}}" class="d-flex align-items-center position-relative mr-2">
                <img src="{{asset('assets/images/cart.svg')}}" width="20px" alt="">
                <p class="circle">{{$number_cart}}</p>
            </a>
            <a href="https://m.me/874919005985495?ref=chat" class="d-flex">
                <img src="{{asset('assets/images/chat.svg')}}" width="20px" alt="">
            </a>
        </div>
    </div>
</header>
{{--Content Page--}}
<main class="main">


    {{--content of page--}}
    <div class="content" style="padding-bottom: 60px;height: auto">
        <div class="d-flex">
            <img src="{{$product->image}}" class="w-100">
        </div>
        <div class="bg-white title-product">
            <h2 class="text-name-product">{{$product->name}}</h2>
            <div class="d-flex align-items-center" style="margin-bottom: 6px">
                <p class="m-0" style="font-size: 10px;">
                    <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="#FDD835" class="bi bi-star-fill" viewBox="0 0 16 16">
                        <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                    </svg>
                    4.8
                </p>
                <div style="width: 1px; background: #828282; height: 11px; margin: 0 5px"></div>
                <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="#828282" class="bi bi-basket2-fill" viewBox="0 0 16 16">
                    <path d="M5.929 1.757a.5.5 0 1 0-.858-.514L2.217 6H.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h.623l1.844 6.456A.75.75 0 0 0 3.69 15h8.622a.75.75 0 0 0 .722-.544L14.877 8h.623a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5h-1.717L10.93 1.243a.5.5 0 1 0-.858.514L12.617 6H3.383L5.93 1.757zM4 10a1 1 0 0 1 2 0v2a1 1 0 1 1-2 0v-2zm3 0a1 1 0 0 1 2 0v2a1 1 0 1 1-2 0v-2zm4-1a1 1 0 0 1 1 1v2a1 1 0 1 1-2 0v-2a1 1 0 0 1 1-1z"/>
                </svg>
                <p class="m-0" style="font-size: 10px;color: #828282;padding-left: 6px">Đã bán 999+</p>
            </div>
            <p class="m-0 text-price">{{number_format($product->price)}}đ</p>
        </div>
        <div class="desc-product position-relative">
            <p class="text-title">Mô tả sản phẩm</p>
            <div class="text-description">{!! $product->description !!}</div>
            <div class="read-more d-flex justify-content-center align-items-end">
                <button class="btn-read-more border-0 bg-transparent">Xem thêm <img src="{{url('assets/images/arrow-down.svg')}}" width="12px" class="arrow-down"></button>
            </div>
        </div>
        <div class="similar-product">
            <p class="text-title">Sản phẩm tương tự</p>
            <div class="data-product" style="margin-top: 11px">
                @foreach($similar_product as $item)
                    <div class="position-relative" style="padding: 15px 0; border-bottom: 1px solid #DBDBDB;">
                        <a href="{{route('product', $item->slug)}}" class="d-flex justify-content-between">
                            <img src="{{url($item->image)}}" style="width: 88px; height: 88px;object-fit: cover">
                            <div class="position-relative" style="width: calc(100% - 106px)">
                                <div>
                                    <p class="d-block name-product m-0">{{$item->name}}</p>
                                    <div class="m-0 d-flex align-items-center">
                                        <div style="width: 1px; height: 7px;background: #828282;margin-right: 2px"></div>
                                        <p class="m-0 content-product">{{$item->name_category}}</p>
                                    </div>
                                    <p class="m-0 name-product">{{number_format($item->price)}}đ</p>
                                    {{--                                                <p class="m-0 desc-product">Đã bán 999</p>--}}
                                </div>
                            </div>
                        </a>
                        <button value="{{$item->slug}}" class="btn-add bg-transparent border-0 p-0 btn-plus"><img src="{{url('assets/images/btn-plus.svg')}}" width="15px"></button>
                    </div>
                @endforeach
            </div>
            @if($similar_product->nextPageUrl())
            <button class="btn-load-more w-100 border-0">Xem thêm</button>
            @endif
            <div class="d-none number_page" data-value="{{$similar_product->lastPage()}}"></div>
            <input hidden name="slug" value="{{$product->slug}}">
        </div>
    </div>
    <div class="footer-product position-fixed">
        <?php
        $parent_id = session()->get('parent_id');
        $shop = \App\Models\AdminModel::find($parent_id);
        ?>
        <div class="d-flex">
            <div class="w-50 bg-transparent d-flex justify-content-center" style="padding: 13px 0">
                <a href="{{url('home/'.$shop->slug ?? '')}}" style="padding: 0 17px; border-right: 1px solid #DBDBDB;">
                    <img src="{{url('assets/images/home-product.svg')}}" style="width: 23px">
                </a>
                <button class="border-0 bg-transparent btn-chat">
                    <a href="https://m.me/874919005985495?ref=chat">
                        <img src="{{url('assets/images/messenger.svg')}}" style="width: 23px">
                    </a>
                </button>
                <button class="border-0 bg-transparent btn-add-to-cart" value="{{$product->id}}" style="padding: 0 17px;">
                    <img src="{{url('assets/images/cart-product.svg')}}" style="width: 23px">
                </button>
            </div>
            <button class="w-50 border-0 text-white btn-buy-now" value="{{$product->id}}">
                <img src="{{url('assets/images/smile.svg')}}" width="26px">
                Mua ngay
            </button>
        </div>
    </div>
    <div class="position-fixed loading">
        <img src="{{url('assets/images/load_more_2.gif')}}">
    </div>
    <div class="position-fixed w-100 h-100 d-flex justify-content-center align-items-end popup-slide"></div>
</main>
{{--Footer--}}

{{--JS Plugins--}}

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
{{--JS Page--}}
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script src="{{url('dist/web/product/index.js')}}"></script>
<script src="{{url('dist/web/home/check-login.js')}}" type="text/javascript"></script>
<script src="{{url('dist/web/product/show.js')}}" type="text/javascript"></script>
</body>
</html>


