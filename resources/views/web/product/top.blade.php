<!doctype html>
<html lang="vi">
<head>
    <base href="{{asset('')}}">
    <meta name="google-site-verification" content="googleeacc2166ce777ac3.html"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title> {{$w_title??''}}</title>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    {{--CSS Page--}}
    <link href="assets/images/logo_page.jpg" rel="icon">
    <link rel="stylesheet" type="text/css" href="{{asset('dist/web/style.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('dist/web/address/address.css')}}">
    <style>
        .header-page{
            padding: 21px 26px;
        }
        .btn-item {
            color: #000000;
            font-weight: 600;
            font-size: 14px;
            line-height: 19px;
            background: #F0F0F0;
            width: 100%;
            padding: 15px;
            border: 0;
            text-decoration: none !important;
        }
        .btn-item.active{
            color: #000000;
            background: #ffffff;
        }
        .title-product{
            font-size: 10px;
            font-weight: 700;
            color: #000000;
            line-height: 22px;
        }
        .desc-product{
            font-weight: 700;
            font-size: 7px;
            color: #828282;
        }
        .btn-plus{
            position: absolute;
            bottom: 15px;
            right: 15px;
            z-index: 100;
            outline: none !important;
        }
        .btn-item.bd-left{
            border-top-left-radius: 20px;
        }
        .btn-item.bd-top{
            border-top-right-radius: 20px;
        }
        .btn-item.bd-bottom{
            border-bottom-right-radius: 20px;
        }
        /*.scroll-bar::-webkit-scrollbar{*/
        /*    width: 14px;*/
        /*}*/
        main.main{
            max-width: 411px;
            margin: 0 auto;
        }
        .scroll-bar{
            max-height: 65vh;
            overflow: auto;
        }
    </style>
</head>
<body>
<main class="main" style="height: auto">
    <?php
    $token = session()->get('token_user');
    $user = \App\Models\User::where('token', $token)->first();
    $number_cart = \App\Models\CartModel::where('user_id', $user->id)->count();
    ?>
    <div class="header-page" id="header">
        <div class="d-flex justify-content-between align-items-center">
            <a href="{{url('home/'.$agency->slug)}}" class="d-flex">
                <img src="{{asset('assets/images/back.svg')}}" width="40px">
            </a>
            <p class="m-0 text-white" style="font-size: 20px;font-weight: 700;letter-spacing: -0.165px;">Top Sản Phẩm</p>
            <div class="d-flex align-items-center">
                <a href="{{url('gio-hang/'.$agency->slug)}}" class="d-flex align-items-center position-relative mr-2">
                    <img src="{{asset('assets/images/cart.svg')}}" width="20px" alt="">
                    <p class="circle">{{$number_cart}}</p>
                </a>
                <a href="https://m.me/874919005985495?ref=chat">
                    <img src="{{asset('assets/images/chat.svg')}}" width="20px" alt="">
                </a>
            </div>
        </div>
        <div class="header-page">
            <div class="d-flex align-center p-2" style="background: #EBEBEB;border-radius: 5px;">
                <button class="p-0 bg-transparent border-0 btn-search" value="{{$agency->slug}}" style="outline: none !important;">
                    <img src="{{url('assets/images/icon-search.svg')}}">
                </button>
                <input class="border-0 w-100 bg-transparent" name="key_search" style="padding-left: 10px;color: #828282; font-size: 14px;outline: none" placeholder="Tìm kiếm sản phẩm">
            </div>
        </div>
    </div>
    <div class="content">
        <div class="content-body" style="padding-bottom: 90px">
            <div class="d-flex justify-content-between">
                <div class="bg-white w-100">
                    <div class="w-100 d-flex justify-content-between" style="padding: 15px 0">
                        <p class="m-0" style="font-size: 12px; color: #000000 ">{{count($products)}} sản phẩm</p>
{{--                        <p class="m-0" style="font-size: 12px; color: #F26091 ;font-weight: 600; padding-right: 29px">Gợi ý dành cho bạn</p>--}}
                    </div>
                    <div class="list-item">
                        @foreach($products as $item)
                            <div class="position-relative" style="padding: 15px 0; border-bottom: 1px solid #DBDBDB;">
                                <a href="{{route('product', $item->slug)}}" class="d-flex justify-content-between">
                                    <img src="{{url($item->image)}}" style="width: 88px; height: 88px;object-fit: cover">
                                    <div class="position-relative" style="width: calc(100% - 106px)">
                                        <div>
                                            <p class="d-block title-product m-0">{{$item->name}}</p>
                                            <div class="m-0 d-flex align-items-center">
                                                <div style="width: 1px; height: 7px;background: #828282;margin-right: 2px"></div>
                                                <p class="m-0 desc-product">Bé uống</p>
                                            </div>
                                            <p class="m-0 title-product">{{number_format($item->price)}}đ</p>
                                            {{--                                                <p class="m-0 desc-product">Đã bán 999</p>--}}
                                        </div>
                                    </div>
                                </a>
                                <button value="{{$item->slug}}" class="btn-add bg-transparent border-0 p-0 btn-plus"><img src="{{url('assets/images/btn-plus.svg')}}" width="15px"></button>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<div class="position-fixed w-100 h-100 d-flex justify-content-center align-items-end popup-slide"></div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script src="{{url('dist/web/home/check-login.js')}}" type="text/javascript"></script>
<script src="{{url('dist/web/product/show.js')}}" type="text/javascript"></script>
<script src="{{asset('dist/web/category/index.js')}}"></script>
@include('web.partials.plugins-js', ['slick'=>true, 'sweetalert'=>true])
</body>
</html>
