<!doctype html>
<html lang="vi">
<head>
    <base href="{{asset('')}}">
    <meta name="google-site-verification" content="googleeacc2166ce777ac3.html"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Chỉ số sức khỏa</title>
    {{--Font for web--}}
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
        rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400;500;600;700;800;900&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    {{--CSS Page--}}
    <link href="assets/images/logo_page.jpg" rel="icon">
    <link rel="stylesheet" type="text/css" href="{{asset('dist/web/home/home.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('dist/web/product/indext.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('dist/web/style.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
          integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
    {{--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <style>
        * {
            font-family: 'Nunito', sans-serif;
        }

        .chart {
            background: #FFFFFF;
            box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.12);
            border-radius: 16px;
            padding: 10px;
        }

        .update-health {
            background: linear-gradient(321.69deg, #0038FF 0%, #0094FF 100%);;
            border-radius: 16px;
            color: white;
            font-weight: 600;
            font-size: 15px;
            max-width: 282px;
            margin: 0 auto;
            text-align: center;
            padding: 10px;
            cursor: pointer;
            margin-bottom: 100px;
        }

        .health-select {
            padding: 15px 25px;
            font-weight: 700;
            font-size: 13px;
            cursor: pointer;
        }

        .active-select {
            background: linear-gradient(321.69deg, #0038FF 0%, #0094FF 100%);;
            border-radius: 8px;
            color: white;
        }

        .no-select {
            color: #828282;
        }

        .parameter {
            background: #FFFFFF;
            box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.12);
            border-radius: 8px;
            padding: 20px 30px;
            position: relative;
            top: -25px;
        }

        .time {
            color: #828282;
            font-weight: 400;
            font-size: 13px;
        }

        .parameter-content {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }

        .parameter-line {
            border-right: 2px solid #0094FF;
        }

        .name-header-health {
            font-style: normal;
            font-weight: 700;
            font-size: 15px;
            color: #FFFFFF;
            margin-bottom: 0;
        }

        .input-update {
            border: none;
            outline: none;
            border-bottom: 1px solid #01B2FF;
            padding: 0 25px;
        }

        .input-height-update {
            border: none;
            outline: none;
            border-bottom: 1px solid #FFB199;
            padding: 0 25px;
        }

        .input-weight-update {
            border: none;
            outline: none;
            border-bottom: 1px solid #0094FF;
            padding: 0 25px;
        }

        .input-head-update {
            border: none;
            outline: none;
            border-bottom: 1px solid #FF7A00;
            padding: 0 25px;
        }

        .btn-update-health {
            background: #01B2FF;
            border-radius: 16px;
            font-weight: 600;
            font-size: 15px;
            color: #FFFFFF;
            outline: unset;
            border: none;
            padding: 10px 90px;
        }
        .modal-backdrop{
            width: 100%!important;
            height: 100%!important;
        }
        @media (max-width: 1500px) {
            body{
                padding-right: 0!important;
            }
        }
    </style>
</head>
<body>
{{--Header--}}
<?php
$token = session()->get('token_user');
$user = \App\Models\User::where('token', $token)->first();
$number_cart = \App\Models\CartModel::where('user_id', $user->id)->count();
?>
<div id="header" class="header position-relative">
    <p class="name-header-health position-absolute" style="top: 50%; left: 50%;transform: translate(-50%,-50%)"> Chỉ số sức khỏe</p>
    <div class="w-100 d-flex justify-content-end">
        <div class="d-flex align-items-center">
            <a href="{{url('gio-hang/'.$agency->slug)}}" class="d-flex align-items-center position-relative mr-2">
                <img src="{{asset('assets/images/cart.svg')}}" alt="">
                <p class="circle">{{$number_cart}}</p>
            </a>
            <a href="https://m.me/874919005985495?ref=chat">
                <img src="{{asset('assets/images/chat.svg')}}" alt="">
            </a>
        </div>
    </div>
</div>
{{--Content Page--}}
<main class="main">
    <div class="content p-3" style="padding-bottom: 90px;height: auto;background-color: white">

        <div class="mt-2">
            <div class="d-flex">
                <a href="{{url('health-height')}}" style="text-decoration: none">
                    <p class="health-select no-select">CHIỀU CAO</p>
                </a>
                <a href="{{url('health-weight')}}" style="text-decoration: none">
                    <p class="health-select active-select">CÂN NẶNG</p>
                </a>
                <a href="{{url('account')}}" id="link_account" style="text-decoration: none">
                    <p class="health-select no-select">THÔNG TIN</p>
                </a>
            </div>
            <div class="d-flex justify-content-between align-items-center parameter">
                <div class="parameter-content parameter-line pr-5">
                    <p style="color: #0094FF;font-size: 23px;margin-bottom: 5px" class="birth"></p>
                    <span class="time">Khi sinh</span>
                </div>
                <div class="parameter-content parameter-line pr-5">
                    <p style="color: #0094FF;font-size: 23px;margin-bottom: 5px"><span class="present"></span><span
                            style="font-size: 20px">kg</span></p>
                    <span class="time">Hiện tại</span>
                </div>
                <div class="parameter-content">
                    <p style="color: #0094FF;font-size: 23px;margin-bottom: 5px"><span class="growth"></span><span
                            style="font-size: 16px">%</span></p>
                    <span class="time">Tăng chưởng</span>
                </div>
            </div>
        </div>

        <div class="chart mt-2">
            <div class="d-flex mb-3">
                <img src="{{asset('assets/images/weight.svg')}}" alt="">
                <div class="d-flex flex-column ml-3">
                    <span style="font-weight: 700;font-size: 14px;">Chỉ số cân nặng</span>
                    <span style="font-weight: 400;font-size: 12px;opacity: 0.3;">Tháng 3</span>
                </div>
            </div>
            <canvas id="myChart"></canvas>
        </div>

        <div class="update-health mt-5" data-bs-toggle="modal" data-bs-target="#exampleModal">Cập nhật chỉ số <img
                src="{{asset('assets/images/VectorNext.png')}}" alt=""></div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered w-100 h-100" role="document" style="max-width: 411px;margin-top: 0;margin-bottom: 0">
            <div class="modal-content w-100 h-100 overflow-auto" style="border-radius: 0">
                <div class="modal-header" style="border-bottom: none">
                    <h5 class="modal-title" id="exampleModalLabel"><img src="{{asset('assets/images/Chart.svg')}}"
                                                                        alt=""><span
                            style="font-weight: 700;font-size: 17px;color: #F26091;margin-left: 10px">CẬP NHẬP CHỈ SỐ</span>
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="w-100">
                        <div class="d-flex flex-column position-relative w-100">
                            <span style="font-weight: 400;font-size: 13px;color: #01B2FF; margin-bottom: 8px">Ngày cập nhập</span>
                            <input type="date" class="input-update date p-0 form-control" name="date" style="border-radius: 0">
                        </div>
                        <div class="d-flex flex-column mt-3 position-relative w-100">
                            <span style="font-weight: 400;font-size: 13px;color: #FFB199;">Chiều cao</span>
                            <input type="text" class="input-height-update height p-0 form-control" name="height" style="border-radius: 0">
                            <span
                                style="position: absolute;right: 0;top: 20px;font-weight: 600;font-size: 15px;color: #FFB199">Cm</span>
                        </div>
                        <div class="d-flex flex-column mt-3 position-relative w-100">
                            <span style="font-weight: 400;font-size: 13px;color: #0038FF;">Cân nặng</span>
                            <input type="text" class="input-weight-update weight p-0 form-control" name="weight" style="border-radius: 0">
                            <span
                                style="position: absolute;right: 0;top: 20px;font-weight: 600;font-size: 15px;color: #0094FF">Kg</span>
                        </div>
                        <div class="d-flex flex-column mt-3 position-relative w-100">
                            <span style="font-weight: 400;font-size: 13px;color: #FF7A00;">Chỉ số vòng đầu</span>
                            <input type="text" class="input-head-update p-0 form-control" style="border-radius: 0">
                            <span
                                style="position: absolute;right: 0;top: 20px;font-weight: 600;font-size: 15px;color: #FF7A00">Cm</span>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center mt-5">
                        <button class="btn-update-health" data-bs-dismiss="modal" aria-label="Close">Cập nhập <img src="{{asset('assets/images/VectorNext.png')}}"
                                                                                                                   alt="" class="ml-4"></button>
                    </div>
                    <img src="{{asset('assets/images/banner-health.png')}}" alt="" class="w-100" style="margin-top: 100px">
                </div>
            </div>
        </div>
    </div>

</main>
{{--Footer--}}
@include('web.partials.footer')
{{--JS Plugins--}}

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
{{--JS Page--}}
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8"
        crossorigin="anonymous"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script src="{{url('dist/web/home/check-login.js')}}" type="text/javascript"></script>
<script src="{{url('dist/web/health/index.js')}}" type="text/javascript"></script>
</body>
</html>


