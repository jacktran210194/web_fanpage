<style>
.header{
    width: 411px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    background: #01B2FF;
    padding: 20px 10px;
    margin: 0 auto;
}
    .circle{
        background-color: white;
        color: red;
        border-radius: 50%;
        width: 10px;
        height: 10px;
        font-style: normal;
        font-weight: 800;
        font-size: 7px;
        text-align: center;
        position: absolute;
        left: -5px;
        top: -5px;
    }
    .name{
        font-style: normal;
        font-weight: 700;
        font-size: 15px;
        color: #FFFFFF;
        margin-bottom: 0;
    }
</style>

<header>
    <?php
    $parent_id = session()->get('parent_id');
    $shop = \App\Models\AdminModel::find($parent_id);
    $token = session()->get('token_user');
    $user = \App\Models\User::where('token', $token)->first();
    $date_today = \Carbon\Carbon::now('Asia/Ho_Chi_Minh');
    $month = $date_today->diffInMonths($user->date_of_birth);
    $number_cart = \App\Models\CartModel::where('user_id', $user->id)->count();
    ?>
    <div id="header" class="header position-relative">
{{--        <img src="{{asset('assets/images/menu.svg')}}" alt="">--}}
        <a href="{{url('account')}}" class="position-absolute" style="top: 50%;left: 50%;transform: translate(-50%,-50%); text-decoration: none">
            <p class="m-0 text-uppercase text-white text-center" style="font-size: 16px;font-weight: 700">
                {{$user->name}}
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#fff" class="bi bi-caret-down-fill" viewBox="0 0 16 16">
                    <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                </svg>
            </p>
            <p class="name" style="text-align: center">{{$month}} tháng tuổi</p>
        </a>
{{--        <div style="width: 100%;">--}}
{{--            <p class="name" style="text-align: center">Bé {{$user->name}} - {{$month}} tháng tuổi</p>--}}
{{--        </div>--}}
        <div class="w-100 d-flex justify-content-end">
            <div class="d-flex align-items-center">
                <a href="{{url('gio-hang/'.$shop->slug)}}" class="d-flex align-items-center position-relative mr-2">
                    <img src="{{asset('assets/images/cart.svg')}}" alt="">
                    <p class="circle">{{$number_cart}}</p>
                </a>
                <a href="https://m.me/874919005985495?ref=chat">
                    <img src="{{asset('assets/images/chat.svg')}}" alt="">
                </a>
            </div>
        </div>
    </div>
</header>
