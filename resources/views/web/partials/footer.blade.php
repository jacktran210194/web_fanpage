<style>
    .footer {
        width: 411px;
        height: 80px;
        background: #FFFFFF;
        box-shadow: 0px -4px 36px rgba(0, 0, 0, 0.08);
        border-radius: 15px 15px 0px 0px;
        display: flex;
        align-items: center;
        justify-content: space-evenly;
        position: fixed;
        bottom: 0;
        cursor: pointer;
        left: 50%;
        transform: translateX(-50%);
        z-index: 110;
    }
    .footer-name{
        font-style: normal;
        font-weight: 600;
        font-size: 10px;
        margin-bottom: 0;
        padding-top: 5px;
    }
    .active{
        color: #01B2FF;
    }
</style>
<?php
    $parent_id = session()->get('parent_id');
    $shop = \App\Models\AdminModel::find($parent_id);
?>
<footer>
    <div class="footer">
        <a href="{{url('home/'.$shop->slug ?? '')}}" style="text-decoration: none !important;" class="d-flex flex-column justify-content-center align-items-center item-navige @if(isset($is_page) && $is_page == 'home') active @endif">
            <svg xmlns="http://www.w3.org/2000/svg" width="19" height="20" viewBox="0 0 19 20" fill="none">
                <mask id="mask0_1515_990" style="mask-type:alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="19" height="20">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0 0H18.8385V20H0V0Z" fill="white"/>
                </mask>
                <g mask="url(#mask0_1515_990)">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M10.7674 13.2909C11.8738 13.2909 12.7744 14.1958 12.7744 15.3081V18.1688C12.7744 18.4078 12.9637 18.5994 13.2063 18.605H14.9579C16.3381 18.605 17.4602 17.4834 17.4602 16.1051V7.99165C17.4538 7.51734 17.2304 7.07093 16.8472 6.77426L10.7885 1.88423C9.97525 1.23229 8.83758 1.23229 8.02154 1.88609L2.00419 6.7724C1.60628 7.07837 1.38297 7.52478 1.37838 8.00746V16.1051C1.37838 17.4834 2.50043 18.605 3.88071 18.605H5.64879C5.89783 18.605 6.1 18.405 6.1 18.1595C6.1 18.1056 6.10643 18.0516 6.11746 18.0005V15.3081C6.11746 14.2023 7.01253 13.2983 8.11068 13.2909H10.7674ZM14.9579 20.0002H13.1898C12.1771 19.9761 11.396 19.1716 11.396 18.169V15.3083C11.396 14.9651 11.1139 14.6861 10.7675 14.6861H8.11534C7.77624 14.688 7.49596 14.9679 7.49596 15.3083V18.1597C7.49596 18.2295 7.48677 18.2964 7.46747 18.3597C7.36822 19.2804 6.59078 20.0002 5.64885 20.0002H3.88077C1.74051 20.0002 0 18.2527 0 16.1053V8.00116C0.0091896 7.07673 0.430073 6.23041 1.15697 5.67333L7.16238 0.795397C8.48476 -0.264822 10.3273 -0.264822 11.6469 0.793537L17.6955 5.67612C18.4059 6.2239 18.8267 7.06836 18.8387 7.98163V16.1053C18.8387 18.2527 17.0982 20.0002 14.9579 20.0002Z"
                          fill="@if(isset($is_page) && $is_page == 'home') #01B2FF @else #828282 @endif"/>
                </g>
            </svg>
            <p class="footer-name">Trang chủ</p>
        </a>
        <a href="{{url('danh-muc/'.$shop->slug)}}" class="item-navige @if(isset($is_page) && $is_page == 'category') active @endif" style="text-decoration: none!important;">
            <div class="d-flex flex-column justify-content-center align-items-center">
                @if(isset($is_page) && $is_page == 'category')
                    <svg xmlns="http://www.w3.org/2000/svg" width="19" height="20" viewBox="0 0 19 20" fill="none">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M12.6404 15.042H5.47393C5.063 15.042 4.72949 14.7044 4.72949 14.2886C4.72949 13.8727 5.063 13.5352 5.47393 13.5352H12.6404C13.0513 13.5352 13.3849 13.8727 13.3849 14.2886C13.3849 14.7044 13.0513 15.042 12.6404 15.042Z" fill="#01B2FF"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M12.6404 10.8359H5.47393C5.063 10.8359 4.72949 10.4984 4.72949 10.0825C4.72949 9.66662 5.063 9.3291 5.47393 9.3291H12.6404C13.0513 9.3291 13.3849 9.66662 13.3849 10.0825C13.3849 10.4984 13.0513 10.8359 12.6404 10.8359Z" fill="#01B2FF"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M8.20755 6.63961H5.47296C5.06203 6.63961 4.72852 6.30209 4.72852 5.88621C4.72852 5.47033 5.06203 5.13281 5.47296 5.13281H8.20755C8.61848 5.13281 8.95199 5.47033 8.95199 5.88621C8.95199 6.30209 8.61848 6.63961 8.20755 6.63961Z" fill="#01B2FF"/>
                        <mask id="mask0_1835_736" style="mask-type:alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="19" height="20">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M0.0185547 0H18.0486V20H0.0185547V0Z" fill="white"/>
                        </mask>
                        <g mask="url(#mask0_1835_736)">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M12.8316 1.50684L5.19963 1.51085C2.88889 1.52492 1.5072 2.97144 1.5072 5.38131V14.619C1.5072 17.0449 2.90179 18.4934 5.23537 18.4934L12.8674 18.4904C15.1781 18.4764 16.5598 17.0278 16.5598 14.619V5.38131C16.5598 2.95537 15.1662 1.50684 12.8316 1.50684ZM5.2366 20.0002C2.1159 20.0002 0.0185547 17.8374 0.0185547 14.6189V5.38127C0.0185547 2.13362 2.05039 0.0231042 5.19491 0.00401812L12.8309 0H12.8319C15.9526 0 18.0489 2.16275 18.0489 5.38127V14.6189C18.0489 17.8656 16.0171 19.9771 12.8726 19.9972L5.2366 20.0002Z" fill="#01B2FF"/>
                        </g>
                    </svg>
                @else
                    <svg xmlns="http://www.w3.org/2000/svg" width="19" height="20" viewBox="0 0 19 20" fill="none">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M12.6219 15.042H5.45538C5.04445 15.042 4.71094 14.7044 4.71094 14.2886C4.71094 13.8727 5.04445 13.5352 5.45538 13.5352H12.6219C13.0328 13.5352 13.3663 13.8727 13.3663 14.2886C13.3663 14.7044 13.0328 15.042 12.6219 15.042Z" fill="#828282"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M12.6219 10.8359H5.45538C5.04445 10.8359 4.71094 10.4984 4.71094 10.0825C4.71094 9.66662 5.04445 9.3291 5.45538 9.3291H12.6219C13.0328 9.3291 13.3663 9.66662 13.3663 10.0825C13.3663 10.4984 13.0328 10.8359 12.6219 10.8359Z" fill="#828282"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M8.189 6.63961H5.4544C5.04347 6.63961 4.70996 6.30209 4.70996 5.88621C4.70996 5.47033 5.04347 5.13281 5.4544 5.13281H8.189C8.59993 5.13281 8.93344 5.47033 8.93344 5.88621C8.93344 6.30209 8.59993 6.63961 8.189 6.63961Z" fill="#828282"/>
                        <mask id="mask0_1515_995" style="mask-type:alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="19" height="20">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M0 0H18.0301V20H0V0Z" fill="white"/>
                        </mask>
                        <g mask="url(#mask0_1515_995)">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M12.8131 1.50684L5.18108 1.51085C2.87033 1.52492 1.48865 2.97144 1.48865 5.38131V14.619C1.48865 17.0449 2.88324 18.4934 5.21681 18.4934L12.8488 18.4904C15.1596 18.4764 16.5413 17.0278 16.5413 14.619V5.38131C16.5413 2.95537 15.1477 1.50684 12.8131 1.50684ZM5.21804 20.0002C2.09734 20.0002 0 17.8374 0 14.6189V5.38127C0 2.13362 2.03183 0.0231042 5.17635 0.00401812L12.8123 0H12.8133C15.934 0 18.0304 2.16275 18.0304 5.38127V14.6189C18.0304 17.8656 15.9986 19.9771 12.854 19.9972L5.21804 20.0002Z"
                                  fill=" #828282"/>
                        </g>
                    </svg>
                @endif
                <p class="footer-name">Danh mục</p>
            </div>
        </a>
        <a href="{{url('gio-hang/'.$shop->slug)}}" style="text-decoration: none !important;" class="d-flex flex-column justify-content-center align-items-center item-navige @if(isset($is_page) && $is_page == 'cart') active @endif">
            @if(isset($is_page) && $is_page == 'cart')
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-bag-dash" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M5.5 10a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H6a.5.5 0 0 1-.5-.5z"/>
                    <path d="M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1zm3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4h-3.5zM2 5h12v9a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V5z"/>
                </svg>
                @else
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="#828282" class="bi bi-bag-dash" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M5.5 10a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H6a.5.5 0 0 1-.5-.5z"/>
                    <path d="M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1zm3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4h-3.5zM2 5h12v9a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V5z"/>
                </svg>
            @endif
            <p class="footer-name">Đơn hàng</p>
        </a>
{{--        <div style="text-decoration: none !important;" class="d-flex flex-column justify-content-center align-items-center">--}}
{{--            <img src="{{asset('assets/images/Notification.svg')}}" alt="">--}}
{{--            <p class="footer-name" style="color: #828282">Thông báo</p>--}}
{{--        </div>--}}
        <a href="{{url('health-height')}}" style="text-decoration: none !important;" class="d-flex text-black flex-column justify-content-center align-items-center item-navige @if(isset($is_page) && $is_page == 'chi-so') active @endif">
            @if(isset($is_page) && $is_page == 'chi-so')
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="20" viewBox="0 0 16 20" fill="none">
                    <mask id="mask0_1835_901" style="mask-type:alpha" maskUnits="userSpaceOnUse" x="0" y="12" width="16" height="8">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M0.0185547 12.5781H15.7726V20.0004H0.0185547V12.5781Z" fill="white"/>
                    </mask>
                    <g mask="url(#mask0_1835_901)">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M7.89698 14.0879C3.65909 14.0879 1.51081 14.8247 1.51081 16.2791C1.51081 17.7467 3.65909 18.4905 7.89698 18.4905C12.1339 18.4905 14.2812 17.7537 14.2812 16.2993C14.2812 14.8317 12.1339 14.0879 7.89698 14.0879ZM7.8966 20.0004C5.94822 20.0004 0.0185547 20.0004 0.0185547 16.2792C0.0185547 12.9616 4.51504 12.5781 7.8966 12.5781C9.84497 12.5781 15.7726 12.5781 15.7726 16.2993C15.7726 19.6169 11.2772 20.0004 7.8966 20.0004Z" fill="#01B2FF"/>
                    </g>
                    <mask id="mask1_1835_901" style="mask-type:alpha" maskUnits="userSpaceOnUse" x="2" y="0" width="12" height="11">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M2.61426 0H13.1767V10.6881H2.61426V0Z" fill="white"/>
                    </mask>
                    <g mask="url(#mask1_1835_901)">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M7.89642 1.4375C5.76703 1.4375 4.03447 3.18989 4.03447 5.3449C4.02751 7.49286 5.74714 9.24424 7.86758 9.2523L7.89642 9.97097V9.2523C10.0248 9.2523 11.7564 7.4989 11.7564 5.3449C11.7564 3.18989 10.0248 1.4375 7.89642 1.4375ZM7.89647 10.6885H7.86465C4.9585 10.6794 2.60434 8.28083 2.61429 5.34172C2.61429 2.39758 4.98336 0 7.89647 0C10.8086 0 13.1767 2.39758 13.1767 5.34474C13.1767 8.2919 10.8086 10.6885 7.89647 10.6885Z" fill="#01B2FF"/>
                    </g>
                </svg>
                @else
                <img src="{{asset('assets/images/Profile.svg')}}" alt="">
            @endif
            <p class="footer-name">Chỉ số</p>
        </a>
    </div>
</footer>
