@extends('web.layout.master')
@section('title','Giỏ hàng')
{{--meta--}}
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content="Trang chủ">
@stop

@section('style_page')
    <link rel="stylesheet" type="text/css" href="{{asset('dist/web/cart/cart.css')}}">
@stop

{{--content of page--}}
@section('content')
    <input hidden name="parent_id" value="{{$agency->id}}">
    <div class="cart">
        <div class="popup">
            <p class="mb-0 popup-title">Giao hàng tận nơi</p>
            <div class="d-flex justify-content-between" style="padding: 10px 20px">
                <p class="go mb-0">Giao tới</p>
                <p class="address mb-0">Chọn địa chỉ</p>
            </div>
            <div style="padding: 0 20px;margin-bottom: 5px">
                <img src="{{asset('assets/images/people.svg')}}" alt="">
                <span class="name-address">Toản</span>
            </div>
            <div style="padding: 0 20px;margin-bottom: 5px">
                <img src="{{asset('assets/images/bx_map.svg')}}" alt="">
                <span class="name-address">Phúc Tân, Hoàn Kiếm, Hanoi, Vietnam</span>
            </div>
            <div style="padding: 0 20px;margin-bottom: 5px;padding-bottom: 10px">
                <img src="{{asset('assets/images/phone.svg')}}" alt="">
                <span class="name-address">0962651287</span>
            </div>
        </div>

        <div class="list-sp">
            @foreach($data_cart as $value)
                <div class="d-flex w-100 item-cart" style="border-bottom: 1px solid #EBEBEB;padding: 10px 0">
                    <input name="product_id" hidden value="{{$value->data_product->id}}">
                    <img src="{{$value->data_product->image}}" alt=""
                         style="width: 117px;height: 117px;margin-right: 15px;object-fit: cover;border-radius: 8px;">
                    <div style="width: calc(100% - 122px)">
                        <p class="title-sp mb-1">{{$value->data_product->name}}</p>
                        <p class="gia mb-0">{{$value->total_money}}đ</p>
{{--                        <p class="giam-gia mb-0">850.000đ</p>--}}
                        <div class="d-flex justify-content-between align-items-center mt-2">
                            <div class="buttons_added">
                                <input class="minus is-form" type="button" value="-">
                                <input aria-label="quantity" class="input-qty" max="10000" min="1" name="" type="number"
                                       value="{{$value->quantity}}">
                                <input class="plus is-form" type="button" value="+">
                            </div>
                            <button value="{{$value->id}}" style="outline: none !important;"
                                    class="border-0 p-0 bg-transparent btn-delete-cart"><i class="fa-solid fa-trash-can"
                                                                                           style="color: #828282;cursor: pointer"></i>
                            </button>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="total-all">
            <span class="total">Tổng tiền hàng</span>
            <span class="total-gia"></span>
        </div>

        {{--        <div class="bonus">--}}
        {{--            <div class="d-flex align-items-center">--}}
        {{--                <img src="{{asset('assets/images/bx-badge-check.svg.svg')}}" alt="">--}}
        {{--                <span class="title-bonus">Dùng 150đ thưởng</span>--}}
        {{--            </div>--}}

        {{--        </div>--}}
        {{--        <div class="bonus d-flex justify-content-between align-items-center">--}}
        {{--            <div class="d-flex align-items-center">--}}
        {{--                <img src="{{asset('assets/images/bx-badge-check.svg.svg')}}" alt="">--}}
        {{--                <span class="title-bonus">Dùng 150đ thưởng</span>--}}
        {{--            </div>--}}
        {{--            <input type="checkbox" name="" class="switch-toggle">--}}
        {{--        </div>--}}
        {{--        <div class="accumulated d-flex justify-content-between">--}}
        {{--            <span class="title-bonus">Điểm thưởng tích được: </span>--}}
        {{--            <span class="diem-plus">+ 130đ</span>--}}
        {{--        </div>--}}
        <div class="d-flex justify-content-between align-items-center pay">
            <div>
                <p class="mb-1 pay-all">Tổng thánh toán</p>
                {{--                <p class="mb-1 pay-giam">1.300.000đ</p>--}}
                <p class="mb-1 pay-gia"></p>
            </div>
            <button class="btn-mua">Mua hàng</button>
        </div>
    </div>

@stop

@section('plugins_js')
    @include('web.partials.plugins-js', ['slick'=>true, 'sweetalert'=>true])
@stop
@section('script_page')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{url('dist/web/cart/index.js')}}" type="text/javascript"></script>
@stop
