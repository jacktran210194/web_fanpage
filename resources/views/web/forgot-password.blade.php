<!doctype html>
<html lang="vi">
<head>
    <base href="{{asset('')}}">
    <meta name="google-site-verification" content="googleeacc2166ce777ac3.html"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>{{'Quên mật khẩu'}}</title>
    {{--Font for web--}}
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
        rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400;500;600;700;800;900&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    {{--CSS Page--}}
    <link href="assets/images/logo_page.jpg" rel="icon">
    <link rel="stylesheet" type="text/css" href="{{asset('dist/web/home/home.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('dist/web/product/indext.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
          integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>
    <style>
        * {
            font-family: 'Nunito', sans-serif;
        }

        .name-page {
            font-style: normal;
            font-weight: 700;
            font-size: 50px;
            text-transform: uppercase;
            color: #D60013;
            text-align: center;
            margin-top: 30px;
        }
        .introduce{
            font-style: normal;
            font-weight: 700;
            font-size: 15px;
            text-align: center;
            text-transform: uppercase;
            color: #01B2FF;
            margin-top: 40px;
        }
        .note{
            font-weight: 400;
            font-size: 13px;
            color: #01B2FF;
        }
        .input-phone{
            width: 282px;
            border: none;
            outline: unset;
            border-bottom: 1px solid #01B2FF;
            padding-left: 25px;
        }

        .update-health {
            background: #01B2FF;
            border-radius: 16px;
            color: white;
            font-weight: 600;
            font-size: 15px;
            max-width: 282px;
            margin: 0 auto;
            text-align: center;
            padding: 10px;
            cursor: pointer;
            margin-bottom: 100px;
        }
        main {
            background: aliceblue;
            height: auto;
        }
        .loader{
            top: 0;
            right: 50%;
            z-index: -100;
            max-width: 425px;
            transform: translate(50%,0);
            background: linear-gradient(180deg, #01B2FF 0%, rgba(1, 178, 255, 0.4) 100%);
            padding-bottom: 20px;
            opacity: 0;
            transition: .7s;
        }
        .loader.active{
            opacity: 1;
            z-index: 100;
        }
        .div-banner{
            background-image: url({{asset('assets/images/baner_login_1.png')}});
            background-repeat: no-repeat;
            background-position: bottom;
            background-size: 80%;
        }
        .container-page{
            display: none;
            transition: .7s;
            max-width: 425px;
            margin: 0 auto;
            height: 100vh;
            background: #fff;
        }
        .bg-page{
            padding-bottom: 30px;
            background: linear-gradient(180deg, #01B2FF 0%, rgba(1, 178, 255, 0.38) 100%);
        }
        [name="phone"]{
            border-bottom: 1px solid #E1E3E8;
            border-top: none;
            border-left: none;
            border-right: none;
            width: 100%;
            outline: none;
        }
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        input[type=number] {
            -moz-appearance: textfield;
        }
        .btn-login,.btn-dn{
            font-size: 16px;
            background: linear-gradient(35.54deg, #01B2FF -7.25%, rgba(1, 178, 255, 0.4) 90.75%);
            margin-top: 33px;
            max-width: 315px;
            height: 54px;
            border-radius: 8px;
            border: none;
            outline: none;
        }
        .arrow-button{
            top: 50%;
            right: 18px;
            transform: translate(-50%,-50%);
        }
        .input-name {
            width: 100%;
            border: none;
            outline: unset;
            border-bottom: 1px solid #E1E3E8;
        }
    </style>
</head>
<body style="background: aliceblue">
<main class="main">
    <div class="container-page position-relative">
        {{--        <div class="bg-page">--}}
        {{--            <div class="d-flex justify-content-center" style="padding-top: 43px;padding-bottom: 79px">--}}
        {{--                <img src="{{asset('assets/images/baner_login_2.png')}}" width="200px">--}}
        {{--            </div>--}}
        {{--            <p class="m-0 text-white" style="font-weight: 700;font-size: 34px;padding-left: 38px">Chào bạn</p>--}}
        {{--        </div>--}}
        <div class="w-100">
            <img src="assets/images/banner_app.svg" class="w-100">
        </div>
        <div class="d-flex justify-content-center" style="background: #01B2FF;padding: 14px 20px 50px 35px;">
            <div class="w-100">
                <p class="mb-2 text-white" style="font-weight: 700;font-size: 14px">CHÀO MỪNG BẠN ĐẾN VỚI ỨNG DỤNG</p>
                <p class="mb-1" style="font-weight: 900;font-size: 20px;color: #FBFF50">PHÁT TRIỂN ĐA GIÁC QUAN</p>
                <p class="m-0 text-white" style="font-weight: 700;font-size: 14px;">THEO TỪNG THÁNG TUỔI CỦA ... <span></span></p>
            </div>
        </div>
        <form action="{{route('change-pass')}}" method="post" role="form" id="userFrom" enctype="multipart/form-dat">
            @csrf
            @if(session('alert'))
                <div class="alert alert-danger bg-danger text-light border-0 alert-dismissible fade show" role="alert">
                    {{session('message')}}
                </div>
            @endif
            <div class="w-100 bg-white" style="border-top-right-radius: 32px;border-top-left-radius: 32px;padding: 48px 38px;margin-top: -30px">
                <p style="font-size: 20px;margin-bottom: 14px;font-weight: 900">Hãy nhập sđt để bắt đầu!</p>
                <div style="margin-bottom: 20px">
                    <label style="font-size: 12px;color: #000000;">Số điện thoại</label>
                    <input type="number" name="phone" class="input-name">
                </div>
                <div style="margin-bottom: 20px">
                    <label style="font-size: 12px;color: #000000;">Mật khẩu mới</label>
                    <input type="password" name="password" required class="input-name">
                </div>
                <div class="d-flex justify-content-center">
                    <button type="submit" class="text-white btn-dn d-flex align-items-center justify-content-center position-relative" style="width: 90%">
                        Cập Nhật
                        <div class="position-absolute arrow-button">
                            <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="#fff" class="bi bi-arrow-right" viewBox="0 0 16 16">
                                <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
                            </svg>
                        </div>
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="loader position-fixed w-100 h-100 active">
        <div class="h-100 w-100 div-banner">
            <div style="padding-top: 150px">
                {{--                <p class="m-0 text-white text-center" style="font-weight: bold;font-style: italic;font-size: 30px;">{{$agency->name_shop}}</p>--}}
                <p class="text-white text-center" style="font-size: 20px;opacity: 0.61;width: 90%;margin: 0 auto">Hơn 1000 bài tập kỹ năng cho bé thông minh cao lớn hơn</p>
            </div>
        </div>
    </div>
</main>
{{--Footer--}}

{{--JS Plugins--}}

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
{{--JS Page--}}
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    setTimeout(function () {
        $(".loader").removeClass("active");
        $(".container-page").css("display", "block");
    }, 700)
</script>
</body>
</html>


