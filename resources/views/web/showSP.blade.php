<div id="carouselExampleControls" class="carousel slide slide-room" data-ride="carousel" >
    <button class="bg-transparent border-0 close-popup position-absolute p-0" style="top: 20px;right: 25px;z-index: 100;outline-color: white">
        <i class="fa-solid fa-xmark" style="color: #263238;font-size: 24px"></i>
    </button>
    <div id="show-product" class="carousel-inner">
        <p class="header-mua mb-0">Chọn số lượng</p>
        <div class="d-flex align-items-center" style="padding: 20px; border-bottom: 1px solid #E0E0E0">
            <img src="{{$product->image}}" width="60px" height="60px" style="object-fit: cover" alt="">
            <p class="titleSP mb-0 ml-3">{{$product->name}}</p>
        </div>
        <div class="info d-flex justify-content-between">
            <div>
                <p class="mb-0 loai">1 lọ</p>
                <p class="mb-0 price">{{number_format($product->price)}}đ</p>
            </div>
            <div class="d-flex justify-content-end align-items-center">
                <input hidden name="slug_product" value="{{$product->slug}}">
                <button class="btn-minas-quantity p-0 border-0 bg-transparent" style="outline: none !important;"><img src="{{url('assets/images/btn-minas.svg')}}" width="24px"></button>
                <input name="quantity" type="number" class="text-center" value="1" style="border: 1px solid #828282;border-radius: 5px;width: 54px;height: 25px;margin: 0 10px;padding: 2px">
                <button class="btn-plus-quantity p-0 border-0 bg-transparent" style="outline: none !important;"><img src="{{url('assets/images/btn-plus.svg')}}" width="24px"></button>
            </div>
        </div>
        <div class="select-mua">
            <div class="d-flex justify-content-between">
                <p class="tam-tinh">Tạm tính</p>
                <p class="gia-tam">0đ</p>
            </div>
            <div class="d-flex justify-content-between">
                <button value="{{$product->id}}" class="add-cart">Thêm vào giỏ</button>
                <button value="{{$product->id}}" class="add-sale"><img src="{{asset('assets/images/mua.svg')}}" alt="">Mua ngay</button>
            </div>
        </div>
    </div>
</div>
