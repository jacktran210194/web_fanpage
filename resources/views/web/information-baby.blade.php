<!doctype html>
<html lang="vi">
<head>
    <base href="{{asset('')}}">
    <meta name="google-site-verification" content="googleeacc2166ce777ac3.html"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>{{$product->name??''}}</title>
    {{--Font for web--}}
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
        rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400;500;600;700;800;900&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    {{--CSS Page--}}
    <link href="assets/images/logo_page.jpg" rel="icon">
    <link rel="stylesheet" type="text/css" href="{{asset('dist/web/home/home.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('dist/web/product/indext.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
          integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>
    <style>
        * {
            font-family: 'Nunito', sans-serif;
        }

        .header-information {
            background: linear-gradient(35.54deg, #01B2FF -7.25%, rgba(1, 178, 255, 0.4) 90.75%);
            padding: 48px 0;
        }

        .content-infomation {
            width: 411px;
            padding-top: 40px;
            position: absolute;
            border-top-left-radius: 16px;
            border-top-right-radius: 16px;
            background-color: white;
            top: 85px;
        }

        .title-information {
            font-weight: 700;
            font-size: 17px;
            text-align: center;
            color: #FFFFFF;
            margin-bottom: 5px;
            padding-top: 20px;
        }

        .name-information {
            font-weight: 400;
            font-size: 13px;
            text-align: center;
            color: #FFFFFF;
            margin-bottom: 0px;
            padding-bottom: 40px;
        }

        .select-boy {
            background: #01B2FF;
            border-radius: 20px;
            font-weight: 600;
            font-size: 13px;
            color: #FFFFFF;
            width: 117px;
            padding: 10px;
        }

        .select-girl {
            background: #DEDEDE;
            border-radius: 20px;
            font-weight: 600;
            font-size: 13px;
            width: 117px;
            padding: 10px;
            color: #828282;
        }

        .input-update {
            border: none;
            outline: none;
            border-bottom: 1px solid #01B2FF;
            padding: 0 25px;
        }

        .input-height-update {
            border: none;
            outline: none;
            border-bottom: 1px solid #FFB199;
            padding: 0 25px;
        }

        .input-weight-update {
            border: none;
            outline: none;
            border-bottom: 1px solid #0094FF;
            padding: 0 25px;
        }

        .input-name {
            width: 100%;
            border: none;
            outline: unset;
            border-bottom: 1px solid #E1E3E8;
        }

        .note-name {
            font-weight: 400;
            font-size: 13px;
            color: #01B2FF;
            margin-bottom: 8px;
        }

        .update-health {
            background: #01B2FF;
            border-radius: 16px;
            color: white;
            font-weight: 600;
            font-size: 15px;
            max-width: 282px;
            margin: 0 auto;
            text-align: center;
            padding: 10px;
            cursor: pointer;
            margin-bottom: 100px;
        }

        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
        main {
            background: aliceblue;
            height: auto;
        }

        .select-gt input[type="radio"] {
            display: none;
        }

        .select-gt input[type="radio"]:checked + .select-girl {
            background: #01B2FF;
            color: #FFFFFF;
        }
        .div-content{
            border-top-right-radius: 32px;
            border-top-left-radius: 32px;
            padding: 48px 38px;
            margin-top: -60px;
            background: #ffffff;
        }
        .update-health{
            font-size: 16px;
            background: linear-gradient(35.54deg, #01B2FF -7.25%, rgba(1, 178, 255, 0.4) 90.75%);
            margin-top: 33px;
            max-width: 315px;
            height: 54px;
            border-radius: 8px;
            border: none;
            outline: none;
        }
        .arrow-button{
            top: 50%;
            right: 18px;
            transform: translate(-50%,-50%);
        }
    </style>
</head>
<body>
<main class="main">
    <div class="content bg-white">
        <div class="header-information d-flex justify-content-center">
            <img src="{{asset('assets/images/baner_login_2.png')}}" width="200px">
        </div>
        <div class="div-content">
            <input name="slug" hidden value="{{$agency->slug}}">
            <p class="m-0" style="font-weight: 700;font-size: 34px;color: #3A3A3A">Bước cuối nào</p>
            <p style="font-size: 20px;color: #000000;opacity: 0.61;line-height: 25px;">Hãy nhập ngày tháng năm sinh để hoàn thành</p>
{{--            <div class="d-flex justify-content-between align-items-center">--}}
{{--                <label class="select-gt">--}}
{{--                    <input type="radio" checked name="gioitinh" value="0"/>--}}
{{--                    <div class="select-girl">--}}
{{--                        <img src="{{asset('assets/images/boy.png')}}" alt="">--}}
{{--                        <span class="ml-2">Bé Trai</span>--}}
{{--                    </div>--}}
{{--                </label>--}}
{{--                <label class="select-gt">--}}
{{--                    <input type="radio" name="gioitinh" value="1"/>--}}
{{--                    <div class="select-girl">--}}
{{--                        <img src="{{asset('assets/images/girl.png')}}" alt="">--}}
{{--                        <span class="ml-2">Bé Gái</span>--}}
{{--                    </div>--}}
{{--                </label>--}}
{{--            </div>--}}
            <div>
                <div class="mt-5">
                    <div style="margin-bottom: 20px">
                        <label style="font-size: 12px;color: #000000;">Tên của bé thường gọi ở nhà</label>
                        <input type="text" name="name" class="input-name">
                    </div>
                    <div style="margin-bottom: 20px">
                        <label style="font-size: 12px;color: #000000;">Ngày tháng năm sinh</label>
                        <input type="date"  name="date" class="input-name date">
                    </div>
                    <div style="margin-bottom: 20px">
                        <label style="font-size: 12px;color: #000000;">Chiều cao khi sinh(Cm)</label>
                        <input type="number"  name="height" class="input-name">
                    </div>
                    <div style="margin-bottom: 20px">
                        <label style="font-size: 12px;color: #000000;">Cân nặng khi sinh(Kg)</label>
                        <input type="number"  name="weight" class="input-name">
                    </div>
                    <div class="d-flex justify-content-center">
                        <button class="text-white update-health d-flex align-items-center justify-content-center position-relative" style="width: 90%">
                            TIẾP THEO
                            <div class="position-absolute arrow-button">
                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="#fff" class="bi bi-arrow-right" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
                                </svg>
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
{{--Footer--}}

{{--JS Plugins--}}

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
{{--JS Page--}}
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script src="{{url('dist/web/home/check-login.js')}}"></script>
<script src="{{url('dist/web/home/info-baby.js')}}"></script>
</body>
</html>


