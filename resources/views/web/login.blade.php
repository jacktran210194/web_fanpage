<!doctype html>
<html lang="vi">
<head>
    <base href="{{asset('')}}">
    <meta name="google-site-verification" content="googleeacc2166ce777ac3.html"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>{{$product->name??''}}</title>
    {{--Font for web--}}
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    {{--CSS Page--}}
    <link href="assets/images/logo_page.jpg" rel="icon">
    <link rel="stylesheet" type="text/css" href="{{asset('dist/web/home/home.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('dist/web/product/indext.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>
    <style>
        *{
            font-family: 'Nunito', sans-serif;
        }
        .form-input{
            border: 1px solid #BEBAB3;
            border-radius: 12px;
            padding: 16px;
            color: #78746D;
            font-size: 14px;
            width: 100%;
            margin-bottom: 16px;
        }
        .btn-login{
            font-weight: 500;
            font-size: 16px;
            color: #ffffff;
            background: #01B2FF;
            border-radius: 16px;
            padding: 16px 0;
            width: 100%;
            border: none;
            outline: none !important;
        }
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        input[type=number] {
            -moz-appearance: textfield;
        }
        .form-login{
            padding: 16px;
        }
        main{
            background: aliceblue;
        }
    </style>
</head>
<body>
<main class="main">
    <div class="content bg-white">
        <div class="d-flex" style="padding: 60px 16px 16px 16px">
            <img src="{{url('assets/images/baner-login-page.png')}}" class="w-100">
        </div>
        <p class="text-center" style="font-size: 24px;color: #3C3A36;margin-bottom: 16px">Chào mừng bạn</p>
        <div class="form-login">
            <input class="form-input" name="name" type="text" placeholder="Tên">
            <input class="form-input" name="date" type="date" placeholder="Tên">
            <input class="form-input" name="phone" type="number" placeholder="Số điện thoại">
            <button class="btn-login">Tiếp tục</button>
        </div>
    </div>
</main>
{{--Footer--}}

{{--JS Plugins--}}

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
{{--JS Page--}}
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script src="{{url('dist/web/home/login.js')}}"></script>
</body>
</html>


