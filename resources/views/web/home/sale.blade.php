<section class="sale">
    <div class="category-header">
        <div>
            <img src="{{asset('assets/images/sale.svg')}}" alt="">
            <span class="title-category">FLASH SALE</span>
        </div>
        <span class="see-category">Xem thêm  ></span>
    </div>
    <div class="d-flex justify-content-center mb-3">
        <img src="{{asset('assets/images/banner.png')}}" alt="" style="width: 373px;height: 140px">
    </div>
    <div class="slide-sale-product">
        @for($i=0;$i<9;$i++)
            <div class="d-flex flex-column ml-1">
                <img src="{{asset('assets/images/prosale.png')}}" alt="">
                <p class="sale-title">LineaBon D3K2</p>
                <p class="sale-content">|Bé uống</p>
                <div class="d-flex justify-content-between">
                    <div>
                        <p class="price-sale">295.000đ/lọ</p>
                        <p class="sold">Đã bán 999</p>
                    </div>
                    <p class="add show-sp">+</p>
                </div>
            </div>
        @endfor
    </div>
    <div class="d-flex justify-content-center">
        <button class="btn-more">Xem thêm</button>
    </div>

</section>
