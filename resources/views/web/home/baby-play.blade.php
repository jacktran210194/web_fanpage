<section class="baby-drink mt-2">
    <div class="category-header">
        <div>
            <img src="{{asset('assets/images/play.svg')}}" alt="">
            <span class="title-category">BÉ CHƠI</span>
        </div>
        <span class="see-category">Xem thêm  ></span>
    </div>
    <div class="d-flex justify-content-center mb-3">
        <img src="{{asset('assets/images/banner.png')}}" alt="" style="width: 373px;height: 140px">
    </div>
    <div class="d-flex flex-column justify-content-center align-items-center">
        @for($i=0;$i<4;$i++)
        <div class="list-product">
            <img src="{{asset('assets/images/prosale.png')}}" alt="" style="width: 87px;height: 87px">
            <div class="ml-3" style="width: 356px">
                <p class="sale-title">LineaBon D3K2</p>
                <p class="sale-content">|Bé uống</p>
                <p class="price-sale">295.000đ/lọ</p>
                <p class="sold">Đã bán 999</p>
                <p class="add-pro show-sp">+</p>
            </div>
        </div>
        @endfor
    </div>
    <div class="d-flex justify-content-center">
        <button class="btn-more">Xem thêm</button>
    </div>

</section>
