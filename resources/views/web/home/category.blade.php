<section class="category">
        <div class="category-header">
            <span class="title-category">DANH MỤC SẢN PHẨM</span>
            <a href="{{url('danh-muc/'.$agency->slug)}}" class="see-category">Xem thêm  ></a>
        </div>

        <div class="slide-category-product">
            @foreach($category as $value)
                <a href="{{url('danh-muc/'.$agency->slug.'/'. $value->id)}}" class="d-flex flex-column align-items-center mr-4" style="height: 113px;text-decoration: none !important;">
                    <img src="{{$value->src}}" alt="" style="width: 60px;height: 60px;border-radius: 8px;object-fit: cover">
                    <p class="title-category-product mt-2 mb-1 text-center">{{$value->name}}</p>
                </a>
            @endforeach
        </div>
</section>
