<section class="topProduct">
    <div class="productHot">
        <div class="productHot-header">
            <div class="">
                <img src="{{asset('assets/images/HOT.svg')}}" alt="">
                <span class="topSP">Top sản phẩm</span>
            </div>
            <a href="{{url('top-san-pham/'.$agency->slug)}}" class="see d-flex align-items-center">Xem thêm  ></a>
        </div>

        <div class="slide-top-product">
            @foreach($top_product as $top)
                <a href="{{url('san-pham/'.$top->slug)}}" class="d-flex flex-column align-items-center" style="margin-right: 5px; text-decoration: none !important;">
                    <img src="{{$top->image}}" style="width: 81px;height: 81px;border-radius: 3px;object-fit: cover" alt="">
                    <p class="price-pr">{{$top->price}}Đ</p>
                    <div class="d-flex">
                        <img src="{{asset('assets/images/Vector.svg')}}" alt="">
                        <span class="people">900</span>
                    </div>
                </a>
            @endforeach
        </div>
    </div>


</section>
