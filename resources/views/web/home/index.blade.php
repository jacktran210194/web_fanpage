@extends('web.layout.master')
@section('title','Trang chủ')
{{--meta--}}
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content="Trang chủ">
@stop

@section('plugins_css')
    @include('web.partials.plugins-css', ['slick'=>true, 'sweetalert'=>true])
@stop
@section('style_page')
    <link rel="stylesheet" type="text/css" href="{{asset('dist/web/home/home.css')}}">
@stop
<style>
    .btn-plus{
        position: absolute;
        bottom: 15px;
        right: 15px;
        z-index: 100;
        outline: none !important;
    }
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }
    .popup-lesson{
        background: #ffffff;
        top: 100%;
        right: 50%;
        max-width: 411px;
        transform: translate(50%, 0);
        z-index: -100;
        transition: .5s;
        opacity: 0;
    }
    .popup-lesson.active{
        opacity: 1;
        z-index: 10000;
        top: 0;
    }
    .price-pr{
        font-weight: 700;
        font-size: 13px;
        line-height: 18px;
        color: #FFFFFF;
        margin-bottom: 4px;
    }
</style>
{{--content of page--}}
@section('content')
    <div class="content">
        @include('web.home.program')
        @include('web.home.top-product')
        @include('web.home.category')
        <div class="list-category">
            @foreach($category as $value)
                <section class="baby-drink">
                    <div class="category-header">
                        <div>
                            <img src="assets/images/drink.svg" alt="">
                            <span class="title-category">{{$value->name}}</span>
                        </div>
                        <a href="{{url('danh-muc/'.$value->id)}}" class="see-category">Xem thêm  ></a>
                    </div>
                    <div class="d-flex justify-content-center mb-3">
                        <img src="{{$value->banner}}" alt="" style="width: 373px;height: 140px;object-fit: cover;border-radius: 10px;">
                    </div>
                    <div class="d-flex flex-column justify-content-center align-items-center">
                        @foreach($value->products as $item)
                            <div class="list-product position-relative">
                                <a class="d-flex w-100" href="{{url('san-pham/'.$item->slug)}}" style="text-decoration: none !important;">
                                    <img src="{{$item->image}}" alt="" style="width: 87px;height: 87px;object-fit: cover">
                                    <div style="width: calc(100% - 104px);margin-left: 17px">
                                        <p class="sale-title">{{$item->name}}</p>
                                        <p class="sale-content">|{{$item->name_category}}</p>
                                        <p class="price-sale">{{$item->price}}đ/lọ</p>
                                        <p class="sold">Đã bán 999</p>
                                    </div>
                                </a>
                                <button class="btn-add bg-transparent border-0 p-0 btn-plus" value="{{$item->slug}}"><img src="assets/images/btn-plus.svg" width="15px"></button>
                            </div>
                        @endforeach
                    </div>
                    <div class="d-flex justify-content-center">
                        <button class="btn-more"><a href="{{url('danh-muc/'.$value->id)}}">Xem thêm</a></button>
                    </div>
                </section>
            @endforeach
        </div>
    </div>
    <div class="position-fixed w-100 h-100 popup-lesson">

    </div>
@stop

@section('plugins_js')
    @include('web.partials.plugins-js', ['slick'=>true, 'sweetalert'=>true])
@stop
@section('script_page')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{asset('dist/web/home/home.js')}}"></script>
@stop
