<?php


namespace App\Console\Commands;


use App\Models\DistrictGhnModel;
use Illuminate\Console\Command;
use App\Models\WardGhnModel;
use Mockery\Exception;

class WardGhn extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ward:ghn';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import data ward giao hang nhanh';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $districts = DistrictGhnModel::all();
            foreach ($districts as $district){
                $data = json_decode($this->wardGhn($district->DistrictID));
                if (!empty($data->data) && count($data->data)> 0){
                    foreach ($data->data as $k => $item){
                        $ward = new WardGhnModel();
                        $ward->WardCode = $item->WardCode??'';
                        $ward->DistrictID = $item->DistrictID??'';
                        $ward->WardName = $item->WardName??'';
                        $ward->NameExtension = json_encode($item->NameExtension??['1', '2']);
                        $ward->IsEnable = $item->IsEnable??'';
                        $ward->CanUpdateCOD = $item->CanUpdateCOD??'';
                        $ward->SupportType = $item->SupportType??'';
                        $ward->Status = $item->Status??'';
                        $ward->ReasonCode = $item->ReasonCode??'';
                        $ward->ReasonMessage = $item->ReasonMessage??'';

                        $ward->save();
                        echo $k.'--';
                    }
                }else{
                    $_district = DistrictGhnModel::find($district->id);
                    $_district->delete();
                }
            }
            $this->info('Cron check commissions success! ward giao hang nhanh');
        }catch (Exception $exception){
            dd($exception);
        }
    }

    public function wardGhn($district_id)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://online-gateway.ghn.vn/shiip/public-api/master-data/ward?district_id='.$district_id,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Token: 0eb6c9d0-a8f0-11ec-bc89-6623f36c3aa5'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }
}
