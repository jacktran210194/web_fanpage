<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IntroductionModel extends Model
{
    use HasFactory;
    protected $table = 'introduction';
    protected $guarded = [];
}
