<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProvinceGhnModel extends Model
{
    use HasFactory;
    protected $table = 'province_ghn';
    protected $guarded = [];
}
