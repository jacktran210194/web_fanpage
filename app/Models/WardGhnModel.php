<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WardGhnModel extends Model
{
    use HasFactory;
    protected $table = 'ward_ghn';
    protected $guarded = [];
}
