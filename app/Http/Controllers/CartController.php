<?php

namespace App\Http\Controllers;

use App\Models\AdminModel;
use App\Models\CartModel;
use App\Models\ProductsModel;
use Illuminate\Http\Request;
use PHPUnit\Exception;
use function Symfony\Component\Console\Style\section;

class CartController extends Controller
{
    public function index(Request $request, $slug){
        $user = $this->checkLoginUser();
        if (empty($user)){
            return redirect()->route('login');
        }
        $agency = AdminModel::where('slug', $slug)->where('level', 'admin')->first();
        if (empty($agency)){
            return back();
        }
        $data_cart = $this->getDataCart($user->id, $agency->id);
        $request->session()->put('parent_id' , $agency->id);
        $w_title = 'Giỏ hàng';
        $is_page = 'cart';
        return view('web.cart.cart', compact('w_title', 'is_page', 'agency', 'data_cart'));
    }

    public function updateQuantity (Request $request)
    {
        try{
            $check_token = $this->checkToken($request->header('token'));
            if ($check_token['status']){
                $cart = CartModel::where('user_id', $check_token['id'])->where('product_id', $request->get('product_id'))->first();
                if (empty($cart)){
                     $data_return = [
                         'status' => false,
                         'msg' => 'Sản phẩm trong giỏ hàng không tồn tại'
                     ];
                     return response()->json($data_return, 200);
                }
                $product = ProductsModel::find($request->get('product_id'));
                if ($product->quantity < 1){
                    $cart->delete();
                    $data_return = [
                        'status' => false,
                        'msg' => 'Sản phẩm này vừa được bán hết. Quý khách vui lòng chờ sản phẩm có hàng đẻ tiếp tục'
                    ];
                    return response()->json($data_return, 200);
                }
                $quantity = $request->get('quantity');
                switch ($request->get('type')){
                    case 1:
                        if (empty($quantity)){
                            $quantity = 1;
                        }else{
                            $quantity = $quantity + 1;
                        }
                        if ($quantity > $product->quantity){
                            $quantity = $product->quantity;
                        }
                        break;
                    case 2:
                        if (empty($quantity)){
                            $quantity = 1;
                        }else{
                            $quantity = $quantity - 1;
                        }
                        if ($quantity < 1){
                            $quantity = 1;
                        }
                        break;
                    default:
                        if (empty($quantity) || $quantity < 1){
                            $quantity = 1;
                        }
                        if ($quantity > $product->quantity){
                            $quantity = $product->quantity;
                        }
                }
                $money = $product->price * $quantity;
                $cart->quantity = $quantity;
                $cart->total_money = $money;
                $cart->save();
                $total_money = CartModel::where('user_id', $check_token['id'])->sum('total_money');
                $data_return = [
                    'status' => true,
                    'money' => number_format($money),
                    'quantity' => $quantity,
                    'total_money' => number_format($total_money)
                ];
                return response()->json($data_return, 200);
            }else{
                $data_return = [
                    'status' => false,
                    'msg' => $check_token['msg']
                ];
            }
            return response()->json($data_return, 200);
        }catch (Exception $exception){
            return $exception->getMessage();
        }
    }
}
