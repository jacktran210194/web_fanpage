<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function blog(){
        return view('web.blog.index');
    }
    public function detailBlog(){
        return view('web.blog.detail-blog');
    }
}
