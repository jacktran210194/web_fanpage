<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\GroupChatModel;
use App\Models\MenuBarModel;
use App\Models\SettingBankAdminModel;
use App\Models\WealthPlanModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use PHPUnit\Exception;
use Illuminate\Support\Facades\Storage;

class SettingGlobalController extends Controller
{
    public function apiGlobals(Request $request){
        try {
            $seq = $request->seq;
            $worker = $request->worker;
            $service = $request->service;
            $timeout = $request->timeout;
            $operation = $request->operation;
            $input = $request->input;
            $token = $request->token;

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://apitest.apsc.vn/openapi/services',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => '{
                                            "seq": '.$seq.',
                                            "worker": "'.$worker.'",
                                            "service": "'.$service.'",
                                            "timeout": '.$timeout.',
                                            "operation": "'.$operation.'",
                                            "input": '.$input.'
                                       }',
                CURLOPT_HTTPHEADER => array(
                    'vcode: V0001',
                    'vid: testven01',
                    'vpswd: vpswd123',
                    'seccode: 036',
                    'channel: T1',
                    'ipprivate: 1.1.1.1',
                    'ippublic: 2.2.2.2',
                    'info: Nam\'s iOS',
                    'lang: VI',
                    'signature: ',
                    'Authorization: Bearer '.$token,
                    'Content-Type: application/json'
                ),
            ));

            $response = curl_exec($curl);

            return $response;

        }catch (\Exception $exception){

            Storage::disk('local')->put(rand(1, 9999).'file.txt', $exception->getMessage());
        }
    }

    /**
     * Thông tin tài khoản nhận tiền của hệ thống
    */
    public function getBankAdmin(Request $request){
        $checkToken = $this->checkToken($request->header('token'));
        if ($checkToken['status']) {
            $data = SettingBankAdminModel::all();
            $dataReturn = [
                'status' => true,
                'data' => $data,
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => $checkToken['msg']
            ];
        }
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    /**
     * Danh sách các nhóm chát
    */
    public function getGroupChat(Request $request){
        $checkToken = $this->checkToken($request->header('token'));
        if ($checkToken['status']) {
            $data = GroupChatModel::all();
            $dataReturn = [
                'status' => true,
                'data' => $data,
            ];
        } else {
            $dataReturn = [
                'status' => false,
                'msg' => $checkToken['msg']
            ];
        }
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    /**
     * Thay đổi tên tích sản
    */
    public function changeNameWealthPlan(Request $request){
        try {
            $checkToken = $this->checkToken($request->header('token'));
            if ($checkToken['status']) {
                $data = WealthPlanModel::find($request->id);
                if ($data){
                    $data->type_name = $request->type_name??'';
                    $data->save();

                    $dataReturn = [
                        'status' => true,
                        'data' => $data,
                    ];
                }else{
                    $dataReturn = [
                        'status' => false,
                        'msg' => "Không tồn tại.!",
                    ];
                }
            } else {
                $dataReturn = [
                    'status' => false,
                    'msg' => $checkToken['msg']
                ];
            }
            return response()->json($dataReturn, Response::HTTP_OK);
        }catch (Exception $exception){

        }
    }

    /**
     * Danh sách menubar
    */
    public function getDataMenuBar(Request $request){
        try {
            $checkToken = $this->checkToken($request->header('token'));
            if ($checkToken['status']) {
                $data = MenuBarModel::where('is_active', 1)->get();
                $dataReturn = [
                    'status' => true,
                    'data' => $data,
                ];
            } else {
                $dataReturn = [
                    'status' => false,
                    'msg' => $checkToken['msg']
                ];
            }
            return response()->json($dataReturn, Response::HTTP_OK);
        }catch (Exception $exception){

        }
    }
}
