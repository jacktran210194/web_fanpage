<?php


namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Models\AddressModel;
use App\Models\AdminModel;
use App\Models\CartModel;
use App\Models\CategoriesModel;
use App\Models\OrderItemModel;
use App\Models\OrderModel;
use App\Models\ProductImageModel;
use App\Models\ProductsModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class HomePageController extends Controller
{

    /**
     * Lấy dữ liệu trang chủ
    */
    public function getHomePage(Request $request){
        try {
            $checkToken = $this->checkToken($request->header('token'));
            if ($checkToken['status']) {
                $top_product = ProductsModel::where('is_product_featured', 1)->where('parent_id', $checkToken['parent_id'])->where('is_active', 1)->orderBy('id', 'desc')->paginate(10);
                foreach ($top_product as $top){
                    $top->price = number_format($top->price);
                }
                $categories = CategoriesModel::where('parent_id', $checkToken['parent_id'])->where('is_active', 1)->get();
                $products_categories = CategoriesModel::where('parent_id', $checkToken['parent_id'])->where('is_active', 1)->get();
                foreach ($products_categories as $k => $item){
                    $product = ProductsModel::where('category_id', $item->id)->orderBy('id', 'desc')->paginate(10);
                    foreach ($product as $v){
                        $v->price = number_format($v->price);
                    }
                    $item['products'] = $product;
                }
                $number = $this->getMonth($checkToken['data']->date_of_birth);
                $lesson = $this->getLesson($number);
                $user = [
                    'age' => $number,
                    'name' => $checkToken['data']->name
                ];
                $data = [
                    'lesson' => $lesson,
                    'top_product' => $top_product,
                    'categories' => $categories,
                    'products_categories' => $products_categories,
                    'user' => $user
                ];

                $dataReturn = [
                    'status' => true,
                    'data' => $data
                ];
            } else {
                $dataReturn = [
                    'status' => false,
                    'msg' => $checkToken['msg']
                ];
            }

            return response()->json($dataReturn, Response::HTTP_OK);

        }catch (\Exception $exception){
            dd($exception);
        }
    }

    /**
     * Danh sách sản phẩm trong danh mục
    */
    public function filterProducts(Request $request){
        try {
            $__data_filter = $_POST;
            $dataProduct = $this->startFilter($request, $__data_filter);

            $dataReturn = [
                'status' => true,
                'data' => $dataProduct
            ];

            return response()->json($dataReturn, Response::HTTP_OK);

        }catch (\Exception $exception){
            dd($exception);
        }
    }

    /**
     * Bắt đầu filter sản phẩm
     */
    protected function startFilter($request, $__data_filter){
        try {
            $dataProduct = ProductsModel::query();

            foreach ($__data_filter as $item => $value){
                if ($item == 'key_search'){
                    $dataProduct = $dataProduct->where('name','like', '%'.$value.'%')
                        ->orWhere('sku','like', '%'.$value.'%')
                        ->orWhere('name_category','like', '%'.$value.'%')
                        ->orWhere('short_description','like', '%'.$value.'%')
                        ->orWhere('description','like', '%'.$value.'%')
                    ;
                }
                elseif ($item == 'min_price'){
                    $dataProduct = $dataProduct->where('price','>=', $value);
                }
                elseif($item == 'max_price'){
                    $dataProduct = $dataProduct->where('price','<=', $value);
                }else{
                    $dataProduct = $dataProduct->where($item, $value);

                }
            }

            $dataProduct = $dataProduct->orderBy('id', 'desc')->paginate(30);

            return $dataProduct;
        }catch (\Exception $exception){
            dd($exception);
        }
    }

    /**
     * Chi tiết sản phẩm
     */
    public function productDetail(Request $request){
        try {
            $data = ProductsModel::find($request->product_id);
            $data['image'] = ProductImageModel::where('product_id', $data->id)->get();

            $dataReturn = [
                'status' => true,
                'data' => $data
            ];

            return response()->json($dataReturn, Response::HTTP_OK);
        }catch (\Exception $exception){

            dd($exception);
        }
    }

    /**
     * Them san pham vao gio hang
     */
    public function addToCart(Request $request){
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'quantity' => 'required',
            'type' => 'required'

        ],[
            'product_id.required' => 'Vui lòng chọn sản phẩm!',
            'quantity.required' => 'Vui lòng điền số lượng cần mua!',
            'type.required' => 'Vui lòng chọn kiểu tăng hoặc giảm số lượng!',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'code' => 200,
                'message' => $validator->getMessageBag()->first(),
                'data' => $validator->errors(),
            ], 200);
        }
        $checkToken = $this->checkToken($request->header('token'));
        if ($checkToken['status']) {
            $users = $checkToken['data'];
            $cart = CartModel::where('user_id', $users->id)->where('product_id', $request->product_id)->first();
            $product = ProductsModel::find($request->product_id);
            $agency = AdminModel::find($product->parent_id);
            if ($product && $product->is_active == 1){
                if ($cart){
                    if ($request->type == 0){
                        $cart->quantity = (int)$cart->quantity - (int)$request->quantity;
                        $cart->total_money = (int)$product->price * (int)$cart->quantity;
                        $cart->total_money_promotional_price = (int)$product->promotional_price * (int)$cart->quantity;
                        $cart->save();
                        if ($cart->quantity <= 0){
                            $cart->delete();
                        }
                    }else{
                        $cart->quantity += (int)$request->quantity;
                        $cart->total_money = (int)$product->price * (int)$cart->quantity;
                        $cart->total_money_promotional_price = (int)$product->promotional_price * (int)$cart->quantity;
                        $cart->save();
                    }
                }else{
                    if ($request->type != 0){
                        $cart = new CartModel();
                        $cart->user_id = $users->id;
                        $cart->product_id = $product->id;
                        $cart->quantity = $request->quantity;
                        $cart->total_money = (int)$product->price * (int)$cart->quantity;
                        $cart->total_money_promotional_price = (int)$product->promotional_price * (int)$cart->quantity;
                        $cart->save();
                    }
                }


                $carts = $this->getDataCart($users->id, $product->parent_id);
                $data_return = [
                    'status' => true,
                    'code' => 200,
                    'message' => 'Bạn đã thêm sản phẩm thành công. Cảm ơn bạn.!',
                    'data' => $carts,
                    'slug' => $agency->slug
                ];
            }else{
                $carts = $this->getDataCart($users->id, $product->parent_id);
                $data_return = [
                    'status' => false,
                    'code' => 200,
                    'message' => 'Sản phẩm không tồn tại',
                    'data' => $carts,
                    'slug' => $agency->slug
                ];
            }
        } else {
            $data_return = [
                'status' => false,
                'msg' => $checkToken['msg']
            ];
        }

        return response()->json($data_return, 200);
    }

    /**
     * Danh sách sản phẩm trong giỏ hàng
     */
    public function getCart(Request $request){
        $checkToken = $this->checkToken($request->header('token'));
        if ($checkToken['status']) {
            $users = $checkToken['data'];

            $total_money = CartModel::join('products', 'products.id', '=', 'cart.product_id')->select('cart.*')
                ->where('products.parent_id', $request->get('parent_id'))->where('cart.user_id', $users->id)->sum('cart.total_money');
            $total_money_total_money_promotional_price = CartModel::join('products', 'products.id', '=', 'cart.product_id')->select('cart.*')
                ->where('products.parent_id', $request->get('parent_id'))->where('cart.user_id', $users->id)->sum('cart.total_money_promotional_price');
            $carts = $this->getDataCart($users->id, $request->get('parent_id'));
            $data_return = [
                'status' => true,
                'code' => 200,
                'message' => 'Lấy danh sách sản phẩm trong giỏ hàng thành công. Cảm ơn bạn.!',
                'data' => $carts,
                'total_money' => number_format($total_money),
                'total_money_total_money_promotional_price' => number_format($total_money_total_money_promotional_price),

            ];
        } else {
            $data_return = [
                'status' => false,
                'msg' => $checkToken['msg']
            ];
        }


        return response()->json($data_return, 200);
    }

    /**
     * xóa sản phẩm trong giỏ hàng
     */
    public function destroyCart(Request $request){
        $checkToken = $this->checkToken($request->header('token'));
        if ($checkToken['status']) {

            $users = $checkToken['data'];
            $carts = CartModel::query();
            if ($request->id){
                $carts->where('user_id', $users->id)->where('id', $request->id)->delete();
            }
            $carts = $this->getDataCart($users->id, $request->get('parent_id'));
            $data_return = [
                'status' => true,
                'code' => 200,
                'message' => 'Bạn đã xóa thành công. Cảm ơn bạn.!',
                'data' => $carts,
            ];
        } else {
            $data_return = [
                'status' => false,
                'msg' => $checkToken['msg']
            ];
        }

        return response()->json($data_return, 200);
    }

    /**
     * Tạo đơn hàng
    */
    public function placeOrder(Request $request){
        try {
            $checkToken = $this->checkToken($request->header('token'));
            if ($checkToken['status']) {
                $address = AddressModel::where('user_id', $checkToken['id'])->where('default', 1)->first();
                if (empty($request->name) || empty($request->phone) || empty($request->address)){
                    $dataReturn = [
                        'status' => false,
                        'message' => 'Vui lòng nhập địa chỉ để tiếp tục'
                    ];
                    return response()->json($dataReturn, Response::HTTP_OK);
                }
                if (isset($address)){
                    $address->phone = $request->get('phone');
                    $address->name = $request->get('name');
                    $address->address = $request->get('address');
                    $address->save();
                }else{
                    $address = new AddressModel();
                    $address['user_id'] = $checkToken['id'];
                    $address['name'] = $request->get('name');
                    $address['phone'] = $request->get('phone');
                    $address['address'] = $request->get('address');
                    $address['default'] = 1;
                    $address->save();
                }
                $carts = CartModel::join('products', 'products.id', '=', 'cart.product_id')->select('cart.*')
                    ->where('products.parent_id', $request->get('parent_id'))->where('cart.user_id', $checkToken['id'])->get();
                if (count($carts) > 0){
                    $data_code = $this->createOrder($checkToken['data'], $request, $address, $carts, $request->get('parent_id'));
                    CartModel::join('products', 'products.id', '=', 'cart.product_id')->select('cart.*')->where('products.parent_id', $request->get('parent_id'))->where('cart.user_id', $checkToken['id'])->delete();
                    $agency = AdminModel::find($request->get('parent_id'));
                    $dataReturn = [
                        'status' => true,
                        'message' => 'Tạo đơn hàng thành công.',
                        'data' => $data_code,
                        'slug' => $agency->slug ?? ''
                    ];
                }else{
                    $dataReturn = [
                        'status' => false,
                        'code' => 400,
                        'message' => 'Tạo đơn hàng thất bại. Cảm ơn bạn.!',
                    ];
                }
            } else {
                $dataReturn = [
                    'status' => false,
                    'message' => $checkToken['msg']
                ];
            }

            return response()->json($dataReturn, Response::HTTP_OK);
        }catch (\Exception $exception){
            dd($exception);
        }
    }

    /**
     * tao don hang
     */
    public function createOrder($users, $request, $address, $carts, $parent_id){
        try {
            $date_today = Carbon::now('Asia/Ho_Chi_Minh');
            $fee_ship = 30000;
            $money = 0;
            foreach ($carts as $value){
                $money += $value->total_money;
            }
            $total_money = $fee_ship + $money;

            $_order_refun = 0;

            $order = new OrderModel();
            $order->user_id = $users->id;
            $order->parent_id = $parent_id;
            $order->code_order = '';
            $order->code = $request->code??'';
            $order->money = $money;
            $order->fee_ship = $fee_ship;
            $order->money_use_code = 0;
            $order->total_money = $total_money;
            $order->name_user = $address->name;
            $order->address_user = $address->address;
            $order->email_user = $request->email_user??'';
            $order->phone_user = $address->phone;
            $order->note = $request->note??'';
            $order->status = 0;
            $order->is_order_refund = $_order_refun;
            $order->created_at = $date_today;

            $order->save();
            $order->code_order = 'TECHNIFY'.rand(0, 999).$order->id;
            $order->save();

            $this->createOrderItem($order, $carts);

            return $order;
        }catch (\Exception $exception){
            dd($exception);
        }
    }

    /**
     * Tạo item đơn hàng
    */
    public function createOrderItem($order, $carts){
        try {
            foreach ($carts as $k => $item){
                $product = ProductsModel::find($item->product_id);

                $order_item = new OrderItemModel();
                $order_item->order_id = $order->id;
                $order_item->product_id = $item->id;
                $order_item->category_id = $product->category_id;
                $order_item->category_name = $product->name_category;
                $order_item->sku = $product->sku;
                $order_item->img_product = $product->image;
                $order_item->name_product = $product->name;
                $order_item->description_product = $product->short_description;

                $order_item->price = $product->price;
                $order_item->promotional_price = $product->promotional_price;
                $order_item->quantity = $item->quantity;
                $order_item->total_money = $item->total_money;
                $order_item->total_money_promotional_price = $item->total_money_promotional_price;

                $order_item->save();
            }

            return true;
        }catch (\Exception $exception){

            dd($exception->getMessage());
        }
    }

    /**
     * address
    **/
    public function getAddress (Request $request)
    {
        $checkToken = $this->checkToken($request->header('token'));
        if ($checkToken['status']){
            $address = AddressModel::where('user_id', $checkToken['id'])->where('default', 1)->first();
            $dataReturn = [
                'status' => true,
                'data' => $address,
                'type' => isset($address) ? true : false
            ];
        }else{
            $dataReturn = [
                'status' => false,
                'msg' => $checkToken['msg']
            ];
        }
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    public function getDataAddress (Request $request)
    {
        $checkToken = $this->checkToken($request->header('token'));
        if ($checkToken['status']){
            $address = AddressModel::where('user_id', $checkToken['id'])->where('default', 1)->first();
            $dataReturn = [
                'status' => true,
                'data' => $address,
                'type' => isset($address) ? true : false
            ];
        }else{
            $dataReturn = [
                'status' => false,
                'msg' => $checkToken['msg']
            ];
        }
        return response()->json($dataReturn, Response::HTTP_OK);
    }
}
