<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\CategoriesModel;
use App\Models\HealthsModel;
use App\Models\IntroductionModel;
use App\Models\ProductsModel;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Testing\Fluent\Concerns\Has;

class UserController extends Controller
{
    public function registerOrLogin(Request $request){
        $phone = $request->phone??'';
        $parent_id = $request->parent_id??1;

        $check = User::where('phone', $phone)->first();
        $token = Str::random(60);
        if (isset($check)){
            $check->token = $token;
            $check->save();
            $msg = "Bạn đã đăng ký tham trước đó. Hãy tiếp tục để học tập và mua sắm.";
            $data=[
                'status' => false,
                'msg' => $msg
            ];
            return response()->json($data, Response::HTTP_OK);
        }else{
            $pass = bcrypt($request->get('password') ?? 'Admin123');
            $check = new User();
            $check->parent_id = $parent_id;
            $check->phone = $phone;
            $check->name = $request->name??'';
            $check->token = $token;
            $check->fcm_token = '123';
            $check->remember_token = $token;
            $check->date_of_birth = $request->date_of_birth??null;
            $check->password = $pass;


            $check->save();

//            $this->referalCode($request->intro_code, $check->id);
            $msg = "Bạn đã đăng ký thành công. Hãy tiếp tục để học tập và mua sắm.";
        }

        $check_intro = HealthsModel::where('user_id', $check->id)->get();
        if (count($check_intro) > 0){
            $is_info = 1;
        }else{
            $is_info = 0;
        }
        $check['is_intro'] = $is_info;
        $check['month'] = $this->getMonth($check->date_of_birth);

        $dataReturn = [
            'status' => true,
            'data' => $check,
            'token' => $token,
            'msg' => $msg
        ];

        return response()->json($dataReturn, Response::HTTP_OK);
    }

    /**
     * Thêm chỉ số sức khoẻ thành công
    */
    public function createHealths(Request $request){
        try {
            $checkToken = $this->checkToken($request->header('token'));

            if ($checkToken['status']) {

                $check = HealthsModel::where('day', $request->day)->where('user_id', $checkToken['id'])->first();
                if (empty($check)){
                    $check = new HealthsModel();
                    $check->day = $request->day;

                    $user = User::find($checkToken['id']);
                    $user->name = $request->name??$user->name;
                    if ($user->date_of_birth == null){
                        $user->date_of_birth = $request->day??null;
                    }
                    $user->save();
                }
                $check->height = $request->height??0;
                $check->weight = $request->weight??0;
                $check->sex = $request->sex??0;
                $check->user_id = $checkToken['id'];

                $check->save();

                $dataReturn = [
                    'msg' => 'Cập nhập chỉ số sức khoẻ thành công.!',
                    'status' => true,
                    'data' => $check
                ];
            } else {
                $dataReturn = [
                    'status' => false,
                    'msg' => $checkToken['msg']
                ];
            }

            return response()->json($dataReturn, Response::HTTP_OK);

        }catch (\Exception $exception){
            dd($exception);
        }
    }

    /**
     * Dữ liệu danh sách sức khoẻ
    */
    public function getHealths(Request $request){
        try {
            $checkToken = $this->checkToken($request->header('token'));
            if ($checkToken['status']) {
                $check = HealthsModel::where('user_id', $checkToken['id'])->orderBy('day', 'desc')->get();

                $dataReturn = [
                    'status' => true,
                    'data' => $check
                ];
            } else {
                $dataReturn = [
                    'status' => false,
                    'msg' => $checkToken['msg']
                ];
            }

            return response()->json($dataReturn, Response::HTTP_OK);

        }catch (\Exception $exception){
            dd($exception);
        }
    }

    protected function referalCode($intro_code, $f1_customer_id){
        $check = User::where('phone', $intro_code)->first();
        if ($check){
            $refer = new IntroductionModel();
            $refer->user_parent_id = $check->id;
            $refer->user_children_id = $f1_customer_id;
            $date_today = Carbon::now('Asia/Ho_Chi_Minh');
            $refer->created_at = $date_today;
            $refer->save();

//            $check->count_intro += 1;
//            $check->save();
            return true;
        }else{
            return false;
        }
    }
}
