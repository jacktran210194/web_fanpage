<?php

namespace App\Http\Controllers;

use App\Models\AdminModel;
use App\Models\CategoriesModel;
use App\Models\ProductsModel;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index (Request $request,$slug,$id)
    {
        $agency = AdminModel::where('slug', $slug)->where('level', 'admin')->first();
        if (empty($agency)){
            return back();
        }
        $request->session()->put('parent_id' , $agency->id);
        $categories = CategoriesModel::where('id', $id)->where('is_active', 1)->where('parent_id', $agency->id)->first();
        if (empty($categories)){
            return back();
        }
        $products = ProductsModel::where('is_active', 1)->where('category_id', $id)->get();
        $category = CategoriesModel::where('is_active', 1)->where('parent_id', $agency->id)->get();
        $w_title = 'Danh Mục';$is_page = 'category';
        return view('web.category.index', compact('category', 'categories', 'products', 'w_title', 'is_page', 'agency'));
    }

    public function category (Request $request, $slug)
    {
        $agency = AdminModel::where('slug', $slug)->where('level', 'admin')->first();
        if (empty($agency)){
            return back();
        }
        $request->session()->put('parent_id' , $agency->id);
        $category = CategoriesModel::where('is_active', 1)->where('parent_id', $agency->id)->get();
        $id = null;
        foreach ($category as $key => $value){
            if ($key == 0){
                $id = $value->id;
            }
        }
        $categories = CategoriesModel::where('id', $id)->where('is_active', 1)->where('parent_id', $agency->id)->first();
        if (empty($categories)){
            return back();
        }
        $products = ProductsModel::where('is_active', 1)->where('category_id', $id)->get();
        $w_title = 'Danh Mục';$is_page = 'category';
        return view('web.category.index', compact('category', 'categories', 'products','w_title', 'is_page', 'agency'));
    }

    public function filter (Request $request, $slug)
    {
        $agency = AdminModel::where('slug', $slug)->first();
        $category = CategoriesModel::find($request->get('category_id'));
        if (empty($category)){
            $products = ProductsModel::where('is_active', 1)->where('is_product_featured', 1)->where('parent_id', $agency->id)
                ->where('name', 'like', '%'.$request->get('key_search').'%')->get();
        }else{
            $products = ProductsModel::where("is_active", 1)->where('category_id', $request->get('category_id'))->where('parent_id', $agency->id)
                ->where('name', 'like', '%'.$request->get('key_search').'%')->get();
        }
        $view = view('web.category.product', compact('products'))->render();
        return response()->json(['status' => true, 'html' => $view]);
    }

    public function product (Request $request, $slug)
    {
        $user = $this->checkLoginUser();
        if (empty($user)){
            return redirect()->route('login');
        }
        $product = ProductsModel::where('slug', $slug)->where('is_active', 1)->first();
        if (empty($product)){
            return back();
        }
        $agency = AdminModel::find($product->parent_id);
        $similar_product = ProductsModel::where('is_active', 1)->where('category_id', $product->category_id)->where('id', '!=', $product->id)->paginate(3);
        return view('web.product.index', compact('product', 'similar_product', 'agency'));
    }

    public function similarProduct (Request $request)
    {
        try{
            $product = ProductsModel::where('slug', $request->get('slug'))->where('is_active', 1)->first();
            if (empty($product)){
                $data['status'] = false;
            }
            $similar_product = ProductsModel::where('is_active', 1)->where('category_id', $product->category_id)->where('id', '!=', $product->id)->paginate(3);
            $view = view('web.product.similar', compact('similar_product'))->render();
            return response()->json(['status' => true, 'html' => $view]);
        }catch (\Exception $exception){
            return $exception->getMessage();
        }
    }
}
