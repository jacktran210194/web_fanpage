<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Response;
use App\Models\User;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
    public function index ()
    {
        $titlePage = 'Admin';
        $page_menu = 'dashboard';
        $page_sub = null;
        $listData = [];
        return view('admin.dashboard', compact('titlePage', 'page_menu', 'page_sub', 'listData'));
    }
}
