<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\CategoriesModel;
use App\Models\ImageProduct;
use App\Models\ProductImageModel;
use App\Models\ProductsModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use PHPUnit\Exception;
use function Symfony\Component\String\b;

class ProductsController extends Controller
{
    public function index ()
    {
        $data_product = ProductsModel::orderBy('created_at', 'desc')->where('is_active', 1)->where('parent_id', Auth::id())->paginate(10);
        $titlePage = 'Admin | Sản Phẩm';
        $page_menu = 'products';
        $page_sub = 'index';
        $listData = $data_product;
        return view('admin.products.index', compact('titlePage', 'page_menu', 'page_sub', 'listData'));
    }

    public function create ()
    {
        $category = CategoriesModel::orderBy('created_at', 'desc')->where('is_active', 1)->where('parent_id', Auth::id())->get();
        $data['category'] = $category;
        $data['titlePage'] = 'Admin | Sản Phẩm';
        $data['page_menu'] = 'products';
        $data['page_sub'] = 'create';
        return view('admin.products.create', $data);
    }

    /**
     * store product
    **/
    public function store (Request $request)
    {
        try{
            $slug_product = Str::slug($request->get('name'));
            $category = CategoriesModel::find($request->get('category_id'));

            $is_product_featured = 0;
            if ($request->is_product_featured == 'on'){
                $is_product_featured = 1;
            }
            $fileName = '';
            if ($request->hasFile('src')) {
                $_video = $request->file('src');
                $fileExtension = $_video->getClientOriginalExtension();
                $fileName = md5(time()) . rand(0, 999999) . '.' . $fileExtension;
                $uploadPath = public_path('/upload/product/'); // Thư mục upload
                // Bắt đầu chuyển file vào thư mục
                $_video->move($uploadPath, $fileName);
                $fileName = 'upload/product/'.$fileName;
            }
            $user_id = Auth::id();
            $product = new ProductsModel();
            $product->parent_id = $user_id;
            $product->slug = $slug_product??'';
            $product->sku = $request->sku??'';
            $product->name = $request->name??'';
            $product->category_id = $category->id;
            $product->name_category = $category->name;
            $product->price = $request->price??0;
            $product->image = $fileName;
            $product->promotional_price = $request->promotional_price??0;
            $product->quantity = $request->quantity??0;
            $product->short_description = $request->short_description??'';
            $product->description = $request->description??'';
            $product->is_product_featured = $is_product_featured;
            $product->created_by = $user_id;

            $product->save();
            return \redirect()->route('admin.products.index')->with(['success' => 'Thêm sản phẩm thành công']);
        }catch (\Exception $exception){
            return back()->with(['error' => $exception->getMessage()]);
        }
    }
    /**
     * Delete Product
    **/
    public function delete ($id)
    {
       $product = ProductsModel::find($id);
//       unlink($product->image);

//       $data_image = ImageVariantModel::where('product_id', $id)->get();
//       if ($data_image){
//           foreach ($data_image as $value){
//               unlink($value->src);
//           }
//       }
       $product->delete();

       return back()->with(['success' => 'Xóa sản phẩm thành công']);
    }
    /**
     * Edit
    **/
    public function edit ($id)
    {
        $product = ProductsModel::find($id);
        $category = CategoriesModel::orderBy('created_at', 'desc')->where('is_active', 1)->where('parent_id', Auth::id())->get();
        $data['product'] = $product;
        $data['category'] = $category;

        $data['titlePage'] = 'Admin | Sản Phẩm';
        $data['page_menu'] = 'products';
        $data['page_sub'] = 'index';
        return view('admin.products.edit', $data);
    }
    /**
     * Update product
    **/
    public function update ($id, Request $request)
    {
        try{
            $product = ProductsModel::find($id);
            $slug_product = Str::slug($request->get('name'));
            $category = CategoriesModel::find($request->get('category_id'));

            $is_product_featured = 0;
            if ($request->is_product_featured == 'on'){
                $is_product_featured = 1;
            }
            $fileName = $product->image;
            if ($request->hasFile('src')) {
                $_video = $request->file('src');
                $fileExtension = $_video->getClientOriginalExtension();
                $fileName = md5(time()) . rand(0, 999999) . '.' . $fileExtension;
                $uploadPath = public_path('/upload/product/'); // Thư mục upload
                // Bắt đầu chuyển file vào thư mục
                $_video->move($uploadPath, $fileName);
                $fileName = 'upload/product/'.$fileName;
            }
            $user_id = Auth::id();
            $product->parent_id = $user_id;
            $product->slug = $slug_product??$product->slug;
            $product->sku = $request->sku??$product->sku;
            $product->name = $request->name??$product->name;
            $product->category_id = $category->id??$product->category_id;
            $product->name_category = $category->name??$product->name_category;
            $product->price = $request->price??$product->price;
            $product->image = $fileName;
            $product->promotional_price = $request->promotional_price??$product->promotional_price;
            $product->quantity = $request->quantity??$product->quantity;
            $product->short_description = $request->short_description??$product->short_description;
            $product->description = $request->description??$product->description;
            $product->is_product_featured = $is_product_featured;
            $product->created_by = $user_id;

            $product->save();
            return \redirect()->route('admin.products.index')->with(['success' => 'Sửa sản phẩm thành công']);
        }catch (\Exception $exception){
            return back()->with(['error' => $exception->getMessage()]);
        }
    }
    /**
     * Search product
    **/
    public function search (Request $request)
    {
        $key_work = $request->get('key_work');
        $data_product = ProductsModel::query();
        $data_product = $data_product->where('name', 'like', '%'.$key_work.'%')
            ->orWhere('name_category', 'like', '%'.$key_work.'%')
            ->orWhere('sku', 'like', '%'.$key_work.'%')
            ->orderBy('created_at', 'desc')->paginate(10);

        $titlePage = 'Admin | Sản Phẩm';
        $page_menu = 'products';
        $page_sub = 'index';
        $listData = $data_product;
        return view('admin.products.index', compact('titlePage', 'page_menu', 'page_sub', 'listData'));
    }
    /**
     * delete img variant
    **/
    public function deleteImg ($id)
    {
        try{
            $img = ProductImageModel::find($id);
            $img->delete();
            return back()->with(['success' => 'Xóa hình ảnh sản phẩm thành công']);

        }catch (\Exception $exception){
            $data['status'] = false;
            $data['msg'] = $exception->getMessage();
            return $data;
        }
    }

    /**
     * Image
     **/
    public function image ($id)
    {
        $product = ProductImageModel::where('product_id', $id)->get();
        $data['listData'] = $product;
        $data['product_id'] = $id;

        $data['titlePage'] = 'Admin | Sản Phẩm';
        $data['page_menu'] = 'products';
        $data['page_sub'] = 'index';
        return view('admin.products.image', $data);
    }

    /**
     * Lưu hình ảnh cho sản phẩm
    */
    public function saveImageProduct(Request $request){
        try {
            $get_image = $request->file('file');
            if ($get_image) {
                foreach ($get_image as $image) {
                    $get_name_image = $image->getClientOriginalName();
                    $name_image = current(explode('.', $get_name_image));
                    $new_image = $name_image . rand(0, 99) . '.' . $image->getClientOriginalExtension();
                    $image->move('uploads/images/product/list_image', $new_image);
                    $img_product = new ProductImageModel();
                    $img_product->src = '/uploads/images/product/list_image/'.$new_image;
                    $img_product->product_id = $request->product_id??0;
                    $img_product->save();
                }
            }
            return back()->with(['success' => 'Thêm hình ảnh sản phẩm thành công']);
        }catch (Exception $exception){
            dd($exception);
        }
    }
}
