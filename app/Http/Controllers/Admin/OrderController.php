<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\OrderItemModel;
use App\Models\OrderModel;
use App\Models\ProductsModel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PHPUnit\Exception;
use App\Models\ProvinceGhnModel;

class OrderController extends Controller
{
    /**
     * Danh sách order theo trang thái
    */
    public function getDataOrder(Request $request, $status){
        try {
            $user_id = Auth::id();
            $titlePage = 'Quản lý đơn hàng';
            $page_menu = 'order';
            $page_sub = '';
            $listData = OrderModel::query();
            if ($status !== 'all'){
                $listData = $listData->where('status', $status);
            }
            $listData = $listData->orderBy('created_at', 'desc')->where('parent_id', $user_id)->paginate(10);
            foreach ($listData as $item){
                $item['status_name'] = $this->checkStatusOrder($item);
            }

            $order_pending = OrderModel::where('status', 0)->where('parent_id', $user_id)->count();
            $order_confirm = OrderModel::where('status', 1)->where('parent_id', $user_id)->count();
            $order_delivery = OrderModel::where('status', 2)->where('parent_id', $user_id)->count();
            $order_complete = OrderModel::where('status', 3)->where('parent_id', $user_id)->count();
            $order_cancel = OrderModel::where('status', 4)->where('parent_id', $user_id)->count();
            $order_all = OrderModel::where('parent_id', $user_id)->count();
            return view('admin.order.index', compact('titlePage', 'page_menu', 'listData', 'page_sub', 'order_pending', 'order_confirm', 'order_delivery', 'order_complete', 'order_cancel', 'status', 'order_all'));
        }catch (\Exception $exception){
            dd($exception);
        }
    }
    /**
     * Chi tiết đơn hàng
    */
    public function orderDetail($order_id){
        try {
            $titlePage = 'Chi tiết đơn hàng';
            $page_menu = 'Đơn hàng';
            $page_sub = 'index';
            $check = false;
            $listData = OrderModel::find($order_id);
            if ($listData){
                $order_item = OrderItemModel::where('order_id', $order_id)->get();
                $listData['status_name'] = $this->checkStatusOrder($listData);
                $listData['ward_name'] = isset($ward) ? $ward->WardName : null;
                $listData['order_item'] = $order_item;
                $listData['check'] = $check;
                return view('admin.order.detail', compact('titlePage', 'page_menu', 'listData', 'page_sub'));

            }else{
                return back()->withErrors(['error' => 'Đơn hàng không tồn tại']);
            }
        }catch (\Exception $exception){
            return back()->withErrors(['error' => $exception->getMessage()]);
        }
    }

    /**
     * Xét trạng thái đơn hàng
    */
    public function statusOrder($order_id, $status_id){
        try {
            $order = OrderModel::find($order_id);
            if ($order){
                $order->status = $status_id;
                $order->save();
                return \redirect()->route('admin.order.index', ['status', $status_id])->with(['success' => 'Xét trạng thái đơn hàng thành công']);
            }
        }catch (\Exception $exception){
            dd($exception);
        }
    }

    /**
     * Trang tạo đơn hàng mới
    */
    public function createOrderAdmin(){
        try {
            $titlePage = 'Tạo đơn hàng mới || Quản lý đơn hàng';
            $page_menu = 'Tạo đơn hàng';
            $page_sub = 'create';
            $province = ProvinceGhnModel::orderBy('id', 'desc')->get();
            $products = ProductsModel::orderBy('id', 'desc')->paginate(20);

            return view('admin.order.create', compact('titlePage', 'page_menu', 'page_sub', 'province', 'products'));
        }catch (\Exception $exception){
            dd($exception);
        }
    }

    /**
     * Lưu đơn hàng admin tao moi
    */
    public function storeOderAdmin(Request $request){
        try {
            $value = $request->get('value_id');

            $province = ProvinceGhnModel::where('ProvinceID', $request->province_id)->first();
            $district = DistrictGhnModel::where('DistrictID', $request->district_id)->first();
            $ward = WardGhnModel::where('WardCode', $request->ward_id)->first();
            $_address_shipping = [
                'name' => $request->name,
                'phone' => $request->phone,
                'province_id_ghn' => $request->province_id,
                'district_id_ghn' => $request->district_id,
                'ward_id_ghn' => $request->ward_id,
                'address_detail' => $request->address_detail,
                'full_address' => $request->address_detail.' - '.$ward->WardName.' - '.$district->DistrictName.' - '.$province->ProvinceName
            ];

            $address_shop['district_id_ghn'] = '1490';
            $address_shop['phone'] = '0387287333';
            $address_shop['address'] = 'Ngõ 406, Kim Giang, Thanh Xuân, Hà Nội.';
            $weight = 300;
            $fee_ship = $this->pricing_order_ghn($address_shop, $_address_shipping, $weight);
            $total_money_product = $this->totalMoneyOrder($value);
//            $money_use_code = $this->getMoneyCode($request);
            $money_use_code = 0;
            $total_money = $fee_ship + $total_money_product - $money_use_code;
            $type_payment_name = 'Nhận hàng thanh toán';
            $type_payment_id = 0;

            $order = new OrderModel();
            $order->user_id = 0;
            $order->province_id = $_address_shipping['province_id_ghn'];
            $order->district_id = $_address_shipping['district_id_ghn'];
            $order->ward_id = $_address_shipping['ward_id_ghn'];
            $order->address_detail = $_address_shipping['address_detail'];
            $order->full_address = $_address_shipping['full_address'];
            $order->name = $_address_shipping['name'];
            $order->phone = $_address_shipping['phone'];
            $order->voucher_id = 0;
            $order->voucher_name = 'Null';
            $order->money_use_voucher = 0;
            $order->fee_shipping = $fee_ship;
            $order->order_code = 'MRT-OUTLET';
            $order->order_code_transport = 'GNH';
            $order->transport_name = 'GHN';
            $order->transport_id = 0;
            $order->total_money_product = $total_money_product;
            $order->total_money = $total_money;
            $order->type_payment_id = $type_payment_id;
            $order->type_payment_name = $type_payment_name;
            $is_percent_discount = 2;
            $order->is_percent_discount = $is_percent_discount;

            $order->save();
            $order->order_code = 'MRT-OUTLET'.rand(0, 999).$order->id;
            $order->save();
            $cart = $this->valueTransferCart($value);
            $this->createOrderItem($cart,$order, $address_shop, $_address_shipping);

            return \redirect()->route('admin.order.index', ['status' => 'all'])->with(['success' => 'Tạo đơn hàng mới thành công']);


        }catch (\Exception $exception){
            dd($exception);
        }
    }

    /**
     * Tìm kiếm sản phẩm tạo đơn hàng
    */
    public function searchProduct(Request $request){
        try {
            $key_search = $request->get('query');
            $products = ProductsModel::where('sku', 'LIKE', '%'.$key_search.'%')->orWhere('name', 'LIKE', '%'.$key_search.'%')->where('parent_id', Auth::id())->orderBy('id', 'desc')->paginate(10);

            $view = view('admin.order.item-product', compact('products'))->render();
            return response()->json(['table_data' => $view]);
        }catch (\Exception $exception){
            dd($exception);
        }
    }

}
