<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdminModel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login()
    {
        $title = 'Đăng nhập';
        return view('admin.login', compact('title'));
    }

    public function dologin (Request $request)
    {
        try{
            $bodyData = $request->all();
            $check = AdminModel::where('name', $bodyData['username'])
                ->exists();
            if ($check){
                $dataAttemptAdmin = [
                    'name' => $bodyData['username'],
                    'password' => $bodyData['password'],
                ];
                if (Auth::attempt($dataAttemptAdmin)) {
                    return redirect()->route('admin.index');
                }
            }else{
                return redirect()->route('admin.login')->with(['alert'=>'danger', 'message' => 'Tài khoản hoặc mật khẩu không chính xác']);
            }
        }catch (\Exception $e){
            return back()->with(['error' => $e->getMessage()]);
        }
    }

    /**
     * Đăng xuất
     **/
    public function logout()
    {
        Auth::logout();
        return redirect()
            ->route('admin.login')
            ->with(['alert' => 'success', 'message' => 'Đăng xuất thành công']);
    }

    public function loginCTV ()
    {
        $title = 'Đăng nhập';
        return view('ctv.login', compact('title'));
    }

    public function dologinCTV (Request $request)
    {
        try{
            $bodyData = $request->all();
            $check = AdminModel::where('name', $bodyData['username'])
                ->exists();
            if ($check){
                $dataAttemptAdmin = [
                    'name' => $bodyData['username'],
                    'password' => $bodyData['password'],
                ];
                if (Auth::attempt($dataAttemptAdmin)) {
                    return redirect()->route('ctv.index');
                }
            }else{
                return redirect()->route('ctv.login')->with(['alert'=>'danger', 'message' => 'Tài khoản hoặc mật khẩu không chính xác']);
            }
        }catch (\Exception $e){
            return back()->with(['error' => $e->getMessage()]);
        }
    }
}
