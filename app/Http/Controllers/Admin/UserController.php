<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Danh sách tài khoản
     */
    public function index()
    {
        $titlePage = 'Danh sách tài khoản khách hàng';
        $page_menu = 'users';
        $page_sub = 'users';

        $listData = User::where('parent_id', Auth::id())->orderBy('id', 'desc')->paginate(20);

        return view('admin.users.index', compact('titlePage', 'page_menu', 'listData', 'page_sub'));
    }

}
