<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdminModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Response;
use App\Models\User;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class AgencyController extends Controller
{
    public function index (Request $request)
    {
        $titlePage = 'Admin';
        $page_menu = 'agency';
        $page_sub = 'index';
        if (isset($request->key_work)){
            $listData = AdminModel::where('level', 'admin')->where('rule', 2)->where('name', 'like', '%'.$request->get('key_work').'%')->orderBy('created_at', 'desc')->paginate(10);
        }else{
            $listData = AdminModel::where('level', 'admin')->where('rule', 2)->orderBy('created_at', 'desc')->paginate(10);
        }
        foreach ($listData as $item){
            $url = $this->getUrl();
            $item['web'] = $url.'/home/'.$item->slug;
        }
        return view('admin.agency.index', compact('titlePage', 'page_menu', 'page_sub', 'listData'));
    }

    public function create ()
    {
        $titlePage = 'Admin';
        $page_menu = 'agency';
        $page_sub = 'create';
        return view('admin.agency.create', compact('titlePage', 'page_menu', 'page_sub'));
    }

    public function store (Request $request)
    {
        if (empty($request->name) || empty($request->name_shop)){
            return back()->with(['error' => 'Vui lòng điền đầy đủ thông tin']);
        }
        $slug = Str::slug($request->get('name_shop'));
        $agency = AdminModel::where('name', $request->get('name'))->where('level', 'admin')->first();
        $agency_2 = AdminModel::where('slug', $slug)->where('level', 'admin')->first();
        if (isset($agency) || isset($agency_2)){
            return back()->with(['error' => 'Đại lý đã tồn tại']);
        }
        $agency = new AdminModel();
        $agency['name'] = $request->get('name');
        $agency['name_shop'] = $request->get('name_shop');
        $agency['slug'] = $slug;
        $agency['password'] = bcrypt($request->password);
        $agency['level'] = 'admin';
        $agency->save();
        return \redirect()->route('admin.agency.index')->with(['success' => 'Tạo mới đại lý thành công']);
    }

    public function delete ($id)
    {
        $agency = AdminModel::find($id);
        if (empty($agency)){
            return back()->with(['error' => 'Đại lý không tồn tại']);
        }
        $agency->delete();
        return back()->with(['success' => 'Xóa tài khoản thành công']);
    }
}
