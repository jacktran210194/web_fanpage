<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\AdminModel;
use App\Models\CategoriesModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class SettingController extends Controller
{

    public function index ()
    {
        $user_rule = Auth::user()->rule;
        if ($user_rule == 1){
            $titlePage = 'Admin';
            $page_menu = 'agency';
            $page_sub = 'index';
            if (isset($request->key_work)){
                $listData = AdminModel::where('level', 'admin')->where('rule', 2)->where('name', 'like', '%'.$request->get('key_work').'%')->orderBy('created_at', 'desc')->paginate(10);
            }else{
                $listData = AdminModel::where('level', 'admin')->where('rule', 2)->orderBy('created_at', 'desc')->paginate(10);
            }
            foreach ($listData as $item){
                $url = $this->getUrl();
                $item['web'] = $url.'/home/'.$item->slug;
            }
            return view('admin.agency.index', compact('titlePage', 'page_menu', 'page_sub', 'listData'));
        }else{
            $titlePage = 'Admin | Danh Mục';
            $page_menu = 'category';
            $page_sub = 'index';
            $user_id = Auth::id();
            $listData = CategoriesModel::where('parent_id', $user_id)->get();
//        foreach ($listData as $k => $item){
//            $item['top'] = $listData->firstItem() + $k;
//        }
            return view('admin.category.index', compact('titlePage', 'page_menu', 'listData', 'page_sub'));
        }
    }

    public function create()
    {
        $titlePage = 'Admin | Danh Mục';
        $page_menu = 'category';
        $page_sub = 'create';
        return view('admin.category.create', compact('titlePage', 'page_menu', 'page_sub'));
    }

    public function store (Request $request)
    {
        try{
            $user_id = Auth::id();
            $fileName = '';
            if ($request->hasFile('src')) {
                $_video = $request->file('src');
                $fileExtension = $_video->getClientOriginalExtension();
                $fileName = md5(time()) . rand(0, 999999) . '.' . $fileExtension;
                $uploadPath = public_path('/upload/image/'); // Thư mục upload
                // Bắt đầu chuyển file vào thư mục
                $_video->move($uploadPath, $fileName);
                $fileName = 'upload/image/'.$fileName;
            }
            $_fileName = '';
            if ($request->hasFile('banner')) {
                $_banner = $request->file('banner');
                $_fileExtension = $_banner->getClientOriginalExtension();
                $_fileName = md5(time()) . rand(0, 999999) . '.' . $_fileExtension;
                $_uploadPath = public_path('/upload/image/'); // Thư mục upload
                // Bắt đầu chuyển file vào thư mục
                $_banner->move($_uploadPath, $_fileName);
                $_fileName = 'upload/image/'.$_fileName;
            }
            $slug = Str::slug($request->get('name'));
            $category = new CategoriesModel();
            $category['parent_id'] = $user_id;
            $category['name'] = $request->get('name');
            $category['slug'] = $slug;
            $category['is_active'] = $request->get('active');
            $category['src'] = $fileName;
            $category['banner'] = $_fileName;
            $category->save();
            return \redirect()->route('admin.category.index')->with(['success' => 'Tạo mới danh mục thành công']);
        }catch (\Exception $exception){
            return $exception->getMessage();
        }
    }

    public function delete ($id)
    {
        $category = CategoriesModel::find($id);
        $category->delete();
        return \redirect()->route('admin.category.index')->with(['success' => 'Xóa danh mục thành công']);
    }

    public function edit ($id)
    {
        $category = CategoriesModel::find($id);
        $titlePage = 'Admin | Danh Mục';
        $page_menu = 'category';
        $page_sub = null;
        return view('admin.category.edit', compact('category', 'titlePage', 'page_menu', 'page_sub'));
    }

    public function update (Request $request , $id)
    {
        try{
            $slug = Str::slug($request->get('name'));

            $category = CategoriesModel::find($id);
            $category->name = $request->get('name');
            $category->slug = $slug;
            $category->is_active = $request->get('active');
            $fileName = $category->src;
            if ($request->hasFile('src')) {
                $_video = $request->file('src');
                $fileExtension = $_video->getClientOriginalExtension();
                $fileName = md5(time()) . rand(0, 999999) . '.' . $fileExtension;
                $uploadPath = public_path('/upload/image/'); // Thư mục upload
                // Bắt đầu chuyển file vào thư mục
                $_video->move($uploadPath, $fileName);
                $fileName = 'upload/image/'.$fileName;
            }

            $_fileName = $category->banner;
            if ($request->hasFile('banner')) {
                $_banner = $request->file('banner');
                $_fileExtension = $_banner->getClientOriginalExtension();
                $_fileName = md5(time()) . rand(0, 999999) . '.' . $_fileExtension;
                $_uploadPath = public_path('/upload/image/'); // Thư mục upload
                // Bắt đầu chuyển file vào thư mục
                $_banner->move($_uploadPath, $_fileName);
                $_fileName = 'upload/image/'.$_fileName;
            }
            $category->src = $fileName;
            $category->banner = $_fileName;
            $category->save();
            return \redirect()->route('admin.category.index')->with(['success' => 'Cập nhập danh mục thành công']);
        }catch (\Exception $exception){
            return back()->with(['error' => $exception->getMessage()]);
        }
    }
}
