<?php

namespace App\Http\Controllers;

use App\Models\CartModel;
use App\Models\DistrictGhnModel;
use App\Models\ProductsModel;
use App\Models\User;
use App\Models\WardGhnModel;
use Carbon\Carbon;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Storage;
use Illuminate\Routing\UrlGenerator;

class Controller extends BaseController
{
    protected $url;

    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
    }

    public function getUrl()
    {
        $url = $this->url->to('/');
        return $url;
    }

    /**
     * lưu hình ảnh
     */
    public function saveImge($image){

        $fileExtension = $image->getClientOriginalExtension();
        $fileName = md5(time()) . rand(0, 999999) . '.' . $fileExtension;
        $uploadPath = public_path('/upload/images/notification/'); // Thư mục upload
        // Bắt đầu chuyển file vào thư mục
        $image->move($uploadPath, $fileName);
        $fileName = '/upload/images/notification/'.$fileName;

        return $fileName;
    }

    /**
     * Lấy danh sách quận huyện theo tỉnh
     */
    public function getDistrict($province_id){
        try {
            $districts = DistrictGhnModel::where('ProvinceId', $province_id)->get();

            $html = 'Tất cả';
            foreach ($districts as $value){
                $html .= '<option value="'.$value->DistrictID.'">'.$value->DistrictName.'</option>';
            }
            $data['html'] = $html;
            $data['status'] = true;
            return $data;
        }catch (\Exception $exception){
            dd($exception);
        }
    }

    /**
     * Lấy danh sách Phường/Xã theo quận huyện
     */
    public function getWard($district_id){
        try {
            $districts = WardGhnModel::where('DistrictID', $district_id)->get();

            $html = 'Tất cả';
            foreach ($districts as $value){
                $html .= '<option value="'.$value->WardCode.'">'.$value->WardName.'</option>';
            }
            $data['html'] = $html;
            $data['status'] = true;
            return $data;
        }catch (\Exception $exception){
            dd($exception);
        }
    }
    /**
     * check token
     */
    public function checkToken($token)
    {
        if (!$token){
            $dataReturn = [
                'status' => false,
                'msg' => 'Tài khoản của bạn đã đăng nhập vào thiết bị khác, vui lòng đăng nhập lại !'
            ];
        }else{
            $check = User::where('token', $token)->first();
            if ($check) {
                $dataReturn = [
                    'status' => true,
                    'data' => $check,
                    'id' => $check['id'],
                    'parent_id' => $check['parent_id']
                ];
            } else {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'Tài khoản của bạn đã đăng nhập vào thiết bị khác, vui lòng đăng nhập lại !'
                ];
            }
        }
        return $dataReturn;
    }

    /**
     * Tính toán tháng tuổi
    */
    public function getMonth($date_of_birth){
        $date_today = Carbon::now('Asia/Ho_Chi_Minh');
        $month = $date_today->diffInMonths($date_of_birth);

        return $month;
    }
    /**
     * Danh sách sản phẩm giỏ hàng
     */
    public function getDataCart($user_id,$parent_id){
        try {
            $carts = CartModel::join('products', 'products.id', '=', 'cart.product_id')->select('cart.*')
            ->where('products.parent_id', $parent_id)->where('cart.user_id', $user_id)->get();
            foreach ($carts as $k => $item){
                $product = ProductsModel::find($item->product_id);
                $carts[$k]['data_product'] = $product;
                $carts[$k]['total_money'] = number_format($item->total_money);
            }

            return $carts;
        }catch (\Exception $exception){

        }
    }


    /**
     * kiểm tra trạng thái đơn hàng
     */
    public function checkStatusOrder($item){

        if ($item->status == 0){
            $val_status = 'Chờ xác nhận';
        }elseif ($item->status == 1){
            $val_status = 'Đã xác nhận';
        }elseif ($item->status == 2){
            $val_status = 'Đang vận chuyển';
        }elseif ($item->status == 3){
            $val_status = 'Đã hoàn thành';
        }elseif ($item->status == 4){
            $val_status = 'Đã hủy';
        }else{
            $val_status = 'Hoàn trả hàng';
        }

        return $val_status;
    }

    /**
     * Lấy video từ bên thứ 3 về
    */
    public function getLesson($number){
        try {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'http://103.226.249.30:8001/api/api_lesson/'. $number,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            $response = json_decode($response);

            return $response->data;

        }catch (\Exception $exception){

        }
    }
    /**
     * Lấy video từ bên thứ 3 về
    */
    public function getDetailLesson($id){
        try {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'http://103.226.249.30:8001/api/api_detail_lesson/'. $id,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            $response = json_decode($response);

            return $response->data;

        }catch (\Exception $exception){

        }
    }

    public function checkLoginUser ()
    {
        $token = session()->get('token_user');
//        if (empty($token)){
//            return redirect()->route('login');
//        }
        $user = User::where('token', $token)->first();
        return $user;
    }
}
