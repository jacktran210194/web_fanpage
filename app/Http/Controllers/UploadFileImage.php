<?php

namespace App\Http\Controllers;

use App\Models\FileImageModel;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UploadFileImage extends Controller
{
    public function upload (Request $request)
    {
        if ($request->hasFile('file')){
            $file = $request->file('file');
            $file_name = $request->get('path').'/'.Str::random(40). '.'. $file->getClientOriginalExtension();
            $request->file('file')->move($request->get('path'), $file_name);
            $extension = $file->getClientOriginalExtension();
            $upload_file = new FileImageModel();
            $upload_file['src'] = $file_name;
            $upload_file['size'] = null;
            $upload_file['extension'] = $extension;
            $upload_file->save();
            $data['status'] = true;
            $data['src'] = $file_name;
        }else{
            $data['status'] = false;
        }
        return $data;
    }

    public function video (Request $request)
    {
        try{
            if(!$request->hasFile('files')) {
                return response()->json(['upload_file_not_found'], 400);
            }
            $file = $request->file('files');
            $size = $file->getSize();
            $extension = $file->getClientOriginalExtension();
            $file_name = $request->get('path').'/'.Str::random(40). '.'. $extension;
            $request->file('files')->move($request->get('path'), $file_name);
            $video = new FileImageModel([
                'src' => $file_name,
                'size' => $size,
                'extension' => $extension
            ]);
            $video->save();
            $data['status'] = true;
            $data['src'] = $file_name;
            return $data;
        }catch (\Exception $exception){
            return $exception->getMessage();
        }
    }
}
