<?php

namespace App\Http\Controllers;

use App\Models\AddressModel;
use App\Models\User;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function index(){
        return view('web.address.address');
    }

    public function create ()
    {
        return view('web.address.create');
    }
    public function edit(Request $request, $id){
        $user = $this->checkLoginUser();
        if (empty($user)){
            return redirect()->route('login');
        }
        $address = AddressModel::where('id', $id)->where('user_id', $user->id)->first();
        if (empty($address)){
            return back();
        }
        return view('web.address.update-address', compact('address','user'));
    }

    public function store (Request $request)
    {
        $checkToken = $this->checkToken($request->header('token'));
        if ($checkToken['status']){
            $address_user = AddressModel::where('user_id', $checkToken['id'])->where('default', 1)->first();
            $address = new AddressModel([
                'user_id' => $checkToken['id'],
                'name' => $request->get('name'),
                'phone' => $request->get('phone'),
                'address' => $request->get('address'),
                'comment' => $request->get('comment'),
                'default' => isset($address_user) ? 0 : 1
            ]);
            $address->save();
            $data = [
                'status' => true,
                'msg' => 'Thêm địa chỉ thành công'
            ];
        }else{
            $data = [
                'status' => false,
                'msg' => $checkToken['msg']
            ];
        }
        return response($data, 200);
    }

    public function update (Request $request, $id)
    {
        try{
            $user = User::where('token', $request->get('token'))->first();
            if (empty($user)){
                return back();
            }
            $address = AddressModel::where('id', $id)->where('user_id', $user->id)->first();
            if (empty($address)){
                return back();
            }
            $address->name = $request->get('name');
            $address->phone = $request->get('phone');
            $address->address = $request->get('address');
            $address->comment = $request->get('comment');
            $address->save();
            return redirect()->route('cart');
        }catch (\Exception $exception){
            return back()->with(['error' => $exception->getMessage()]);
        }
    }
}
