<?php

namespace App\Http\Controllers;

use App\Models\AdminModel;
use App\Models\HealthModel;
use App\Models\HealthsModel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class HealthController extends Controller
{
    public function healthHeight(Request $request)
    {
        $user = $this->checkLoginUser();
        if (empty($user)){
            return redirect()->route('login');
        }
        $agency = AdminModel::find($request->session()->get('parent_id'));
        return view('web.health.index', compact('agency'));
    }

    public function healthWeight(Request $request)
    {
        $user = $this->checkLoginUser();
        if (empty($user)){
            return redirect()->route('login');
        }
        $agency = AdminModel::find($request->session()->get('parent_id'));
        return view('web.health.health-weight', compact('agency'));
    }

    public function updateHealth(Request $request)
    {
        try {
            $check_token = $this->checkToken($request->header('token'));
            if ($check_token['status']) {
                $health = new HealthModel();
                $health->user_id = $check_token['id'];
                $health->height = $request->get('height');
                $health->weight = $request->get('weight');
                $health->date = $request->get('date');
                $health->save();
                $data_return = [
                    'status' => true,
                    'data' => $health
                ];
                return response()->json($data_return, 200);
            } else {
                $data_return = [
                    'status' => false,
                    'msg' => $check_token['msg']
                ];
            }
            return response()->json($data_return, 200);
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function heightChart(Request $request)
    {
        try {
            $check_token = $this->checkToken($request->header('token'));
            if ($check_token['status']) {
                $health = HealthModel::where('user_id', $check_token['id'])->get();
                $data_return = [
                    'status' => true,
                    'data' => $health
                ];
                return response()->json($data_return, 200);
            } else {
                $data_return = [
                    'status' => false,
                    'msg' => $check_token['msg']
                ];
            }
            return response()->json($data_return, 200);
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function account (Request $request)
    {
        $user = $this->checkLoginUser();
        if (empty($user)){
            return redirect()->route('login');
        }
        $agency = AdminModel::find($request->session()->get('parent_id'));
        return view('web.account', compact('user','agency'));
    }

    public function accountUpdate (Request $request)
    {
        $user = $this->checkLoginUser();
        if (empty($user)){
            return redirect()->route('login');
        }
        $user->name = $request->get('name');
        $user->date_of_birth = $request->get('date_of_birth');
        $user->save();
        return back();
    }

    public function getDataHealths (Request $request)
    {
        try{
            $user = $this->checkLoginUser();
            if (isset($user)) {
                $check = HealthsModel::where('user_id', $user->id)->orderBy('day', 'desc')->get();
                $dataReturn = [
                    'status' => true,
                    'data' => $check
                ];
            } else {
                $dataReturn = [
                    'status' => false,
                    'msg' => 'Vui lòng đăng nhập để tiếp tục'
                ];
            }

            return response()->json($dataReturn, Response::HTTP_OK);
        }catch (\Exception $exception){
            return $exception->getMessage();
        }
    }

    public function createHealths (Request $request)
    {
        $user = $this->checkLoginUser();
        if (isset($user)){
            $check = HealthsModel::where('day', $request->day)->where('user_id', $user->id)->first();
            if (empty($check)){
                $check = new HealthsModel();
                $check->day = $request->day;
                $user->name = $request->name??$user->name;
                if ($user->date_of_birth == null){
                    $user->date_of_birth = $request->day??null;
                }
                $user->save();
            }
            $check->height = $request->height??0;
            $check->weight = $request->weight??0;
            $check->sex = $request->sex??0;
            $check->user_id = $user->id;

            $check->save();

            $dataReturn = [
                'msg' => 'Cập nhập chỉ số sức khoẻ thành công.!',
                'status' => true,
                'data' => $check
            ];
        }else{
            $dataReturn = [
                'status' => false,
                'msg' => 'Vui lòng đăng nhập để tiếp tục'
            ];
        }
        return response()->json($dataReturn, Response::HTTP_OK);
    }
}
