<?php

namespace App\Http\Controllers;

use App\Models\AddressModel;
use App\Models\AdminModel;
use App\Models\CartModel;
use App\Models\CategoriesModel;
use App\Models\OrderItemModel;
use App\Models\OrderModel;
use App\Models\ProductsModel;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Testing\Fluent\Concerns\Has;

class HomeController extends Controller
{
    public function index(Request $request, $slug)
    {
        $agency = AdminModel::where('slug', $slug)->where('level', 'admin')->first();
        if (empty($agency)){
            return back();
        }
        $request->session()->put('parent_id' , $agency->id);
        $user = $this->checkLoginUser();
        if (empty($user)){
            return redirect()->route('login');
        }

        $category = CategoriesModel::where('parent_id', $agency->id)->get();
        foreach ($category as $item){
            $product = ProductsModel::where('category_id', $item->id)->orderBy('id', 'desc')->paginate(10);
            foreach ($product as $v){
                $v->price = number_format($v->price);
            }
            $item['products'] = $product;
        }
        $top_product = ProductsModel::where('is_product_featured', 1)->where('parent_id', $agency->id)->where('is_active', 1)->orderBy('id', 'desc')->paginate(10);
        foreach ($top_product as $top){
            $top->price = number_format($top->price);
        }
        $w_title = 'Trang chủ';
        $is_page = 'home';
        return view('web.home.index', compact('w_title', 'is_page', 'category', 'agency', 'top_product'));
    }

    public function showSP(Request $request)
    {
        try{
            $product = ProductsModel::where('is_active', 1)->where('slug', $request->get('slug'))->first();
            if (empty($product)){
                $data['status'] = false;
                $data['msg'] = 'product is not default';
                return $data;
            }
            $view = view('web.showSP', compact('product'))->render();
            return response()->json(['status' => true, 'prop' => $view]);
        }catch (\Exception $exception){
            $data['status'] = false;
            return $data;
        }
    }

    public function checkLogin (Request $request)
    {
        $checkToken = $this->checkToken($request->get('token'));
        if ($checkToken['status']){
            $number = $this->getMonth($checkToken['data']->date_of_birth);
            $number_cart = CartModel::where('user_id', $checkToken['data']->id)->count();
            $user = [
                'age' => $number,
                'name' => $checkToken['data']->name,
                'number_cart' => $number_cart
            ];
            $data = [
                'status' => true,
                'user' => $user
            ];
        }else{
            $data = [
                'status' => false,
                'msg' => $checkToken['msg']
            ];
        }
        return response()->json($data, 200);
    }

    public function login ()
    {
        $parent_id = session()->get('parent_id');
        $agency = AdminModel::find($parent_id);
        return view('web.dang-nhap', compact('agency'));
    }

    public function dangnhap ()
    {
        $parent_id = session()->get('parent_id');
        $agency = AdminModel::find($parent_id);
        return view('web.loginDN', compact('agency'));
    }
    public function forgotPassword ()
    {
        return view('web.forgot-password');
    }

    public function changePass (Request $request)
    {
        try{
            $user = User::where('phone', $request->get('phone'))->first();
            if (empty($user)){
                return back()->with(['alert'=>'danger', 'message' => 'Số điện thoại không tồn tại']);
            }
            $pass = Hash::make($request->get('password'));
            $user->password = $pass;
            $user->save();
            return redirect()->route('login')->with(['alert'=> 'success', 'message' => 'Cập nhật mật khảu mới thành công']);
        }catch (\Exception $exception){
            return back()->with(['alert'=>'danger', 'message' => $exception->getMessage()]);
        }
    }

    public function dologin (Request $request)
    {
        try{
            $bodyData = $request->all();
            $check = User::where('phone', $bodyData['phone'])
                ->first();
            if (isset($check)){
                if (Hash::check($bodyData['password'], $check->password)) {
                    $request->session()->put('token_user' , $check->token);
                    $slug = $bodyData['slug'];
                    if (empty($slug)){
                        $agency = AdminModel::find($check->parent_id);
                        $slug = $agency->slug??'';
                    }
                    return redirect('home/'.$slug);
                }else{
                    return redirect()->route('login')->with(['alert'=>'danger', 'message' => 'Mật khẩu không chính xác']);
                }
            }else{
                return redirect()->route('login')->with(['alert'=>'danger', 'message' => 'Số điện thoại không chính xác']);
            }
        }catch (\Exception $e){
            dd($e);
            return back()->with(['error' => $e->getMessage()]);
        }
    }

    /**
     * check quantity product
    **/
    public function checkQuantity (Request $request)
    {
        try{
            $product = ProductsModel::where('is_active', 1)->where('slug', $request->get('slug'))->first();
            if (empty($product)){
                $data['status'] = false;
                $data['msg'] = 'Không tìm thấy sản phẩm';
            }
            $quantity = $request->get('quantity');
            if ($product->quantity == 0){
                $data['status'] = false;
                $data['msg'] = 'Sản phẩm đã hết hàng';
                return $data;
            }
            switch ($request->get('type')){
                case 1:
                    if (empty($quantity)){
                        $quantity = 0;
                    }
                    $quantity = $quantity + 1;
                    if ($quantity > $product->quantity){
                        $quantity = $product->quantity;
                    }
                    break;
                case 2:
                    if (empty($quantity) || $quantity == 0){
                        $quantity = 1;
                    }
                    $quantity = $quantity - 1;
                    if ($quantity < 1){
                        $quantity = 1;
                    }
                    break;
                default:
                    if (empty($quantity) || $quantity == 0){
                        $quantity = 1;
                    }
                    if ($quantity > $product->quantity){
                        $quantity = $product->quantity;
                    }
            }
            $price = $product->price * $quantity;
            $data['status'] = true;
            $data['quantity'] = $quantity;
            $data['price'] = number_format($price);
            return $data;
        }catch (\Exception $exception){
            return $exception->getMessage();
        }
    }

    public function information ()
    {
        $user = $this->checkLoginUser();
        if (empty($user)){
            return redirect()->route('login');
        }
        $parent_id = session()->get('parent_id');
        $agency = AdminModel::find($parent_id);
        return view('web.information-baby', compact('agency'));
    }
    public function topProduct ($slug)
    {
        $user = $this->checkLoginUser();
        if (empty($user)){
            return redirect()->route('login');
        }
        $agency = AdminModel::where('slug', $slug)->first();
        if (empty($agency)){
            return back();
        }
        $products = ProductsModel::where('is_active', 1)->where('is_product_featured', 1)->where('parent_id', $agency->id)->get();
        $w_title = 'Top sản phẩm';
        return view('web.product.top', compact('products', 'w_title', 'agency'));
    }

    public function addToCart(Request $request){
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'quantity' => 'required',
            'type' => 'required'

        ],[
            'product_id.required' => 'Vui lòng chọn sản phẩm!',
            'quantity.required' => 'Vui lòng điền số lượng cần mua!',
            'type.required' => 'Vui lòng chọn kiểu tăng hoặc giảm số lượng!',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'code' => 200,
                'message' => $validator->getMessageBag()->first(),
                'data' => $validator->errors(),
            ], 200);
        }

        $users = $this->checkLoginUser();
        if (empty($users)){
            return redirect()->route('login');
        }
            $cart = CartModel::where('user_id', $users->id)->where('product_id', $request->product_id)->first();
            $product = ProductsModel::find($request->product_id);
            $agency = AdminModel::find($product->parent_id);
            if ($product && $product->is_active == 1){
                if ($cart){
                    if ($request->type == 0){
                        $cart->quantity = (int)$cart->quantity - (int)$request->quantity;
                        $cart->total_money = (int)$product->price * (int)$cart->quantity;
                        $cart->total_money_promotional_price = (int)$product->promotional_price * (int)$cart->quantity;
                        $cart->save();
                        if ($cart->quantity <= 0){
                            $cart->delete();
                        }
                    }else{
                        $cart->quantity += (int)$request->quantity;
                        $cart->total_money = (int)$product->price * (int)$cart->quantity;
                        $cart->total_money_promotional_price = (int)$product->promotional_price * (int)$cart->quantity;
                        $cart->save();
                    }
                }else{
                    if ($request->type != 0){
                        $cart = new CartModel();
                        $cart->user_id = $users->id;
                        $cart->product_id = $product->id;
                        $cart->quantity = $request->quantity;
                        $cart->total_money = (int)$product->price * (int)$cart->quantity;
                        $cart->total_money_promotional_price = (int)$product->promotional_price * (int)$cart->quantity;
                        $cart->save();
                    }
                }


                $carts = $this->getDataCart($users->id, $product->parent_id);
                $data_return = [
                    'status' => true,
                    'code' => 200,
                    'message' => 'Bạn đã thêm sản phẩm thành công. Cảm ơn bạn.!',
                    'data' => $carts,
                    'slug' => $agency->slug
                ];
            }else{
                $carts = $this->getDataCart($users->id, $product->parent_id);
                $data_return = [
                    'status' => false,
                    'code' => 200,
                    'message' => 'Sản phẩm không tồn tại',
                    'data' => $carts,
                    'slug' => $agency->slug
                ];
            }
        return response()->json($data_return, 200);
    }

    public function destroyCart(Request $request){
        $users = $this->checkLoginUser();
        if (empty($users)){
            return redirect()->route('login');
        }
            $carts = CartModel::query();
            if ($request->id){
                $carts->where('user_id', $users->id)->where('id', $request->id)->delete();
            }
            $carts = $this->getDataCart($users->id, $request->get('parent_id'));
            $data_return = [
                'status' => true,
                'code' => 200,
                'message' => 'Bạn đã xóa thành công. Cảm ơn bạn.!',
                'data' => $carts,
            ];

        return response()->json($data_return, 200);
    }

    public function placeOrder(Request $request){
        try {
            $users = $this->checkLoginUser();
            if (empty($users)){
                return redirect()->route('login');
            }
                $address = AddressModel::where('user_id', $users->id)->where('default', 1)->first();
                if (empty($request->name) || empty($request->phone) || empty($request->address)){
                    $dataReturn = [
                        'status' => false,
                        'message' => 'Vui lòng nhập địa chỉ để tiếp tục'
                    ];
                    return response()->json($dataReturn, Response::HTTP_OK);
                }
                if (isset($address)){
                    $address->phone = $request->get('phone');
                    $address->name = $request->get('name');
                    $address->address = $request->get('address');
                    $address->save();
                }else{
                    $address = new AddressModel();
                    $address['user_id'] = $users->id;
                    $address['name'] = $request->get('name');
                    $address['phone'] = $request->get('phone');
                    $address['address'] = $request->get('address');
                    $address['default'] = 1;
                    $address->save();
                }
                $carts = CartModel::join('products', 'products.id', '=', 'cart.product_id')->select('cart.*')
                    ->where('products.parent_id', $request->get('parent_id'))->where('cart.user_id', $users->id)->get();
                if (count($carts) > 0){
                    $data_code = $this->createOrder($users, $request, $address, $carts, $request->get('parent_id'));
                    CartModel::join('products', 'products.id', '=', 'cart.product_id')->select('cart.*')->where('products.parent_id', $request->get('parent_id'))->where('cart.user_id', $users->id)->delete();
                    $agency = AdminModel::find($request->get('parent_id'));
                    $dataReturn = [
                        'status' => true,
                        'message' => 'Tạo đơn hàng thành công.',
                        'data' => $data_code,
                        'slug' => $agency->slug ?? ''
                    ];
                }else{
                    $dataReturn = [
                        'status' => false,
                        'code' => 400,
                        'message' => 'Tạo đơn hàng thất bại. Cảm ơn bạn.!',
                    ];
                }

            return response()->json($dataReturn, Response::HTTP_OK);
        }catch (\Exception $exception){
            dd($exception);
        }
    }

    public function getAddress (Request $request)
    {
        $users = $this->checkLoginUser();
        if (empty($users)){
            return redirect()->route('login');
        }
            $address = AddressModel::where('user_id', $users->id)->where('default', 1)->first();
            $dataReturn = [
                'status' => true,
                'data' => $address,
                'type' => isset($address) ? true : false
            ];
        return response()->json($dataReturn, Response::HTTP_OK);
    }

    public function getDataAddress (Request $request)
    {

        $users = $this->checkLoginUser();
        if (empty($users)){
            return redirect()->route('login');
        }
            $address = AddressModel::where('user_id', $users->id)->where('default', 1)->first();
            $dataReturn = [
                'status' => true,
                'data' => $address,
                'type' => isset($address) ? true : false
            ];

        return response()->json($dataReturn, Response::HTTP_OK);
    }

    public function store (Request $request)
    {
        $users = $this->checkLoginUser();
        if (empty($users)){
            return redirect()->route('login');
        }
            $address_user = AddressModel::where('user_id', $users->id)->where('default', 1)->first();
            $address = new AddressModel([
                'user_id' => $users->id,
                'name' => $request->get('name'),
                'phone' => $request->get('phone'),
                'address' => $request->get('address'),
                'comment' => $request->get('comment'),
                'default' => isset($address_user) ? 0 : 1
            ]);
            $address->save();
            $data = [
                'status' => true,
                'msg' => 'Thêm địa chỉ thành công'
            ];

        return response($data, 200);
    }

    /**
     * tao don hang
     */
    public function createOrder($users, $request, $address, $carts, $parent_id){
        try {
            $date_today = Carbon::now('Asia/Ho_Chi_Minh');
            $fee_ship = 30000;
            $money = 0;
            foreach ($carts as $value){
                $money += $value->total_money;
            }
            $total_money = $fee_ship + $money;

            $_order_refun = 0;

            $order = new OrderModel();
            $order->user_id = $users->id;
            $order->parent_id = $parent_id;
            $order->code_order = '';
            $order->code = $request->code??'';
            $order->money = $money;
            $order->fee_ship = $fee_ship;
            $order->money_use_code = 0;
            $order->total_money = $total_money;
            $order->name_user = $address->name;
            $order->address_user = $address->address;
            $order->email_user = $request->email_user??'';
            $order->phone_user = $address->phone;
            $order->note = $request->note??'';
            $order->status = 0;
            $order->is_order_refund = $_order_refun;
            $order->created_at = $date_today;

            $order->save();
            $order->code_order = 'TECHNIFY'.rand(0, 999).$order->id;
            $order->save();

            $this->createOrderItem($order, $carts);

            return $order;
        }catch (\Exception $exception){
            dd($exception);
        }
    }

    /**
     * Tạo item đơn hàng
     */
    public function createOrderItem($order, $carts){
        try {
            foreach ($carts as $k => $item){
                $product = ProductsModel::find($item->product_id);

                $order_item = new OrderItemModel();
                $order_item->order_id = $order->id;
                $order_item->product_id = $item->id;
                $order_item->category_id = $product->category_id;
                $order_item->category_name = $product->name_category;
                $order_item->sku = $product->sku;
                $order_item->img_product = $product->image;
                $order_item->name_product = $product->name;
                $order_item->description_product = $product->short_description;

                $order_item->price = $product->price;
                $order_item->promotional_price = $product->promotional_price;
                $order_item->quantity = $item->quantity;
                $order_item->total_money = $item->total_money;
                $order_item->total_money_promotional_price = $item->total_money_promotional_price;

                $order_item->save();
            }

            return true;
        }catch (\Exception $exception){

            dd($exception->getMessage());
        }
    }
    public function getDataLesson (Request $request)
    {
        $user = $this->checkLoginUser();
        $number = $this->getMonth($user->date_of_birth);
        $lesson = $this->getLesson($number);
        $data = [
            'lesson' => $lesson,
        ];
        $dataReturn = [
            'status' => true,
            'data' => $data
        ];
        return response()->json($dataReturn, Response::HTTP_OK);
    }
}
