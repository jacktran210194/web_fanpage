<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\RedisEvent;
use App\Models\MessagesModel;



class RedisController extends Controller
{
    public function index(){
        $messages = MessagesModel::all();
        return view('messages',compact('messages'));
    }


    public function postSendMessage(Request $request){
    	$messages = MessagesModel::create($request->all());
    	event(
    		$e = new RedisEvent($messages)
    	);
    	return redirect()->back();
    }

}
