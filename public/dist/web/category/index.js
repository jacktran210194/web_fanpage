$(document).ready(function () {
    let index = $(".btn-item").length;
    for (let i = 0; i < index; i ++){
        if ($(".btn-item").eq(i).hasClass("active")){
            if (i > 0){
                $(".btn-item").eq(i + 1).addClass("bd-top");
                $(".btn-item").eq(i - 1).addClass("bd-bottom");
                $(".btn-item").eq(0).addClass("bd-left");
            }else{
                $(".btn-item").eq(0).removeClass("bd-left");
                $(".btn-item").eq(1).addClass("bd-top");
            }
        }
    }
    $(".btn-search").click(function () {
        let slug = $(this).val();
        let data = {};
        data['key_search'] = $('input[name="key_search"]').val();
        data['category_id'] = $('input[name="category_id"]').val();
        $.ajax({
           url: window.location.origin + '/danh-muc/'+slug+'/tim-kiem',
           data: data,
           type: 'post',
           dataType: 'json',
           success:function (data) {
               if (data.status){
                    $(".list-item").html(data.html);
               }
           }
        });
    });
});
