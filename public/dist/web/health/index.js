function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
$(".btn-update-health").click(function () {
    let form = new FormData();
    let day = $('input[name="date"]').val();
    let height = $('input[name="height"]').val();
    let weight = $('input[name="weight"]').val();
    form.append("day", day);
    form.append("height", height);
    form.append("weight", weight);

    let settings = {
        "url": window.location.origin + "/healths/create-healths",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "token": getCookie('token')
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form
    };

    $.ajax(settings).done(function (response) {
        let data = JSON.parse(response);
        if (data.status){
            location.reload();
        }else{
            console.log(data);
        }
    });
});
function healthWeight() {
    let form = new FormData();
    let settings = {
        "url": window.location.origin + "/data-healths",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "token": getCookie('token')
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form
    };

    $.ajax(settings).done(function (response) {
        let data = JSON.parse(response);
        let arrHealth = [];
        for (let i =0;i<data.data.length;i++){
            arrHealth.unshift(data.data[i].weight);
        }
        let growth = (arrHealth[data.data.length - 1] / (arrHealth[data.data.length - 1] + arrHealth[data.data.length - 2])) * 100
        if(isNaN(growth)){
            $('.growth').html(0);
        }else {
            $('.growth').html(growth.toFixed(2));
        }
        $('.present').html(arrHealth[data.data.length-1]);
        $('.birth').html(arrHealth[0]);
        const labels = [
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            '10',
            '11',
            '12',
        ];
        const data9 = {
            labels: labels,
            datasets: [{
                label: '',
                borderColor: '#F26091',
                tension: 0.1,
                fill: false,
                data: arrHealth,
            }]
        };
        const config = {
            type: 'line',
            data: data9,
            options: {
                legend: {
                    display: false,
                },
            }
        };

        const myChart = new Chart(
            document.getElementById('myChart'),
            config
        );
    });


}
$(function () {
    healthWeight()
});

