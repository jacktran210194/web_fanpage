function get_item_cart() {
    let form = new FormData();
    form.append("parent_id", $('input[name="parent_id"]').val());
    let settings = {
        "url": window.location.origin + "/api/cart/get-cart",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "token": getCookie('token')
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form
    };
    $.ajax(settings).done(function (data) {
        let response = JSON.parse(data);
        if (response.status){
            let count_cart = response.data.length;
            for (let i = 0; i < count_cart; i++){
                let html = '<div class="d-flex w-100 item-cart" style="border-bottom: 1px solid #EBEBEB;padding: 10px 0">'+
                    '<input name="product_id" hidden value="'+response.data[i].data_product.id+'">' +
                    '                    <img src="'+response.data[i].data_product.image+'" alt=""\n' +
                    '                         style="width: 117px;height: 117px;margin-right: 15px;object-fit: cover;border-radius: 8px;">\n' +
                    '                    <div style="width: calc(100% - 122px)">\n' +
                    '                        <p class="title-sp mb-1">'+response.data[i].data_product.name+'</p>\n' +
                    '                        <p class="gia mb-0">'+response.data[i].total_money+'đ</p>\n' +
                    // '                        <p class="giam-gia mb-0">850.000đ</p>\n' +
                    '                        <div class="d-flex justify-content-between align-items-center mt-2">\n' +
                    '                            <div class="buttons_added">\n' +
                    '                                <input class="minus is-form" type="button" value="-">\n' +
                    '                                <input aria-label="quantity" class="input-qty" max="10000" min="1" name="" type="number"\n' +
                    '                                       value="'+response.data[i].quantity+'">\n' +
                    '                                <input class="plus is-form" type="button" value="+">\n' +
                    '                            </div>\n' +
                    '                            <button value="'+response.data[i].id+'" style="outline: none !important;" class="border-0 p-0 bg-transparent btn-delete-cart"><i class="fa-solid fa-trash-can" style="color: #828282;cursor: pointer"></i></button>\n' +
                    '                        </div>\n' +
                    '                    </div>\n' +
                    '                </div>';
                $(".list-sp").append(html);
            }
            $(".total-all .total-gia").text(response.total_money + 'đ');
            $(".pay .pay-gia").text(response.total_money + 'đ');
        }
    });
}
$(function () {
   // get_item_cart();
   get_address();
});
$(document).on("click", ".item-cart .plus", function () {
   let parent = $(this).closest(".item-cart");
   let quantity = parent.find('.input-qty').val();
   let product_id = parent.find('input[name="product_id"]').val();
   let type = 1;
   add_cart_item(quantity, product_id, type, parent);
});
$(document).on("click", ".item-cart .minus", function () {
    let parent = $(this).closest(".item-cart");
    let quantity = parent.find('.input-qty').val();
    let product_id = parent.find('input[name="product_id"]').val();
    let type = 2;
    add_cart_item(quantity, product_id, type, parent);
});

function add_cart_item(quantity, product_id, type, parent) {
    let form = new FormData();
    form.append("product_id", product_id);
    form.append("quantity", quantity);
    form.append("type", type);

    let settings = {
        "url": window.location.origin + "/update-item-cart",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "token": getCookie('token')
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form
    };

    $.ajax(settings).done(function (response) {
        let data = JSON.parse(response);
        if (data.status){
            parent.find(".gia").text(data.money + 'đ');
            parent.find(".input-qty").val(data.quantity);
            $(".total-all .total-gia").text(data.total_money + 'đ');
            $(".pay .pay-gia").text(data.total_money + 'đ');
        }else{
            Swal.fire({
                title: data.msg,
                icon: 'error',
                showCancelButton: false,
                showCloseButton: true
            });
        }
    });
}
function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
function get_address() {
    let form = new FormData();
    let settings = {
        "url": window.location.origin + "/address/detail",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "token": getCookie('token')
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form
    };

    $.ajax(settings).done(function (response) {
        let data = JSON.parse(response);
        console.log(data);
        if (data.status){
            let html;
            if (data.type){
                 html = '<p class="mb-0 popup-title">Giao hàng tận nơi</p>\n' +
                    '            <div class="d-flex justify-content-between" style="padding: 10px 20px">\n' +
                    '                <p class="go mb-0">Giao tới</p>\n' +
                    '            </div>\n' +
                    '            <div style="padding: 0 20px;margin-bottom: 10px;">\n' +
                    '                <div class="d-flex" style="border-bottom: 1px solid #828282;padding-bottom: 5px">' +
                     '<img src="assets/images/people.svg" alt="">' +
                     '<input class="name-address" name="name-address" placeholder="Họ tên" type="text" value="'+data.data.name+'">' +
                     '</div> \n' +
                    '            </div>\n' +
                    '            <div style="padding: 0 20px;margin-bottom: 10px">\n' +
                    '                <div class="d-flex" style="border-bottom: 1px solid #828282;padding-bottom: 5px"> <img src="assets/images/bx_map.svg" alt=""><input type="text" name="address" placeholder="Địa chỉ" value="'+data.data.address+'" class="name-address"></div>\n' +
                    '            </div>\n' +
                    '            <div style="padding: 0 20px;margin-bottom: 5px;padding-bottom: 10px">\n' +
                    '               <div class="d-flex"> <img src="assets/images/phone.svg" alt=""><input type="number" name="phone-address" value="'+data.data.phone+'" placeholder="Số điện thoại" class="name-address"></div>\n' +
                    '            </div>';
            }else{
                html = '<p class="mb-0 popup-title">Giao hàng tận nơi</p>\n' +
                    '            <div class="d-flex justify-content-between" style="padding: 10px 20px">\n' +
                    '                <p class="go mb-0">Giao tới</p>\n' +
                    '            </div>\n' +
                    '            <div style="padding: 0 20px;margin-bottom: 10px;">\n' +
                    '                <div class="d-flex" style="border-bottom: 1px solid #828282;padding-bottom: 5px">' +
                    '<img src="assets/images/people.svg" alt="">' +
                    '<input class="name-address" name="name-address" placeholder="Họ tên" type="text">' +
                    '</div> \n' +
                    '            </div>\n' +
                    '            <div style="padding: 0 20px;margin-bottom: 10px">\n' +
                    '                <div class="d-flex" style="border-bottom: 1px solid #828282;padding-bottom: 5px"> <img src="assets/images/bx_map.svg" alt=""><input type="text" name="address" placeholder="Địa chỉ" class="name-address"></div>\n' +
                    '            </div>\n' +
                    '            <div style="padding: 0 20px;margin-bottom: 5px;padding-bottom: 10px">\n' +
                    '               <div class="d-flex"> <img src="assets/images/phone.svg" alt=""><input type="number" name="phone-address" placeholder="Số điện thoại" class="name-address"></div>\n' +
                    '            </div>';
            }
            $(".cart .popup").html(html);
        }
    });
}
$(document).on("click", ".btn-mua", function () {
    let name = $('input[name="name-address"]').val();
    let address = $('input[name="address"]').val();
    let phone = $('input[name="phone-address"]').val();
    let form = new FormData();
    form.append('name', name);
    form.append('address', address);
    form.append('phone', phone);
    form.append('parent_id', $('input[name="parent_id"]').val());
    let settings = {
        "url": window.location.origin + "/place-order",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "token": getCookie('token')
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form
    };
    $.ajax(settings).done(function (response) {
        let data = JSON.parse(response);
        if (data.status){
            Swal.fire({
                title: data.message,
                icon: 'success',
                showCancelButton: false,
                showCloseButton: true,
                width: 411
            });
            setTimeout(function () {
                location.replace('home/' + data.slug);
            }, 700);
        }else {
            Swal.fire({
                title: data.message,
                icon: 'error',
                showCancelButton: false,
                showCloseButton: true,
                width: 411
            });
        }
    });
});
$(document).on("click", ".btn-delete-cart", function () {
    let form = new FormData();
    form.append("id", $(this).val());
    form.append('parent_id', $('input[name="parent_id"]').val());
    let settings = {
        "url": window.location.origin + "/cart/delete-cart",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "token": getCookie('token')
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form
    };

    $.ajax(settings).done(function (response) {
        let data = JSON.parse(response);
        if (data.status){
            location.reload();
        }else{
            Swal.fire({
                title: data.message,
                icon: 'error',
                showCancelButton: false,
                showCloseButton: true,
                width: 411
            });
        }
    });
});
