function SliderMain(){
    let count_item = $(".slide-top-product a").length;
    if (count_item > 4){
        $('.slide-top-product').slick({
            infinite: true,
            speed: 1000,
            autoplay: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            prevArrow: '<div class="prev-arrow"><button style="width: 17px; height: 39px;left: 10px;z-index: 10;" class="btn btn-small"><i class="fa-solid fa-angle-left" style="color: white"></i></button></div>',
            nextArrow: '<div class="next-arrow"><button style="width: 17px; height: 39px;right: 10px;z-index: 10;" class="btn btn-small"><i class="fa-solid fa-angle-right" style="color: white"></i></button></i></div>',

        });
    }
}
function SliderDeatilBlog(){
    $('.slide-detail-blog').slick({
        infinite: true,
        speed: 1000,
        autoplay: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        prevArrow: '<div class="prev-arrow"><button style="width: 17px; height: 39px;left: 10px;z-index: 10;display: none" class="btn btn-small"><i class="fa-solid fa-angle-left" style="color: white"></i></button></div>',
        nextArrow: '<div class="next-arrow"><button style="width: 17px; height: 39px;right: 10px;z-index: 10;display: none" class="btn btn-small"><i class="fa-solid fa-angle-right" style="color: white"></i></button></i></div>',

    });
}

function SliderCategory() {
    let count_item = $('.slide-category-product a').length;
    if (count_item > 8){
        $('.slide-category-product').slick({
            rows: 2,
            infinite: true,
            speed: 1000,
            autoplay: false,
            dots: false,
            slidesToShow: 4,
            slidesToScroll: 2,
            arrows: true,
            prevArrow: '<div class="prev-arrow"><button style="width: 17px; height: 39px;left: 10px;z-index: 10;" class="btn btn-cate-next"><i class="fa-solid fa-angle-left" style="color: white"></i></button></div>',
            nextArrow: '<div class="next-arrow"><button style="width: 17px; height: 39px;left: -10px;z-index: 10;" class="btn btn-cate-next"><i class="fa-solid fa-angle-right" style="color: white"></i></button></div>',

        });
    }else{
        $(".slide-category-product").addClass("flex-wrap").addClass("justify-content-center");
    }
}

function SliderProduct(){
    $('.slide-sale-product').slick({
        infinite: true,
        speed: 1000,
        autoplay: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        prevArrow: '<div class="prev-arrow"><button style="width: 17px; height: 39px;left: 10px;z-index: 10;" class="btn btn-next"><i class="fa-solid fa-angle-left" style="color: white"></i></button></div>',
        nextArrow: '<div class="next-arrow"><button style="width: 17px; height: 39px;right: 10px;z-index: 10;" class="btn btn-next"><i class="fa-solid fa-angle-right" style="color: white"></i></button></i></div>',

    })
}
$(function (){
    SliderProduct();
    SliderDeatilBlog();
    SliderCategory();
    SliderMain();
});
home_page();
function home_page() {
    let settings = {
        "url": window.location.origin + "/get-lesson",
        "method": "post",
        "timeout": 0,
        "headers": {
            "token": getCookie('token')
        },
    };

    $.ajax(settings).done(function (response) {
        if (response.status){
            let count_lesson = response.data.lesson.length;
            for ( let i = 0; i < count_lesson; i++){
                let html = '<button value="'+response.data.lesson[i].id+'" class="d-flex border-0 bg-transparent w-100 justify-content-between align-items-center btn-lesson" style="border-top: 1px solid #01B2FF !important; padding: 10px 0; outline: none !important;">\n' +
                    '            <div>\n' +
                    '                <img src="'+response.data.lesson[i].image+'" alt="" style="width: 40px;height: 40px;border-radius: 8px;object-fit: cover">\n' +
                    '                <span class="ml-3 title-program">'+response.data.lesson[i].name+'</span>\n' +
                    '            </div>\n' +
                    '            <i class="fa-solid fa-angle-right" style="color: #01B2FF"></i>\n' +
                    '        </button>';
                $(".list-lesson").append(html);
            }
        }
    });
}
function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
$(document).on("click", ".btn-lesson", function () {
    let value = $(this).val();
    let settings = {
        "url": window.location.origin + "/api/home/detail-lesson/" + value,
        "method": "GET",
        "timeout": 0,
    };

    $.ajax(settings).done(function (response) {
        let html = '<div class="p-3 d-flex justify-content-end" style="background: #01B2FF">\n' +
            '            <button class="btn-close-lesson bg-transparent border-0 p-0">\n' +
            '                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#fff" class="bi bi-x-lg" viewBox="0 0 16 16">\n' +
            '                    <path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z"/>\n' +
            '                </svg>\n' +
            '            </button>\n' +
            '        </div>\n' +
            '        <div class="p-2 h-100 overflow-auto">\n' +
            '            <video class="w-100" poster="'+response.image+'" controls>\n' +
            '                <source src="'+response.video+'" type="video/mp4">\n' +
            '            </video>\n' +
            '            <h2 class="title-lesson" style="color: #333333;font-weight: 600;font-size: 15px;padding: 19px">'+response.name+'</h2>\n' +
            '            <div class="desc-lesson overflow-hidden" style="font-size: 13px;line-height: 32px;color: #828282;">\n' + response.content+ ' </div>\n' +
            '        </div>';
        $(".popup-lesson").html(html);
        $(".popup-lesson").addClass("active");
    });
});
$(document).on("click", ".btn-close-lesson", function () {
    $(".popup-lesson").html("");
    $(".popup-lesson").removeClass("active");
});
