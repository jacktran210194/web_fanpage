$(document).ready(function () {
    // check_login();
    function check_login() {
        $.ajax({
            url: window.location.origin + '/check-login',
            data: {'token' : getCookie('token')},
            type: 'post',
            dataType: 'json',
            success: function (data) {
                if (!data.status){
                    location.replace('dang-nhap');
                }else{
                    $("#header .name").text('Bé '+ data.user.name+ ' - ' +data.user.age+ ' tháng tuổi');
                    $("#header .circle").text(data.user.number_cart);
                }
            }
        })
    }
    function getCookie(cname) {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for(let i = 0; i <ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
});
