$(document).ready(function () {
    $(".btn-login").click(function () {
        let form = new FormData();
        let phone = $('input[name="phone"]').val();
        let day = $('input[name="date"]').val();
        let name = $('input[name="name"]').val();
        let password = $('input[name="password"]').val();
        let vnf_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
        if (vnf_regex.test(phone) == false){
            Swal.fire({
                title: 'Số điện thoại của bạn không đúng định dạng',
                icon: 'error',
                showCancelButton: false,
                showCloseButton: true,
                width: 411
            });
            return false;
        }
            if (phone == "" || name =="" || day == "" || password == ""){
            Swal.fire({
                title: 'Vui lòng điền đầy đủ thông tin',
                icon: 'error',
                showCancelButton: false,
                showCloseButton: true,
                width: 411
            });
            return false;
        }
        form.append("phone", phone);
        form.append("password", password);
        form.append("name", name);
        form.append("date_of_birth", day);
        form.append("parent_id", $('input[name="parent_id"]').val());

        let settings = {
            "url": window.location.origin + "/api/register-or-login",
            "method": "POST",
            "timeout": 0,
            "processData": false,
            "mimeType": "multipart/form-data",
            "contentType": false,
            "data": form
        };

        $.ajax(settings).done(function (response) {
            let data = JSON.parse(response);
            if (data.status){
                // let  slug = $('input[name="slug"]').val();
                setCookie('token', data.token, 1000);
                // location.replace('home/' + slug);
                location.replace('dang-nhap');
            }else {
                Swal.fire({
                    title: data.msg,
                    icon: 'error',
                    showCancelButton: false,
                    showCloseButton: true,
                    width: 411
                });
            }
        });
    });

    $(".btn-dn").click(function () {
        let phone = $('input[name="phone"]').val();
        let password = $('input[name="password"]').val();
        let vnf_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
        if (vnf_regex.test(phone) == false){
            Swal.fire({
                title: 'Số điện thoại của bạn không đúng định dạng',
                icon: 'error',
                showCancelButton: false,
                showCloseButton: true,
                width: 411
            });
            return false;
        }
        if (phone == "" || password == ""){
            Swal.fire({
                title: 'Vui lòng điền đầy đủ thông tin',
                icon: 'error',
                showCancelButton: false,
                showCloseButton: true,
                width: 411
            });
            return false;
        }
    });

    function setCookie(cname, cvalue, exdays) {
        const d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        let expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
});
