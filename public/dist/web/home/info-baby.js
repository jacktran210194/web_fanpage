$(".update-health").click(function () {
    let form = new FormData();
    let day = $('input[name="date"]').val();
    let name = $('input[name="name"]').val();
    let height = $('input[name="height"]').val();
    let weight = $('input[name="weight"]').val();
    // let sex = $('input[name="gioitinh"]:checked').val();
    if (day == "" || name == "" || height == "" || weight == "" ){
        Swal.fire({
            title: 'Vui lòng điền đầy đủ thông tin',
            icon: 'error',
            showCancelButton: false,
            showCloseButton: true,
            width: 411
        });
        return false;
    }
    form.append("name", name);
    form.append("day", day);
    form.append("height", height);
    form.append("weight", weight);
    // form.append("sex", sex);

    let settings = {
        "url": window.location.origin + "/api/healths/create-healths",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "token": getCookie('token')
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form
    };

    $.ajax(settings).done(function (response) {
        let data = JSON.parse(response);
        if (data.status){
            let slug = $('input[name="slug"]').val();
            location.replace('home/' + slug);
        }else{
            Swal.fire({
                title: data.msg,
                icon: 'error',
                showCancelButton: false,
                showCloseButton: true,
                width: 411
            });
        }
    });
});
function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
