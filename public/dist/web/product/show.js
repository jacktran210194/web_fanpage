$(document).on("click", ".btn-plus", function () {
    let data = {};
    data['slug'] = $(this).val();
    $.ajax({
        url: window.location.origin + '/show-sp',
        data: data,
        type: 'post',
        dataType: 'json',
        success: function (data) {
            if(data.status){
            $(".popup-slide").html(data.prop);
            $(".popup-slide").addClass("active");
            }
        }
    });
});
$(document).on("click", ".close-popup", function () {
    $(".popup-slide").removeClass("active");
    $(".popup-slide").html("");
});
$(document).on("change", '#show-product input[name="quantity"]', function () {
   let quantity = $(this).val();
   let slug = $("#show-product input[name='slug_product']").val();
   let data = {};
   data['quantity'] = quantity;
   data['slug'] = slug;
   data['type'] = 3;
   check_quantity(data);
});
$(document).on("click", "#show-product .btn-plus-quantity", function () {
    let quantity = $('#show-product input[name="quantity"]').val();
    let slug = $("#show-product input[name='slug_product']").val();
    let data = {};
    data['quantity'] = quantity;
    data['slug'] = slug;
    data['type'] = 1;
    check_quantity(data);
});
$(document).on("click", "#show-product .btn-minas-quantity", function () {
    let quantity = $('#show-product input[name="quantity"]').val();
    let slug = $("#show-product input[name='slug_product']").val();
    let data = {};
    data['quantity'] = quantity;
    data['slug'] = slug;
    data['type'] = 2;
    check_quantity(data);
});
$(document).on("click", "#show-product .add-cart", function () {
    let form = new FormData();
    form.append("product_id", $(this).val());
    form.append("quantity", $('#show-product input[name="quantity"]').val());
    form.append("type", "1");

    let settings = {
        "url": window.location.origin + "/api/cart/add-to-cart",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "token": getCookie('token')
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form
    };

    $.ajax(settings).done(function (response) {
        let data = JSON.parse(response);
        $(".popup-slide").removeClass("active");
        if (data.status){
            Swal.fire({
                title: data.message,
                icon: 'success',
                showCancelButton: false,
                showCloseButton: true,
                width: 411
            });
        }else{
            Swal.fire({
                title: data.message,
                icon: 'error',
                showCancelButton: false,
                showCloseButton: true,
                width: 411
            });
        }
    });
});
$(document).on("click", "#show-product .add-sale", function () {
    let form = new FormData();
    form.append("product_id", $(this).val());
    form.append("quantity", $('#show-product input[name="quantity"]').val());
    form.append("type", "1");

    let settings = {
        "url": window.location.origin + "/api/cart/add-to-cart",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "token": getCookie('token')
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form
    };

    $.ajax(settings).done(function (response) {
        let data = JSON.parse(response);
        $(".popup-slide").removeClass("active");
        if (data.status){
            Swal.fire({
                title: data.message,
                icon: 'success',
                showCancelButton: false,
                showCloseButton: true,
                width: 411
            });
            setTimeout(function () {
                location.replace('gio-hang/'+ data.slug);
            }, 700);
        }else{
            Swal.fire({
                title: data.message,
                icon: 'error',
                showCancelButton: false,
                showCloseButton: true,
                width: 411
            });
        }
    });
});
function check_quantity(data) {
    $.ajax({
        url: window.location.origin + '/check-quantity-product',
        type: 'post',
        data: data,
        dataType: 'json',
        success: function (data) {
            if (data.status){
                $('#show-product input[name="quantity"]').val(data.quantity);
                $("#show-product .gia-tam").text(data.price + 'đ');
            }else{
                Swal.fire({
                    title: data.msg,
                    icon: 'error',
                    showCancelButton: false,
                    showCloseButton: true,
                    width: 411
                });
            }
        }
    })
}
function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
