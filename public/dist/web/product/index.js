$(document).ready(function () {
   let height = $(".desc-product").outerHeight();
    let number_page = $(".number_page").attr("data-value");
    let page = 2;
    let slug = $('input[name="slug"]').val();
   if (height > 300){
       $(".desc-product").css("height", "300px");
   }else{
       $(".read-more").addClass("hide");
   }
   $(".btn-read-more").click(function () {
       if ($(this).hasClass("active")){
           $(this).removeClass("active");
           $(".desc-product").css("height", "300px");
           $('.read-more').css("height","93px")
       }else{
           $(this).addClass("active");
           $(".desc-product").css("height", height);
           $('.read-more').css("height","30px")
       }
   });
   $(".btn-load-more").click(function () {
       $(".loading").addClass("active");
       $.ajax({
           url: window.location.origin + '/similar-product?page=' + page,
           data: {'slug' : slug},
           dataType: 'json',
           type: 'post',
           success: function (data) {
               page += 1;
               $(".loading").removeClass("active");
               $(".data-product").append(data.html);
               if (page > parseInt(number_page)){
                   $(".btn-load-more").hide();
               }
           }
       });
   });
   $(".btn-buy-now").click(function () {
       let form = new FormData();
       form.append("product_id", $(this).val());
       form.append("quantity", '1');
       form.append("type", "1");

       let settings = {
           "url": window.location.origin + "/cart/add-to-cart",
           "method": "POST",
           "timeout": 0,
           "headers": {
               "token": getCookie('token')
           },
           "processData": false,
           "mimeType": "multipart/form-data",
           "contentType": false,
           "data": form
       };

       $.ajax(settings).done(function (response) {
           let data = JSON.parse(response);
           $(".popup-slide").removeClass("active");
           if (data.status){
               Swal.fire({
                   title: data.message,
                   icon: 'success',
                   showCancelButton: false,
                   showCloseButton: true,
                   width: 411
               });
               setTimeout(function () {
                   location.replace('gio-hang/'+ data.slug);
               }, 700);
           }else{
               Swal.fire({
                   title: data.message,
                   icon: 'error',
                   showCancelButton: false,
                   showCloseButton: true,
                   width: 411
               });
           }
       });
   });
    $(".btn-add-to-cart").click(function () {
        let form = new FormData();
        form.append("product_id", $(this).val());
        form.append("quantity", '1');
        form.append("type", "1");

        let settings = {
            "url": window.location.origin + "/cart/add-to-cart",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "token": getCookie('token')
            },
            "processData": false,
            "mimeType": "multipart/form-data",
            "contentType": false,
            "data": form
        };

        $.ajax(settings).done(function (response) {
            let data = JSON.parse(response);
            $(".popup-slide").removeClass("active");
            if (data.status){
                Swal.fire({
                    title: data.message,
                    icon: 'success',
                    showCancelButton: false,
                    showCloseButton: true,
                    width: 411
                });
                setTimeout(function () {
                    location.reload();
                }, 700);
            }else{
                Swal.fire({
                    title: data.message,
                    icon: 'error',
                    showCancelButton: false,
                    showCloseButton: true,
                    width: 411
                });
            }
        });
    });
    function getCookie(cname) {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for(let i = 0; i <ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
});
