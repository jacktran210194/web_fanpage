$(document).ready(function () {
   $(".btn-update-address").click(function () {
      let name = $('input[name="name"]').val();
      let phone = $('input[name="phone"]').val();
      let address = $('input[name="address"]').val();
      let comment = $('input[name="comment"]').val();
      if (name =="" || phone == "" || address == ""){
          Swal.fire({
              title: 'Vui lòng thêm đầy đủ thông tin để tiếp tục',
              icon: 'error',
              showCancelButton: false,
              showCloseButton: true,
              width: 411
          });
          return false;
      }
       let form = new FormData();
      form.append('name', name);
      form.append('phone', phone);
      form.append('address', address);
      form.append('comment', comment);
       let settings = {
           "url": window.location.origin + "/address/store",
           "method": "POST",
           "timeout": 0,
           "headers": {
               "token": getCookie('token')
           },
           "processData": false,
           "mimeType": "multipart/form-data",
           "contentType": false,
           "data": form
       };
       $.ajax(settings).done(function (response) {
           let data = JSON.parse(response);
           if (data.status){
               Swal.fire({
                   title: data.msg,
                   icon: 'success',
                   showCancelButton: false,
                   showCloseButton: true,
                   width: 411
               });
               setTimeout(function () {
                  location.replace('gio-hang');
               }, 700);
           }else {
               Swal.fire({
                   title: data.msg,
                   icon: 'error',
                   showCancelButton: false,
                   showCloseButton: true,
                   width: 411
               });
           }
       });
   });
});
