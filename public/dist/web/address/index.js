function get_address() {
    let form = new FormData();
    let settings = {
        "url": window.location.origin + "/address/data-address",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "token": getCookie('token')
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form
    };

    $.ajax(settings).done(function (response) {
        let data = JSON.parse(response);
        if (data.status){
            if (data.type){
                let html = '<div class="info-address mb-3">\n' +
                    '                <div class="d-flex align-items-start">\n' +
                    '                    <img src="assets/images/map.svg" alt="" style="margin-right: 10px">\n' +
                    '                    <div>\n' +
                    '                        <p class="mb-0 name-address">'+data.data.address+'</p>\n' +
                    '                        <p class="mb-0 name-title">'+data.data.name+'</p>\n' +
                    '                        <p class="mb-0 phone">'+data.data.phone+'</p>\n' +
                    '                    </div>\n' +
                    '                </div>\n' +
                    '                <a href="dia-chi/'+data.data.id+'?token='+getCookie('token')+'" class="edit">Sửa</a>\n' +
                    '            </div>';
                $(".address .list-item").append(html);
            }
        }
    });
}
$(function () {
   get_address();
});
