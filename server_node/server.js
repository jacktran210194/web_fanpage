var express = require("express");
var app = express();
var server = require("http").Server(app);
var io = require("socket.io")(server, {
    cors: {
        origin: "http://103.226.249.30:3000",
        methods: ["GET", "POST"],
        transports: ['websocket', 'polling'],
        credentials: true
    },
    allowEIO3: true,
    pingTimeout: 30000,
    pingInterval: 10000
});

server.listen(3000, () => console.log('Server running in port ' + 3000));

io.on('connection', function (socket) { //Bắt sự kiện một client kết nối đến server

    console.log('Co nguoi vua ket noi == ' + socket.id);
    // socket.emit("Stock-sent-data", 'abc');
    //server lắng nghe dữ liệu từ client
    socket.on("Stock-get-info", function(data)
    {
        io.sockets.emit("Stock-sent-data", data);
    });

    socket.on("Index-get-info", function(data)
    {
        io.sockets.emit("Index-sent-data", data);
        console.log(data);
    });

});

