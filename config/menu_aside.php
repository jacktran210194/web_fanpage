<?php

return [
    'admin' => [
        [
            'name' => 'order',
            'title' => 'Quản lý đơn hàng',
            'icon' => 'bi bi-grid',
            'route' => 'admin.order.index',
            'submenu' => [],
            'parameters' => ['status' => 'all'],
            'rule' => 2
        ],
        [
            'name' => 'category',
            'title' => 'Danh mục',
            'icon' => 'bi bi-grid',
            'route' => 'admin.category.index',
            'submenu' => [],
            'rule' => 2
        ],
        [
            'name' => 'products',
            'title' => 'Sản phẩm',
            'icon' => 'bi bi-grid',
            'route' => 'admin.products.index',
            'submenu' => [],
            'rule' => 2
        ],
        [
            'name' => 'users',
            'title' => 'Danh sách tài khoản khách hàng',
            'icon' => 'bi bi-grid',
            'route' => 'admin.users.index',
            'submenu' => [],
            'rule' => 2
        ],
        [
            'name' => 'agency',
            'title' =>'Đại lý',
            'icon' => 'bi bi-grid',
            'route' => null,
            'submenu' => [
                [
                    'title' => 'Danh sách đại lý',
                    'route' => 'admin.agency.index',
                    'name' => 'index'
                ],
                [
                    'title' => 'Tạo mới đaị lý',
                    'route' => 'admin.agency.create',
                    'name' => 'create'
                ]
            ],
            'rule' => 1
        ]
    ]
];
