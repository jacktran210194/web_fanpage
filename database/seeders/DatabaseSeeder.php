<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('setting_bank_admin')->insert([
            'name_bank' => 'Ngân hàng TMCP Kỹ thương Việt Nam(TCB)',
            'name_customer_bank' => 'CTCP CHUNG KHOAN ANPHA',
            'number_bank' => '121554123450145',
            'branch_bank' => 'Chi nhánh',
            'note' => 'Nội dung'
        ]);
    }
}
