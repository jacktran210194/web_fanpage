<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProvinceGhnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('province_ghn', function (Blueprint $table) {
            $table->id();
            $table->string('ProvinceID');
            $table->string('ProvinceName');
            $table->string('CountryID');
            $table->string('Code');
            $table->longText('NameExtension');
            $table->string('IsEnable');
            $table->string('RegionID');
            $table->string('UpdatedBy');
            $table->string('CanUpdateCOD');
            $table->string('Status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('province_ghn');
    }
}
