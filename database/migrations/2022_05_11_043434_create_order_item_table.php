<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_item', function (Blueprint $table) {
            $table->id();
            $table->integer('order_id');
            $table->integer('product_id');
            $table->integer('category_id');
            $table->string('category_name', 191);
            $table->string('sku', 191);
            $table->string('img_product', 191);
            $table->string('name_product', 191);
            $table->longText('description_product')->default(null);
            $table->BigInteger('price');
            $table->BigInteger('promotional_price');
            $table->integer('quantity');
            $table->integer('total_money');
            $table->integer('total_money_promotional_price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_item');
    }
}
