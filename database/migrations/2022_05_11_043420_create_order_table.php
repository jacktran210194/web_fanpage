<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->id();
            $table->integer('parent_id');
            $table->integer('user_id');;
            $table->string('code')->default(null);
            $table->string('code_order');
            $table->bigInteger('money');
            $table->bigInteger('fee_ship')->default(0);
            $table->bigInteger('money_use_code');
            $table->bigInteger('total_money');
            $table->string('name_user', 191);
            $table->string('address_user', 255);
            $table->string('email_user', 255);
            $table->string('phone_user', 191);
            $table->longText('note')->default(null);
            $table->integer('status')->default(0);
            $table->integer('is_order_refund')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
