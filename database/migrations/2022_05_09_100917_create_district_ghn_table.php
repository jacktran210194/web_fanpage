<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDistrictGhnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('district_ghn', function (Blueprint $table) {
            $table->id();
            $table->string('DistrictID')->default(null);
            $table->string('ProvinceID')->default(null);
            $table->string('DistrictName')->default(null);
            $table->string('Type')->default(null);
            $table->string('SupportType')->default(null);
            $table->longText('NameExtension')->default(null);
            $table->string('CanUpdateCOD')->default(null);
            $table->string('Status')->default(null);
            $table->longText('WhiteListClient')->default(null);
            $table->longText('WhiteListDistrict')->default(null);
            $table->string('ReasonCode')->default(null);
            $table->string('ReasonMessage')->default(null);
            $table->longText('OnDates')->default(null);
            $table->string('CreatedIP')->default(null);
            $table->string('CreatedEmployee')->default(null);
            $table->string('CreatedSource')->default(null);
            $table->string('UpdatedIP')->default(null);
            $table->string('UpdatedEmployee')->default(null);
            $table->string('UpdatedSource')->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('district_ghn');
    }
}
