<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWardGhnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ward_ghn', function (Blueprint $table) {
            $table->id();
            $table->string('WardCode');
            $table->string('DistrictID');
            $table->string('WardName');
            $table->longText('NameExtension');
            $table->string('IsEnable');
            $table->string('CanUpdateCOD');
            $table->string('SupportType');
            $table->string('Status');
            $table->string('ReasonCode');
            $table->string('ReasonMessage');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ward_ghn');
    }
}
