<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->integer('parent_id');
            $table->string('name');
            $table->string('slug')->nullable();
            $table->string('sku');
            $table->string('image');
            $table->integer('category_id');
            $table->string('name_category');
            $table->integer('price');
            $table->integer('promotional_price')->nullable();
            $table->integer('promotional_percent')->default(0);
            $table->integer('quantity');
            $table->string('video')->nullable();
            $table->string('short_description')->nullable();
            $table->longText('description')->nullable();
            $table->integer('is_active')->default(1);
            $table->integer('is_product_featured')->default(0);
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
