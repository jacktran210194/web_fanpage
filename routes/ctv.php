<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\LoginController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('dang-nhap', [LoginController::class,'loginCTV'])->name('login');
Route::post('dologin', [LoginController::class,'dologinCTV'])->name('doLogin');
Route::get('logout', [LoginController::class,'logout'])->name('logout');
Route::middleware('check-ctv-auth')->group(function (){
    Route::get('', [\App\Http\Controllers\Admin\DashboardController::class,'index'])->name('index');
});
