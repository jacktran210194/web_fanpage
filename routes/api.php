<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\HomePageController;
use App\Http\Controllers\AddressController;
use App\Http\Controllers\Controller;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('register-or-login', [UserController::class, 'registerOrLogin']);


//Trang chủ
Route::group(['prefix'=>'home'],function(){
    Route::get('', [HomePageController::class, 'getHomePage']);
    Route::get('detail-lesson/{id}', [Controller::class, 'getDetailLesson']);
});
//Chỉ số sức khoẻ
Route::group(['prefix'=>'healths'],function(){
    Route::get('', [UserController::class, 'getHealths']);
    Route::post('create-healths', [UserController::class, 'createHealths']);
});

//Danh mục và sản phẩm
Route::group(['prefix'=>'categories'],function(){
    Route::post('filter-products', [HomePageController::class, 'filterProducts']);
    Route::post('product-detail', [HomePageController::class, 'productDetail']);
});

//Giỏ hàng
Route::prefix('cart')->group(function () {
    Route::post('add-to-cart', [HomePageController::class, 'addToCart']);
    Route::post('get-cart', [HomePageController::class, 'getCart']);
    Route::post('delete-cart', [HomePageController::class, 'destroyCart']);
});
//Don hang
Route::prefix('place-order')->group(function () {
    Route::post('', [HomePageController::class, 'placeOrder']);
    Route::post('my-order', [HomePageController::class, 'getDataOrder']);
    Route::post('cancel-order/{id}', [HomePageController::class, 'cancelOrder']);
});
// Địa chỉ
Route::prefix('address')->group(function (){
    Route::post('detail', [HomePageController::class, 'getAddress']);
    Route::post('data-address', [HomePageController::class, 'getDataAddress']);
    Route::post('store', [AddressController::class, 'store']);
});
