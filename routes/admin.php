<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\ProductsController;
use App\Http\Controllers\Admin\AgencyController;
use App\Http\Controllers\Admin\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('dang-nhap', [LoginController::class,'login'])->name('login');
Route::post('dologin', [LoginController::class,'dologin'])->name('doLogin');
Route::get('logout', [LoginController::class,'logout'])->name('logout');
Route::middleware('check-admin-auth')->group(function (){
    Route::get('', [SettingController::class,'index'])->name('index');
    Route::prefix('category')->name('category.')->group(function (){
        Route::get('', [SettingController::class,'index'])->name('index');
        Route::get('create', [SettingController::class,'create'])->name('create');
        Route::post('store', [SettingController::class,'store'])->name('store');
        Route::get('delete/{id}', [SettingController::class,'delete']);
        Route::get('edit/{id}', [SettingController::class,'edit'])->name('edit');
        Route::post('update/{id}', [SettingController::class, 'update'])->name('update');
    });

    //Quản lý đơn hàng
    Route::prefix('order')->name('order.')->group(function (){
        Route::get('index/{status}', [OrderController::class,'getDataOrder'])->name('index');
        Route::get('create', [OrderController::class,'createOrderAdmin'])->name('create');
        Route::post('store', [OrderController::class,'storeOderAdmin'])->name('store');
        Route::get('detail/{id}', [OrderController::class,'orderDetail'])->name('detail');
        Route::get('status/{order_id}/{status_id}', [OrderController::class,'statusOrder'])->name('status');
        Route::post('select-kho', [OrderController::class,'selectKho']);
        Route::post('delete-product', [OrderController::class, 'deleteProduct']);
        Route::post('show-product', [OrderController::class, 'showProduct']);
        Route::post('search-product', [OrderController::class, 'filterProduct']);
        Route::post('add-product', [OrderController::class, 'addProduct']);
    });

    //Sản phẩm
    Route::prefix('products')->name('products.')->group(function (){
        Route::get('', [ProductsController::class, 'index'])->name('index');
        Route::get('create', [ProductsController::class, 'create'])->name('create');
        Route::post('store', [ProductsController::class, 'store'])->name('store');
        Route::get('search', [ProductsController::class, 'search'])->name('search');
        Route::get('delete/{id}', [ProductsController::class, 'delete']);
        Route::get('edit/{id}', [ProductsController::class, 'edit']);
        Route::get('image/{id}', [ProductsController::class, 'image']);
        Route::post('update/{id}', [ProductsController::class, 'update']);
        Route::get('delete-img/{id}', [ProductsController::class, 'deleteImg']);
        Route::post('save-image-product', [ProductsController::class, 'saveImageProduct'])->name('save_image_product');
        Route::get('search_product', [OrderController::class, 'searchProduct'])->name('search_product');
    });

    //Quản lý đơn hàng
    Route::prefix('order')->name('order.')->group(function (){
        Route::get('index/{status}', [OrderController::class,'getDataOrder'])->name('index');
        Route::get('create', [OrderController::class,'createOrderAdmin'])->name('create');
        Route::post('store', [OrderController::class,'storeOderAdmin'])->name('store');
        Route::get('detail/{id}', [OrderController::class,'orderDetail'])->name('detail');
        Route::get('status/{order_id}/{status_id}', [OrderController::class,'statusOrder'])->name('status');
        Route::post('select-kho', [OrderController::class,'selectKho']);
        Route::post('delete-product', [OrderController::class, 'deleteProduct']);
        Route::post('show-product', [OrderController::class, 'showProduct']);
        Route::post('search-product', [OrderController::class, 'filterProduct']);
        Route::post('add-product', [OrderController::class, 'addProduct']);
    });

    Route::prefix('agency')->name('agency.')->group(function (){
        Route::get('', [AgencyController::class, 'index'])->name('index');
        Route::get('create', [AgencyController::class, 'create'])->name('create');
        Route::post('store', [AgencyController::class, 'store'])->name('store');
        Route::get('delete/{id}', [AgencyController::class, 'delete'])->name('delete');
    });

    Route::prefix('users')->name('users.')->group(function (){
        Route::get('', [UserController::class, 'index'])->name('index');
    });
});

/**
 * CKEditor
 **/
Route::post('ckeditor/upload', [\App\Http\Controllers\CKEditorController::class, 'upload'])->name('ckeditor.image-upload');
