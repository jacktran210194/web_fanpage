<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RedisController;
use \App\Http\Controllers\HomeController;
use App\Http\Controllers\CategoryController;
use \App\Http\Controllers\CartController;
use \App\Http\Controllers\AddressController;
use App\Http\Controllers\Controller;
use \App\Http\Controllers\BlogController;
use \App\Http\Controllers\HealthController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('send-message', [RedisController::class, 'index'])->name('send-message');
Route::post('send-message', [RedisController::class,'postSendMessage']);

Route::get('get_district/{province_id}', [Controller::class,'getDistrict']);
Route::get('get_ward/{district_id}', [Controller::class,'getWard']);
Route::prefix('danh-muc/{slug}')->name('category.')->group(function (){
    Route::get('', [CategoryController::class, 'category']);
    Route::get('{id}', [CategoryController::class, 'index'])->name('index');
    Route::post('tim-kiem', [CategoryController::class, 'filter'])->name('filter');
});
Route::get('blog', [BlogController::class, 'blog']);
Route::get('detail-blog', [BlogController::class, 'detailBlog']);
Route::get('health-height', [HealthController::class, 'healthHeight']);
Route::get('health-weight', [HealthController::class, 'healthWeight']);
Route::get('account', [HealthController::class, 'account']);
Route::post('account/update', [HealthController::class, 'accountUpdate']);
Route::post('update-health', [HealthController::class, 'updateHealth']);
Route::post('health-height-chart', [HealthController::class, 'heightChart']);
Route::get('san-pham/{slug}', [CategoryController::class,'product'])->name('product');
Route::post('similar-product', [CategoryController::class, 'similarProduct']);
Route::get('gio-hang/{slug}', [CartController::class, 'index'])->name('cart');
Route::get('dia-chi', [AddressController::class, 'index'])->name('dia-chi');
Route::post('dia-chi/cap-nhat/{id}', [AddressController::class, 'update'])->name('address.update');
Route::get('dia-chi/them-moi', [AddressController::class, 'create'])->name('address.create');
Route::get('dia-chi/{id}', [AddressController::class, 'edit']);
Route::post('show-sp', [HomeController::class, 'showSP']);
Route::post('check-login', [HomeController::class, 'checkLogin']);
Route::get('dang-ky', [HomeController::class, 'login'])->name('dang-nhap');
Route::get('dang-nhap', [HomeController::class, 'dangnhap'])->name('login');
Route::get('forgot-password', [HomeController::class, 'forgotPassword'])->name('forgot-password');
Route::post('dologin', [HomeController::class,'dologin'])->name('doLogin');
Route::post('change-pass', [HomeController::class,'changePass'])->name('change-pass');
Route::post('check-quantity-product', [HomeController::class, 'checkQuantity']);
Route::post('update-item-cart', [CartController::class, 'updateQuantity']);
Route::get('information-baby', [HomeController::class, 'information']);
Route::get('top-san-pham/{slug}', [HomeController::class, 'topProduct'])->name('top-product');

Route::prefix('cart')->group(function () {
    Route::post('add-to-cart', [HomeController::class, 'addToCart']);
    Route::post('delete-cart', [HomeController::class, 'destroyCart']);
});

Route::prefix('place-order')->group(function () {
    Route::post('', [HomeController::class, 'placeOrder']);
});

Route::prefix('address')->group(function (){
    Route::post('detail', [HomeController::class, 'getAddress']);
    Route::post('data-address', [HomeController::class, 'getDataAddress']);
    Route::post('store', [HomeController::class, 'store']);
});

Route::get('home/{slug}', [HomeController::class, 'index'])->name('home');
Route::post('get-lesson', [HomeController::class, 'getDataLesson']);
Route::post('data-healths', [HealthController::class, 'getDataHealths']);

Route::prefix('healths')->group(function (){
    Route::post('create-healths', [HealthController::class, 'createHealths']);
});
